'use strict';

var config = require('../helper/lib').getConfig();
var adapter = require('../database/AdapterDB');

/**
 * route GeoService Provider Info
 * @param tenantId
 * @param tenantDomain
 * @param cityId
 * @param typeApp
 * @param callback
 * @returns {*}
 */
function routeGeoServiceProviderInfo(tenantId, tenantDomain, cityId, typeApp, callback) {
    var geoServiceType = "routing_service";
    adapter.getGeoKey(tenantId,cityId, geoServiceType ).then(function (geo_key) {
        var routeGeoServiceProviders = {
            1: "osrm",
            2: "google",
            3: "google",
            4: "herecom",
            5: "2gis"
        };
        var response = {
            "route_geo_service_provider": {
                "provider": null,
                "key_1": null,
                "key_2": null
            }
        };

        let service_provider = parseInt( geo_key.service_provider_id);
        response.route_geo_service_provider.provider = routeGeoServiceProviders[service_provider];
        response.route_geo_service_provider.key_1 = geo_key.key_1;
        response.route_geo_service_provider.key_2 = geo_key.key_2;

        return callback(null, response);
    });


    /*if (check.undefined(cityId)) {
        return callback(null, response);
    }


    if (typeApp === "driver") {
        if (check.undefined(tenantDomain)) {
            return callback(null, response);
        }
        query = {
            "query": {
                "bool": {
                    "must": [
                        {
                            "term": {
                                "tenant_domain": tenantDomain
                            }
                        }, {
                            "term": {
                                "city_id": cityId
                            }
                        }, {
                            "term": {
                                "service_type": geoServiceType
                            }
                        }

                    ]
                }
            },
            "size": 1
        };

    } else {
        if (check.undefined(tenantId)) {
            return callback(null, response);
        }
        query = {
            "query": {
                "bool": {
                    "must": [
                        {
                            "term": {
                                "tenant_id": tenantId
                            }
                        }, {
                            "term": {
                                "city_id": cityId
                            }
                        }, {
                            "term": {
                                "service_type": geoServiceType
                            }
                        }
                    ]
                }
            },
            "size": 1
        };
    }
    var cmd = {
        index: config.ELASTICSEARCH_PROVIDER_INDEX,
        body: query
    };
    // query backend
    service.search(backend, cmd, function (err, docs, meta) {
        if (err) {
            return callback(err);
        } else {
            if (check.array(docs)) {
                if (check.object(docs[0])) {
                    var providerData = docs[0];
                    var providerId = parseInt(providerData.service_provider_id, 10);
                    response.route_geo_service_provider.provider = routeGeoServiceProviders[providerId];
                    response.route_geo_service_provider.key_1 = providerData.key_1;
                    response.route_geo_service_provider.key_2 = providerData.key_2;
                    return callback(null, response);
                }
            }
            return callback(null, response);
        }
    });*/
}

module.exports = routeGeoServiceProviderInfo;
