'use strict';


/**
 * standart route response
 * @param version
 * @param query
 * @param errors
 * @param timestamp
 * @param results
 * @returns {{geocoding: {version: (*|string), query: *, errors: (*|Array), timestamp: (*|string)}, results: (*|Array)}}
 */
function standartResponse(version, query, errors, timestamp, results) {
    return {
        distancematrix: {
            version: version || '',
            query: query || query,
            errors: errors || [],
            timestamp: timestamp || ''
        },
        results: results || []
    };
}

module.exports = standartResponse;
