'use strict';

/**
 * Get config
 * @return {*}
 */
exports.getConfig = function () {
    return require('dotenv-safe').load().parsed;
};

/**
 * Get service version
 */
exports.getServiceVersion = function () {
    return require('../package.json').version;
};
