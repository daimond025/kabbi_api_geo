'use strict';

var md5 = require("md5");
var check = require('check-types');
var backend = require('../src/backend');
var config = require('../helper/lib').getConfig();
var service = {
    search: require('../service/search')
};

/**
 * Application query hash validator
 * @param {object} reqQuery
 * @param {string} hash
 * @param {string} typeApp
 * @param {number} tenantId
 * @param {string} tenantDomain
 * @param {function} callback
 */
function queryHashValidator(reqQuery, hash, typeApp, tenantId, tenantDomain, callback) {
    var orderedQuery = {};
    Object.keys(reqQuery).sort().forEach(function (key) {
        if (key !== "hash") {
            orderedQuery[key] = reqQuery[key];
        }
    });
    var queryString = requestParamsToQueryString(orderedQuery);
    var elastickQuery;
    if (typeApp === "driver") {
        elastickQuery = {
            "query": {
                "bool": {
                    "must": [
                        {
                            "term": {
                                "tenant_domain": tenantDomain
                            }
                        }, {
                            "term": {
                                "type_app": typeApp
                            }
                        }
                    ]
                }
            },
            "size": 1
        };
    } else if (typeApp === "client") {
        elastickQuery = {
            "query": {
                "bool": {
                    "must": [
                        {
                            "term": {
                                "tenant_id": tenantId
                            }
                        }, {
                            "term": {
                                "type_app": typeApp
                            }
                        }
                    ]
                }
            },
            "size": 1
        };
    } else if (typeApp === "frontend") {
        elastickQuery = {
            "query": {
                "bool": {
                    "must": [
                        {
                            "term": {
                                "type_app": typeApp
                            }
                        }
                    ]
                }
            },
            "size": 1
        };
    }
    var cmd = {
        index: config.ELASTICSEARCH_ACCESS_INDEX,
        body: elastickQuery
    };
    service.search(backend, cmd, function (err, docs, meta) {
        if (err) {
            return callback(err);
        } else {
            if (check.array(docs)) {
                if (check.object(docs[0])) {
                    if (check.string(docs[0].api_key)) {
                        var apiKey = docs[0].api_key;
                        var ourHash = md5(queryString + apiKey);
                        if (ourHash === hash) {
                            return callback(null, true);
                        }
                    }
                }
            }
            return callback(null, false);
        }
    });
}

/**
 * App request params to query string
 * @param {Object} incomeRequestParams
 * @returns {string}
 */
function requestParamsToQueryString(incomeRequestParams) {
    var out = [];
    for (var key in incomeRequestParams) {
        out.push(key + '=' + encodeURIComponent(incomeRequestParams[key]));
    }
    return out.join('&');
}

module.exports = queryHashValidator;
