'use strict';
var logger = require('./logger');

var number_ = 'street_number';
var street_ = 'route';
var city_ = 'locality';
var state_ = 'administrative_area_level_1';
var country_ = 'country';
var code_ = 'postal_code';

/**
 *
 * @param address_components
 * @param lat
 * @param lon
 * @returns {Array}
 */
function googlePlaceDeteils(address_components, name ,  lat, lon){
    var result = [];

    var number = '';
    var street = '';
    var city = '';
    var state = '';
    var country = '';
    var code = '';

    address_components.forEach(function(item){

        var component = (typeof item.types !== undefined || item.types.length > 0) ? item.types : [];
        var valueS = (typeof item.short_name !== undefined) ? item.short_name : [];
        var valueL = (typeof item.long_name !== undefined) ? item.long_name : [];

        if(component.includes(number_)){
            number = valueL;
        }

        if(component.includes(street_)){
            street = valueL;
        }

        if(component.includes(city_)){
            city = valueL;
        }

        if(component.includes(city_)){
            city = valueL;
        }

        if(component.includes(state_)){
            state = valueL;
        }

        if(component.includes(country_)){
            country = valueL;
        }

        if(component.includes(code_)){
            code = valueL;
        }
    });

    var itemFormatted = addressItem(number, street, city, state, country, code, name, lat, lon );
    result = itemFormatted;
    return result;
}

/**
 * formet address  item
 * @param house
 * @param street
 * @param city
 * @param state
 * @param country
 * @param code
 * @param lat
 * @param lon
 * @returns {{id: *, distance: *, duration: *}}
 */
function addressItem(house, street, city, state, country, code, name,  lat, lon){

    return {
        'house': house,
        'street': street,
        'city': city,
        'state': state,
        'country': country,
        'code': code,
        'lat': lat,
        'lon': lon,
        'name': name
    };
}

module.exports.googleFormatter = googlePlaceDeteils;