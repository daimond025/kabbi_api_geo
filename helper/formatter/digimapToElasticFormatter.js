'use strict';

var formatDataToElastic = require('./formatDataToElastic');

/**
 * digimap To Elastic Formatter
 * @param data
 * @returns {{docs: Array, meta: {scores: Array}}}
 */
function digimapToElasticFormatter(data) {
  var results = {
      'docs': [],
      'meta': {
          "scores": []
      }
  };
  var rawResultArray = data.data && data.data.places || [];
  rawResultArray.forEach(function(item, i, raw) {
    var geoItem = item;

    var lat = null;
    var lon = null;
    var number = '';
    var street = '';
    var name = '';
    var admin1 = '';
    var suburb = '';
    var locality = '';
    var alpha3 = '';
    var admin0 = '';
    var category = [];
    var _id = 1;
    var _type = "";
    var _score = 1;

    if (item.address) {
      street = item.address;
    }
    if (item.house) {
      number = item.house;
    }
    if (item.attributes) {
      locality = item.attributes.shift();
      admin1 = item.attributes.pop();
    }
    if (item.location) {
      lat = item.location.y;
      lon = item.location.x;
    }

    var formattedItem = formatDataToElastic(lat, lon, number, street, name, suburb, admin1, locality, alpha3, admin0, category, _id, _type, _score);

    results.docs.push(formattedItem);
    results.meta.scores.push(1);
  });

  return results;
}

module.exports = digimapToElasticFormatter;
