'use strict';

/**
 * yandex suggest to elastic formatter
 * @param data
 * @returns {{docs: Array, meta: {scores: Array}}}
 */
function yandexSuggestToElasticFormatter(data) {
    var rawResultArray = [];
    var results = {
        "docs": [],
        "meta": {
            "scores": []
        }
    };

    if (data && data.results instanceof Array) {
        rawResultArray = data.results;
        rawResultArray.forEach(function (item, i, rawResultArray) {
            if (item.type === "geo") {
                if (isNaN(parseFloat(item.lat)) || isNaN(parseFloat(item.lon))) {
                    return;
                }
                var _id = 1;
                var _type = "osmaddress";
                var _score = 1;
                var category = null;
                var admin1 = null;
                var admin0 = null;
                var suburb = null;
                var street = '';
                var number = '';
                var name = '';
                var locality = '';
                var descArr = [];
                var descNameArr = [];
                try {
                    descNameArr = item.name.split(',');
                } catch (err) {
                    console.log(err);
                }
                if (descNameArr.length === 2) {
                    street = descNameArr[0].trim();
                    number = descNameArr[1].trim();
                } else {
                    street = item.name;
                }
                try {
                    descArr = item.desc.split(',');
                } catch (err) {
                    console.log(err);
                }
                if (descArr.length > 1) {
                    admin0 = descArr.pop().trim();
                    locality = descArr.join(',');
                }
                var formattedItem = {
                    "center_point": {
                        "lat": item.lat,
                        "lon": item.lon
                    },
                    "address": {
                        "number": number,
                        "street": street
                    },
                    "name": {
                        "default": name
                    },
                    "suburb": suburb,
                    "admin1": admin1,
                    "locality": locality,
                    "alpha3": null,
                    "admin0": admin0,
                    "category": category,
                    "_id": _id,
                    "_type": _type,
                    "_score": _score
                };
                results.docs.push(formattedItem);
                results.meta.scores.push(1);
            }
        });
    }
    return results;
}


module.exports = yandexSuggestToElasticFormatter;
