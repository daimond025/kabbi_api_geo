'use strict';

/**
 * twoGis to elastic formatter
 * @param data
 * @returns {{docs: Array, meta: {scores: Array}}}
 */
function twoGisToElasticFormatter(data) {

    var results = {
        'docs': [],
        'meta': {
            "scores": []
        }
    };
    var rawResultArray = data.result.items ? data.result.items : [];

    rawResultArray.forEach(function (item) {
        var geoItem = item;
        var lat = null;
        var lon = null;
        var number = '';
        var street = '';
        var name = '';
        var admin1 = '';
        var suburb = '';
        var locality = '';
        var alpha3 = '';
        var admin0 = '';
        var category = [];
        var _id = 1;
        var _type = "osmnode";
        var _score = 1;
        if (geoItem.type === "building") {
            _type = "osmaddress";
        }

        if (geoItem.type === "street") {
            street = geoItem.name;
        }
        if (geoItem.name) {
            name = geoItem.name;
        }
        if (geoItem.adm_div) {
            geoItem.adm_div.forEach(function (admItem) {
                if (admItem.type === "city") {
                    locality = admItem.name;
                }
                if (admItem.type === "district_area") {
                    suburb = admItem.name;
                }
                if (admItem.type === "settlement") {
                    locality = admItem.name;
                }
            })
        }

        if (geoItem.address && geoItem.address.components) {
            geoItem.address.components.forEach(function (component) {
                if (component.type === "street_number") {
                    number = component.number;
                    street = component.street;
                }
            })
        }

        if (geoItem.geometry && geoItem.geometry.selection) {
            var geometryString = geoItem.geometry.selection;
            var startAddressPosition = null;
            var endAddressPosition = null;
            var firstCommaPosition = null;
            var doubleBracketsStartPosition = geometryString.indexOf("((");
            if (doubleBracketsStartPosition !== -1) {
                startAddressPosition = doubleBracketsStartPosition + 2;
                firstCommaPosition = geometryString.indexOf(",");
                if (firstCommaPosition !== -1) {
                    endAddressPosition = firstCommaPosition;
                }
            } else {
                var singleBracketsStartPosition = geometryString.indexOf("(");
                if (singleBracketsStartPosition !== -1) {
                    startAddressPosition = singleBracketsStartPosition + 1;
                    firstCommaPosition = geometryString.indexOf(",");
                    if (firstCommaPosition !== -1) {
                        endAddressPosition = firstCommaPosition;
                    } else {
                        var endSingleBracketPosition = geometryString.indexOf(")");
                        if (endSingleBracketPosition !== -1) {
                            endAddressPosition = endSingleBracketPosition;
                        }
                    }

                }
            }
            if (startAddressPosition && endAddressPosition) {
                var coordsRaw = geometryString.substring(startAddressPosition, endAddressPosition);
                var coords = coordsRaw.split(' ');
                if (coords[0] && coords[1]) {
                    lon = coords[0];
                    lat = coords[1];
                }
            }

        }

        var formattedItem = {
            "center_point": {
                "lon": lon,
                "lat": lat
            },
            "address": {
                "number": number,
                "street": street
            },
            "name": {
                "default": name
            },
            "suburb": suburb,
            "admin1": admin1,
            "locality": locality,
            "alpha3": alpha3,
            "admin0": admin0,
            "category": category,
            "_id": _id,
            "_type": _type,
            "_score": _score
        };

        if (formattedItem.center_point.lon && formattedItem.center_point.lat) {
            results.docs.push(formattedItem);
            results.meta.scores.push(1);
        }

    });
    return results;
}

module.exports = twoGisToElasticFormatter;
