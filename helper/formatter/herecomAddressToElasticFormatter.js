'use strict';

/**
 * herecom address to elastic formatter
 * @param data
 * @returns {{docs: Array, meta: {scores: Array}}}
 */
function herecomAddressToElasticFormatter(data) {
    var rawResultArray = [];
    var results = {
        "docs": [],
        "meta": {
            "scores": []
        }
    };
    if (data.Response) {
        var response = data.Response;
        if (response.View[0]) {
            var rView = response.View[0];
            if (rView.Result) {
                rawResultArray = rView.Result;
            }
        }
    }
    if (rawResultArray.length === 0) {
        return results;
    }

    rawResultArray.forEach(function (item, i, rawResultArray) {
        var geoItem = item;
        var lat = null;
        var lon = null;
        var number = '';
        var street = '';
        var name = '';
        var admin1 = '';
        var suburb = '';
        var locality = '';
        var alpha3 = '';
        var admin0 = '';
        var category = [];
        var _id = 1;
        var _type = "osmnode";
        var _score = 1;

        if (geoItem.Location) {
            if (geoItem.Location.DisplayPosition) {
                lat = geoItem.Location.DisplayPosition.Latitude;
                lon = geoItem.Location.DisplayPosition.Longitude;
            }
            if (geoItem.Location.Address) {
                var address = geoItem.Location.Address;
                alpha3 = address.Country;
                admin0 = address.Country;
                if (address.District) {
                    suburb = address.District;
                }
                if (address.District) {
                    suburb = address.District;
                }
                if (address.City) {
                    locality = address.City;
                }
                if (address.Street) {
                    street = address.Street;
                }
                if (address.HouseNumber) {
                    number = address.HouseNumber;
                    _type = "osmaddress";
                }

            }
        }

        var formattedItem = {
            "center_point": {
                "lon": lon,
                "lat": lat
            },
            "address": {
                "number": number,
                "street": street
            },
            "name": {
                "default": name
            },
            "suburb": suburb,
            "admin1": admin1,
            "locality": locality,
            "alpha3": alpha3,
            "admin0": admin0,
            "category": category,
            "_id": _id,
            "_type": _type,
            "_score": _score
        };
        results.docs.push(formattedItem);
        results.meta.scores.push(1);
    });
    return results;
}

module.exports = herecomAddressToElasticFormatter;