'use strict';

/**
 * gomapaz results to elastic format
 * @param {Array} rawResultArray
 * @param {String} lang
 * @return {{docs: Array, meta: {scores: Array}}}
 */
function gomapAzToElasticFormatter(rawResultArray, lang) {
    var results = {
        'docs': [],
        'meta': {
            "scores": []
        }
    };
    rawResultArray.forEach(function (item, i, raw) {
        var number = '';
        var street = '';
        var name = '';
        var admin1 = '';
        var suburb = '';
        var locality = '';
        var alpha3 = 'AZN';
        var admin0 = 'Azerbaijan';
        if (lang) {
            switch (lang) {
                case 'en':
                    alpha3 = 'AZN';
                    admin0 = 'Azerbaijan';
                    break;
                case 'ru':
                    alpha3 = 'AZN';
                    admin0 = 'Азербайджан';
                    break;
                case 'az':
                    alpha3 = 'AZN';
                    admin0 = 'Azərbaycan';
                    break;
                default:
                    break;
            }
        }
        var category = [];
        var _id = 1;
        var _type = "osmaddress";
        var _score = 1;
        var lat = item.y;
        var lon = item.x;
        if (item.nm === "address" || item.nm === "адрес" || item.nm === "ünvan") {
            _type = "osmaddress";
            name = item.addr;
            street = item.addr;
        } else {
            _type = "osmnode";
            if (item.stp) {
                name = item.stp + ': ' + item.nm;
            } else {
                name = item.nm;
            }
            street = item.addr;
        }

        name = replaceAll(name, '<br>', ', ');
        street = replaceAll(street, '<br>', ', ');
        var formattedItem = {
            "center_point": {
                "lon": lon,
                "lat": lat
            },
            "address": {
                "number": number,
                "street": street
            },
            "name": {
                "default": name
            },
            "suburb": suburb,
            "admin1": admin1,
            "locality": locality,
            "alpha3": alpha3,
            "admin0": admin0,
            "category": category,
            "_id": _id,
            "_type": _type,
            "_score": _score
        };
        results.docs.push(formattedItem);
        results.meta.scores.push(1);
    });
    return results;
}

function replaceAll(str, find, replace) {
    return str.replace(new RegExp(find, 'g'), replace);
}


module.exports = gomapAzToElasticFormatter;
