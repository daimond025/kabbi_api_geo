'use strict';

const http = require('http');
const request = require('request');
const uuid = require('uuid/v4');
const Room = require('./services/room.js');
const ChatModel = require('./services/mongoose').ChatModel;
const Client = require('./services/client.js');
const async = require('async');
const amqp = require('amqplib/callback_api');
const lib = require("./services/lib");
const config = lib.getConfig();
const express = require('express');
const _ = require('underscore');
const nodemailer = require('nodemailer');
const xssFilters = require('xss-filters');
const logger = require('./services/logger');
const chatLogger = logger.chatLogger;
const orderLogger = logger.orderLogger;
const workerSocketLogger = logger.workerSocketLogger;
const messageUnreadedWorker = require('./services/notify/messageUnreadedWorker');
const mainRouter = require('./api/routes/main');
const clients = {};
const rooms = {};
let activeSockets = [];

const app = express();

app.use(function (req, res, next) {
    req.activeState = {
        clients: clients,
        rooms: rooms,
        activeSockets: activeSockets,
    };
    next();
});

app.use('/', mainRouter);

const server = http.createServer(app);

// version 2.2.0  //"socket.io": "^2.2.0",
const io = require('socket.io')(server, {
    pingInterval: 20000,
    pingTimeout: 30000,
    transports: ['polling', 'websocket' ]
});

server.listen(config.MY_BIND_PORT, config.MY_BIND_HOST, () => {
    chatLogger('info', 'server started at ' + config.MY_BIND_HOST + ':' + config.MY_BIND_PORT);
});


// old version
/*const io = require('socket.io').listen(server, {
        pingInterval: 3000,
        pingTimeout: 15000,
        origins: '*:*'
    }
);*/


//io.sockets.on('connection', (socket) => {
io.sockets.on('connection', (socket) => {

    chatLogger('info', 'socket connected: ' + socket.id);
    let o = _.findWhere(activeSockets, {'id': socket.id});
    if (typeof o !== "undefined") {
        chatLogger('error', `socket with id: ${socket.id} already connected and will be disconnected`);
        socket.disconnect();
        return;
    }
    //Р”РѕР±Р°РІРёР»Рё СЃРѕРєРµС‚ РІ РјР°СЃСЃРёРІ Р°РєС‚РёРІРЅС‹С… СЃРѕРєРµС‚РѕРІ
    activeSockets.push(socket);
    let amqpConnection = null;
    let amqpChannel = null;
    let connSelfClosed = false;
    let chanelSelfClosed = false;
    let client = null;
    let socketCheckerId = null;
    let ID = (socket.id).toString();

    socket.on('error', (error) => {
        chatLogger('error', `socket error event: ${error.message}`);
        console.log(error);
    });

    socket.on('disconnect', (reason) => {
        let client = clients[socket.id];
        chatLogger('info', `socket with id: ${socket.id} disconnected. Socket user: ${JSON.stringify(clients[socket.id])}. Disconnect reason: ${reason}`);
        chatLogger('info', reason);

        console.log(reason);
        try {
            clearTimeout(socketCheckerId);
        } catch (err) {
            chatLogger('error', err.message);
        }
        if (typeof client !== "undefined") {
            if (client.client_type === "worker") {
                const workerCallsign = client.client_id;
                const tenantId = client.tenant_id;
                if (amqpChannel && typeof amqpChannel.close === 'function') {
                    try {
                        amqpChannel.close();
                        chanelSelfClosed = true;
                    } catch (err) {
                        chatLogger('error', err.message);
                    }

                }
                if (amqpConnection && typeof amqpConnection.close === 'function') {
                    try {
                        amqpConnection.close();
                        connSelfClosed = true;
                    } catch (err) {
                        chatLogger('error', err.message);
                    }

                }
                chatLogger('info', `unsubscribe from ${tenantId}_worker_${workerCallsign}`);
            }
            socketAction(client, 'user_disconnected');
            if (_.size(client.inroom) > 0) {
                async.each(client.inroom,
                    (roomId, callback) => {
                        const room = rooms[roomId];
                        if (_.contains((room.clients), client.id)) {
                            const personIndex = room.clients.indexOf(client.id);
                            room.clients.splice(personIndex, 1);
                            socket.leave(room.id);
                        }
                    },
                    (err) => {
                        chatLogger('error', err.message);
                    }
                );
            }
        }
        const o = _.findWhere(activeSockets, {'id': socket.id});
        activeSockets = _.without(activeSockets, o);
        delete clients[socket.id];
        client = null;
    });

    socket.emit('send_client_data', ID);

    socket.on('auth',  (data) => {
        console.log( uuid());
    });

    checkSocket();

    socket.on('save_client_data', (clientData) => {


        chatLogger('info', `client sent his auth data: ${JSON.stringify(clientData)}`);
        clientData = filterClientData(clientData);

        createClient(socket.id, clientData, (err, currClient) => {
            if (err) {
                chatLogger('error', `createClient error: ${err.message}`);
                return;
            }
            chatLogger('info', `create client result: ${JSON.stringify(currClient)}`);
            client = currClient;

            const tenantRoomArr = getTenantMainRoomsId(client.tenant_login, client.city_arr);
            socketAction(clientData, 'user_connected');
            //РџРѕРґРїРёС€РµРј РєР»РёРµРЅС‚Р° РЅР° РІСЃРµ РєРѕРјРЅР°С‚С‹ С‚РµРЅР°РЅС‚Р°
            async.each(tenantRoomArr,
                (tenantRoomLogin, callback) => {
                    if (!client.isInRoom(tenantRoomLogin)) {
                        const tenantRoom = rooms[tenantRoomLogin];
                        socket.join(tenantRoomLogin);
                        client.addRoom(tenantRoomLogin);
                        tenantRoom.addClient(client.id);
                        return callback(null, 1);
                    }
                    return callback(null, 1);
                }, (err) => {
                    if (err) {
                        chatLogger('error', `error of subscribing client ${JSON.stringify(client.id)} to all tenant rooms: ${err.message}`);
                        return;
                    }
                    chatLogger('info', `client ${JSON.stringify(client.id)} subscribed at all tenant rooms`);
                });
            if (client.client_type === "worker") {

                //РЎРѕР·РґР°РµРј РљРѕРјРЅР°С‚Сѓ РІРѕРґРёС‚РµР»СЏ
                const workerRoom = createWorkerRoom(client.tenant_login, client.city_arr[0], client.client_id);
                //РџРѕРґРїРёСЃС‹РІР°РµРј РІРѕРґРёС‚РµР»СЏ РЅР° СЃРІРѕСЋ РєРѕРјРЅР°С‚Сѓ
                if (!client.isInRoom(client.tenant_login + "_worker_" + client.client_id)) {
                    socket.join(workerRoom.id);
                    client.addRoom(workerRoom.id);
                    workerRoom.addClient(client.id);
                }
                //РџРѕР»СѓС‡Р°РµРј РІСЃРµС… Р°РєС‚РёРІРЅС‹С… РєР»РёРµРЅС‚РѕРІ РІ РіР»Р°РІРЅРѕРј С‡Р°С‚Рµ РіРѕСЂРѕРґР° РІРѕРґРёС‚РµР»СЏ
                const tenantRoom = rooms[client.tenant_login + "_city_" + client.city_arr[0]];
                const tenantActiveClients = tenantRoom.clients;
                async.each(tenantActiveClients,
                    (clientId, callback) => {
                        //РџРѕРґРїРёСЃС‹РІР°РµРј РІСЃРµС… СЃРѕС‚СЂСѓРґРЅРёРєРѕРІ РЅР° РІРѕРґРёС‚РµР»СЏ
                        _.find(clients, (key, value) => {
                            if (key.id === clientId) {
                                if (key.client_type === "user") {
                                    const currentUser = clients[clientId];
                                    if (!currentUser.isInRoom(workerRoom.id)) {
                                        if (io.sockets.connected[currentUser.id]) {
                                            const userSocket = io.sockets.connected[currentUser.id];
                                            userSocket.join(workerRoom.id);
                                            currentUser.addRoom(workerRoom.id);
                                            workerRoom.addClient(currentUser.id);
                                        }
                                    }
                                }
                            }
                        });
                    }, (err) => {
                        if (err) {
                            chatLogger('error', `error of subscribing all tenant users to worker room ${workerRoom.id}: ${err.message}`);
                            return;
                        }
                        chatLogger('info', `all tenant users subscribe at worker room ${workerRoom.id}`);
                    });
                workerQueueListener();
            }
            if (client.client_type === "user") {
                async.each(rooms,
                    (currentRoom, callback) => {
                        //РџРѕРґРїРёСЃС‹РІР°РµРј РґРёСЃРїРµС‚С‡РµСЂР° РЅР° РІСЃРµС… РІРѕРґРёС‚РµР»РµР№
                        if (currentRoom.owner_type === 'worker'
                            && currentRoom.tenant_login === client.tenant_login
                            && _.indexOf(client.city_arr, currentRoom.city_id) !== -1) {
                            const workerRoom = currentRoom;
                            socket.join(workerRoom.id);
                            client.addRoom(workerRoom.id);
                            workerRoom.addClient(client.id);
                        }
                    }, (err) => {
                        if (err) {
                            chatLogger('error', `error of subscribing tenant user ${client.id} to all worker rooms : ${err.message}`);
                            return;
                        }
                        chatLogger('info', `user ${client.id} subscribed to all worker rooms`);
                    });
            }
        });
    });

    socket.on('new_message', (msg) => {
        chatLogger('info', `new_message raw: ${JSON.stringify(msg)}`);
        msg.message = xssFilters.inHTMLData(msg.message);
        let receiverType = msg.receiver_type;
        let receiverId = msg.receiver_id;
        let cityId = msg.city_id;
        let message = truncate(msg.message, 1000);
        let messageTime = msg.message_time;
        let timeStamp = parseInt((new Date()).getTime());
        timeStamp = timeStamp.toString();
        let client, tenantLogin, new_message;
        client = clients[socket.id];
        if (typeof client === "undefined") {
            chatLogger('error', `socket ${socket.id} is unknown and will be disconnected`);
            socket.disconnect();
            return;
        }
        if (receiverType === "main_city_chat") {
            cityId = receiverId;
            tenantLogin = client.tenant_login;
            const roomId = tenantLogin + '_city_' + cityId;
            const room = rooms[roomId];
            const senderId = client.client_id;
            const senderType = client.client_type;
            new_message = {
                'city_id': cityId,
                'receiver_id': receiverId,
                'receiver_type': receiverType,
                'tenant_id': client.tenant_id,
                'tenant_domain': tenantLogin,
                'sender_id': client.client_id,
                'sender_type': client.client_type,
                'sender_role': client.client_role,
                'sender_f': client.client_f,
                'sender_i': client.client_i,
                'sender_o': client.client_o,
                'time': messageTime,
                'timestamp': timeStamp,
                'message': message
            };
            chatLogger('info', `new_message: ${JSON.stringify(new_message)}`);
            saveToChatHistory(roomId, new_message, false);
            io.sockets.in(room.id).emit("new_message", new_message);
            sendPushToCityChat(tenantLogin, cityId, client.client_f, client.client_i, client.client_o, messageTime, timeStamp, message, senderId, senderType)

        } else if (receiverType === "user") {
            client = clients[socket.id];
            tenantLogin = client.tenant_login;
            new_message = {
                'city_id': cityId,
                'tenant_id': client.tenant_id,
                'tenant_domain': tenantLogin,
                'receiver_id': receiverId,
                'receiver_type': receiverType,
                'sender_id': client.client_id,
                'sender_type': client.client_type,
                'sender_role': client.client_role,
                'sender_f': client.client_f,
                'sender_i': client.client_i,
                'sender_o': client.client_o,
                'time': messageTime,
                'timestamp': timeStamp,
                'message': message
            };
            chatLogger('info', `new_message: ${JSON.stringify(new_message)}`);
            sendAndSavePersonalRoomMessage(receiverId, client.client_id, cityId, tenantLogin, new_message);
        } else if (receiverType === "worker") {
            //РљС‚Рѕ РѕС‚РїСЂР°РІРёР» РјРµСЃСЃР°РіСѓ
            client = clients[socket.id];
            //Р›РѕРіРёРЅ С‚РµРЅР°РЅС‚Р°
            tenantLogin = client.tenant_login;
            //РРґ РіРѕСЂРѕРґР° СЃРѕРѕР±С‰РµРЅРёСЏ
            cityId = msg.city_id;
            //РџРѕР·С‹РІРЅРѕР№ РІРѕРґРёР»С‹
            const workerCallsign = msg.receiver_id;
            //РС‰РµРј РєРѕРјРЅР°С‚Сѓ
            const workerRoomId = tenantLogin + "_worker_" + workerCallsign;
            //Р•СЃР»Рё РєРѕРјРЅР°С‚С‹ РЅРµС‚ С‚Рѕ РЅР°РґРѕ РµРµ СЃРѕР·РґР°С‚СЊ Рё РїРѕРґРїРёСЃР°С‚СЊ РІСЃРµС… РґРёСЃРїРµС‚С‡РµСЂРѕРІ
            if (typeof rooms[workerRoomId] !== 'undefined') {
                sendMessageToWorker(tenantLogin, workerCallsign, msg, client);
                return;
            }
            let workerRoom = new Room(workerRoomId, tenantLogin, cityId, 'worker', false);
            rooms[workerRoomId] = workerRoom;
            //РџРѕР»СѓС‡Р°РµРј РІСЃРµС… Р°РєС‚РёРІРЅС‹С… РєР»РёРµРЅС‚РѕРІ РІ РіР»Р°РІРЅРѕРј С‡Р°С‚Рµ РіРѕСЂРѕРґР° РІРѕРґРёС‚РµР»СЏ
            let tenantRoom = rooms[tenantLogin + "_city_" + cityId];
            let tenantActiveClients = tenantRoom.clients;
            async.each(tenantActiveClients,
                (clientId, callback) => {
                    //РџРѕРґРїРёСЃС‹РІР°РµРј РІСЃРµС… РґРёСЃРїРµС‚С‡РµСЂРѕРІ РЅР° РІРѕРґРёС‚РµР»СЏ
                    _.find(clients, (key, value) => {
                        if (key.id === clientId) {
                            if (key.client_type === "user") {
                                const currentUser = clients[clientId];
                                if (!currentUser.isInRoom(workerRoom.id)) {
                                    const userSocket = io.sockets.connected[currentUser.id];
                                    userSocket.join(workerRoom.id);
                                    currentUser.addRoom(workerRoom.id);
                                    workerRoom.addClient(currentUser.id);
                                }

                            }
                        }
                    });
                    callback();
                }, (err) => {
                    if (err) {
                        chatLogger('error', err.message);
                    } else {
                        sendMessageToWorker(tenantLogin, workerCallsign, msg, client);
                    }
                });
        }
    });

    socket.on('get_last_messages', (msg) => {
        const tenantLogin = msg.tenant_login;
        const receiverId = msg.receiver_id;
        let receiverType = msg.receiver_type;
        const cityId = msg.city_id;
        if (receiverType === "main_city_chat") {
            receiverType = 'city';
        }
        if (receiverType === "user") {
            getPrivateLastMessages(tenantLogin, receiverId, msg);
        } else {
            const roomId = tenantLogin + '_' + receiverType + '_' + receiverId;
            const count = msg.count;
            getLastMessages(roomId, cityId, count);
        }

    });

    socket.on('get_history_messages', (msg) => {
        const tenantLogin = msg.tenant_login;
        const receiverId = msg.receiver_id;
        let receiverType = msg.receiver_type;
        if (receiverType === "main_city_chat") {
            receiverType = 'city';
        }
        if (receiverType === "user") {
            getPrivatePreviousMessages(tenantLogin, receiverId, msg);
        } else {
            const roomId = tenantLogin + '_' + receiverType + '_' + receiverId;
            const cityId = msg.city_id;
            const count = msg.count;
            const timestamp = msg.last_timestamp;
            getPreviousMessages(roomId, cityId, timestamp, count);
        }

    });

    socket.on('get_next_messages', (msg) => {
        const tenantLogin = msg.tenant_login;
        const receiverId = msg.receiver_id;
        let receiverType = msg.receiver_type;
        if (receiverType === "main_city_chat") {
            receiverType = 'city';
        }
        if (receiverType === "user") {
            getPrivateNextMessages(tenantLogin, receiverId, msg);
        } else {
            const roomId = tenantLogin + '_' + receiverType + '_' + receiverId;
            const cityId = msg.city_id;
            const count = msg.count;
            const timestamp = msg.last_timestamp;
            getNextMessages(roomId, cityId, timestamp, count);
        }

    });

    socket.on('delete_message', (msg) => {
        const timestamp = msg.timestamp;
        const message = msg.message;
        deleteMessage(timestamp, message);
    });

    socket.on('update_message', (msg) => {
        const timestamp = msg.timestamp;
        const newMessage = msg.new_message;
        const oldMessage = msg.old_message;
        findOneMessageAndUpdate(timestamp, oldMessage, newMessage);
    });

    socket.on('get_online_users', (msg) => {
        getOnlineUsers(msg.tenant_login, msg.city_arr);
    });

    socket.on('get_unreaded_message', (msg) => {
        chatLogger('info', `get_unreaded_message: ${JSON.stringify(msg)}`);
        getUnreadedMessages(msg.tenant_login, msg.client_type, msg.client_id);
    });

    socket.on('message_is_readed', (msg) => {
        let client = clients[socket.id];
        if (client) {
            const tenantLogin = msg.tenant_login;
            const cityId = msg.city_id;
            const senderId = msg.sender_id;
            const receiverId = msg.receiver_id;
            const receiverType = msg.receiver_type;
            if (receiverType === "user") {
                setUserMessageReaded(senderId, receiverId, cityId);
            } else if (receiverType === "worker") {
                const roomId = tenantLogin + "_worker_" + receiverId;
                setWorkerMessageReaded(roomId, receiverId, client.client_type);
            }
        }
    });


//////////////////////////////////Common function//////////////////////////////////////////////////////////////////////

    /**
     * Check client socket auth data
     * Need rewrite to good auth method
     */
    function checkSocket() {
        socketCheckerId = setTimeout(() => {
            disconnectBadSocket();
        }, 30000);
    }

    /**
     * Disconnect client if bad auth data
     */
    function disconnectBadSocket() {
        const currentUser = clients[socket.id];
        if (typeof currentUser === "undefined") {
            chatLogger('error', `socket with id: ${socket.id} is unauthorized and will be disconnected`);
           //socket.disconnect();
        }
    }

    /**
     * If worker connected, start listening his amqp queue and process messages
     */
    function workerQueueListener() {
        // Time to live for worker message queue in second. After this time queue will be deleted if there no active consumers.
        // For default it's 2 weeks
        const workerQueueExpireTime = 1209600;
        const q = `${client.tenant_id}_worker_${client.client_id}`;


        workerSocketLogger(q, 'info', `workerQueueListener->starting listening rabbitMQ queue: ${q}`);
        amqp.connect(`amqp://${config.RABBITMQ_MAIN_HOST}:${config.RABBITMQ_MAIN_PORT}?heartbeat=60`, (err, conn) => {
            if (err) {
                workerSocketLogger(q, 'error', 'workerQueueListener->create connection error: ' + err.message);
                return setTimeout(workerQueueListener, 2000);
            }
            conn.on("error", err => {
                if (err.message === "Connection closing") {
                    workerSocketLogger(q, 'info', `workerQueueListener->connection-> ${err.message}`);
                } else {
                    workerSocketLogger(q, 'error', `workerQueueListener->connection->Error ${err.message}`);
                }
            });
            conn.on("close", () => {
                if (connSelfClosed) {
                    workerSocketLogger(q, 'info', `workerQueueListener->connection self-closed`);
                } else {
                    const reconnectTimeInSeconds = 2;
                    workerSocketLogger(q, 'info', `workerQueueListener->connection closed, need to try reconnect after ${reconnectTimeInSeconds} seconds...`);
                    return setTimeout(workerQueueListener, reconnectTimeInSeconds * 1000);
                }
            });

            workerSocketLogger(q, 'info', "workerQueueListener->success create connection");
            amqpConnection = conn;

            conn.createChannel((err, ch) => {
                if (err) {
                    workerSocketLogger(q, 'error', `workerQueueListener->rabbitMQ->create channel->Error: ${err.message}`);
                    return;
                }
                ch.on("error", (err) => {
                    workerSocketLogger(q, 'error', `workerQueueListener->rabbitMQ->chanel->Error: ${err.message}`);
                });
                ch.on("close", () => {
                    workerSocketLogger(q, 'info', `workerQueueListener->rabbitMQ->channel closed`);
                });
                amqpChannel = ch;

                // socket change order_status
                socket.on("set_order_status", (result) => {
                    let workertData =  result; // filterClientData(result);

                    workerSocketLogger(q, 'info', "OrderSetOrderStatus->receive message" +  JSON.stringify(result));

                    // params status  order
                    let uu = (typeof workertData.uuid !== undefined) ? workertData.uuid :  uuid();
                    let order_id = ((typeof workertData.order_id !== undefined)) ? parseInt(workertData.order_id) : null;
                    let worker_login = ((typeof workertData.worker_login !== undefined)) ? parseInt(workertData.worker_login) : null;   //client.client_id;
                    let lang = (typeof workertData.lang === undefined ) ?  workertData.lang : null;
                    lang = getLang(lang);

                    /*console.log("Входные данные");
                    console.log(result);
                    console.log(order_id);
                    console.log(worker_login);*/
                    if(order_id !== null || worker_login !== null )
                    {
                        let param = {};

                        // накопленеи параметров запроса
                        if(typeof workertData.status_id  !== undefined){
                            param.status_id = workertData.status_id;
                        }

                        if(typeof workertData.time_to_client  !== undefined){
                            param.time_to_client = workertData.time_to_client;
                        }

                        if(typeof workertData.detail_order_data  !== undefined){
                            param.detail_order_data = workertData.detail_order_data;
                        }

                        if(typeof workertData.order_refuse  !== undefined){
                            param.order_refuse = workertData.order_refuse;
                        }

                        // предвариельная цена
                        if(typeof workertData.predv_price  !== undefined){
                            param.predv_price = workertData.predv_price;
                        }


                        let orderData = JSON.stringify({
                            uuid: uu,
                            type: 'order_event',
                            timestamp: Math.floor(Date.now() / 1000),
                            sender_service: "worker_service",
                            command: "update_order_data",
                            order_id: order_id,
                            tenant_id: client.tenant_id,
                            change_sender_id: worker_login,
                            last_update_time: null,
                            lang: lang,
                            params: param
                        });


                        try {
                            let order_queue = 'order_' +  order_id;
                            amqpChannel.assertQueue(order_queue, {durable: true }, function (err, ok) {
                                if(err){
                                    workerSocketLogger(q, 'info', `OrderSetOrderStatus->error create queue ${err.message}` );
                                }
                                let consume = (typeof ok.consumerCount !== undefined && (ok.consumerCount === parseInt(ok.consumerCount))) ? parseInt(ok.consumerCount) :  0;

                                if(consume >= 1){
                                    workerSocketLogger(q, 'info', "OrderSetOrderStatus->success send message" +   JSON.stringify(orderData));

                                    amqpChannel.sendToQueue(order_queue , new Buffer(orderData), {
                                        persistent: true,
                                        contentType: 'application/json',
                                        contentEncoding: 'utf-8'
                                    });
                                }else {
                                    let orderComplete = {
                                        uuid: uuid(),
                                        type: 'order_event',
                                        timestamp: Math.floor(Date.now() / 1000),
                                        sender_service: "engine_service",
                                        command: "complete_order",
                                        tenant_id: client.tenant_id,
                                        tenant_login: 'taxi',
                                        worker_callsign: worker_login,
                                        change_sender_id: '1',
                                        params: {
                                            order_id: order_id
                                        }
                                    };

                                    workerSocketLogger(q, 'info', "OrderSetOrderStatus->completeOrder->send message" +   JSON.stringify(orderData));
                                    socket.emit(orderComplete.type, orderComplete);

                                    socket.on(orderComplete.uuid, (result) => {
                                        workerSocketLogger(q, 'info', `OrderSetOrderStatus->completeOrder->Response for uuid : ${orderComplete.uuid} from worker device. Message recieve  ${result}`);

                                        amqpChannel.deleteQueue(order_queue, {ifUnused:true}, function (err_delete, success_delete) {
                                            if(err_delete){
                                                workerSocketLogger(q, 'info', `OrderSetOrderStatus->completeOrder->Error: not delete ${order_queue} reason ${err.message}`);
                                            }
                                            else{
                                                workerSocketLogger(q, 'info', `OrderSetOrderStatus->completeOrder->Sucess: delete ${order_queue}`);
                                            }
                                        });
                                    });
                                }
                            });
                        }catch (err) {
                            workerSocketLogger(q, 'error', 'workerQueueListener->Error of parsing socket message:' + err.message);
                        }

                    }else{
                        workerSocketLogger(q, 'info', "OrderSetOrderStatus->failure send message" +   JSON.stringify(result));
                    }

                });

                workerSocketLogger(q, 'info', "workerQueueListener->success create channel");
                const messageWorkTime = 1;
                ch.assertQueue(q, {
                    durable: true,
                    expires: workerQueueExpireTime * 1000
                }, (err, ok) => {
                    if (err) {
                        workerSocketLogger(q, 'error', `workerQueueListener->rabbitMQ->ch.assertQueue->Error: ${err.message}`);
                        return;
                    }

                    workerSocketLogger(q, 'info', "workerQueueListener->success assert queue");
                    ch.consume(q, (message) => {
                        if (!message) {
                            workerSocketLogger(q, 'error', `workerQueueListener->Bad socket message: ${message}`);
                            return;
                        }
                        workerSocketLogger(q, 'info', "workerQueueListener->New socket message!");
                        let taskObject;

                        try {
                            taskObject = JSON.parse(message.content.toString());
                        } catch (err) {
                            workerSocketLogger(q, 'error', 'workerQueueListener->Error of parsing socket message:' + err.message);
                        }

                        if (!taskObject) {
                            workerSocketLogger(q, 'error', `workerQueueListener->Bad taskObject: ${taskObject}`);
                            return;
                        }

                        workerSocketLogger(q, 'info', `workerQueueListener->taskObject: ${JSON.stringify(taskObject)}`);
                        let needEmit = true;
                        try {
                            if (taskObject.command === 'offer_order') {
                                taskObject.params.offer_sec = (taskObject.params.offer_sec + taskObject.timestamp) - Math.floor(Date.now() / 1000);
                                if (taskObject.params.offer_sec <= 0) {
                                    needEmit = false;
                                }
                            }
                            if (taskObject.command === 'confirm_preorder') {
                                taskObject.params.confirm_sec = (taskObject.params.confirm_sec + taskObject.timestamp) - Math.floor(Date.now() / 1000);
                                if (taskObject.params.confirm_sec <= 0) {
                                    needEmit = false;
                                }
                            }
                        } catch (err) {
                            workerSocketLogger(q, 'error', err.message);
                        }
                        let messageOrderId = taskObject.order_id ? taskObject.order_id : null;
                        messageOrderId = (taskObject.params && taskObject.params.order_id) ? taskObject.params.order_id : messageOrderId;
                        if (messageOrderId) {
                            orderLogger(messageOrderId, 'info', `new socket message at channel: ${q}`);
                            orderLogger(messageOrderId, 'info', taskObject);
                            orderLogger(messageOrderId, 'info', `need emit this message? ${needEmit}`);
                        }
                        workerSocketLogger(q, 'info', `workerQueueListener->need emit this message to worker device?: ${needEmit}`);
                        if (needEmit) {
                            workerSocketLogger(q, 'info', `message send to socket ${client.client_id}: ${message.content.toString()}`);
                            socket.emit(taskObject.type, taskObject);

                            workerSocketLogger(q, 'info', `workerQueueListener->messaged emit at socket id: ${socket.id}`);
                            if (messageOrderId) {
                                orderLogger(messageOrderId, 'info', `messaged emit at socket id: ${socket.id}`);
                            }
                            if (!taskObject.uuid) {
                                return;
                            }
                            socket.on(taskObject.uuid, (result) => {
                                workerSocketLogger(q, 'info', `workerQueueListener->New emit result at socket id: ${socket.id}`);
                                workerSocketLogger(q, 'info', `workerQueueListener->Response for uuid : ${taskObject.uuid} from worker device:`);
                                workerSocketLogger(q, 'info', result);
                                if (messageOrderId) {
                                    orderLogger(messageOrderId, 'info', `new emit result at socket id: ${socket.id}`);
                                    orderLogger(messageOrderId, 'info', `response for uuid: ${taskObject.uuid} from worker device:`);
                                    orderLogger(messageOrderId, 'info', result);
                                }
                                try {
                                    console.log("ответ от сервера ");
                                    console.log(taskObject);
                                    console.log("\n");
                                    amqpChannel.ack(message);
                                    workerSocketLogger(q, 'info', `workerQueueListener->Message ${taskObject.uuid} ack-ed from worker queue by device confirmation`);
                                    if (messageOrderId) {
                                        orderLogger(messageOrderId, 'info', `Message ${taskObject.uuid} ack-ed from worker queue by device confirmation`);
                                    }
                                } catch (err) {
                                    workerSocketLogger(q, 'error', `workerQueueListener->Error of ack-ent message from worker queue by device confirmation: ${err.message}`);
                                    if (messageOrderId) {
                                        orderLogger(messageOrderId, 'error', `message ${taskObject.uuid} ack-ed from worker queue by device confirmation`);
                                    }
                                }
                            });
                        } else {
                            setTimeout(() => {
                                try {
                                    amqpChannel.ack(message);
                                    workerSocketLogger(q, 'info', `workerQueueListener->message ${taskObject.uuid} ack-ed from worker queue by timeout`);
                                    if (messageOrderId) {
                                        orderLogger(messageOrderId, 'info', `message ${taskObject.uuid} ack-ed from worker queue by timeout`);
                                    }
                                } catch (err) {
                                    workerSocketLogger(q, 'error', `workerQueueListener->error of ack-ent message from worker queue by timeout: ${err.message}`);
                                    if (messageOrderId) {
                                        orderLogger(messageOrderId, 'error', `error of ack-ent message from worker queue by timeout: ${err.message}`);
                                    }
                                }
                            }, messageWorkTime * 1500);
                        }
                    }, {noAck: false});
                });
            });
        });
    }

    function getLang(lang){

        let Languages = ['ru', 'en', 'az', 'de', 'se', 'sr', 'ro'];
        let default_lang = 'en';

        if(lang === null){
            return default_lang;
        }

        let returnVal = null;
        for (let defLang of Languages) {
            if (defLang === lang) {
                returnVal = defLang;
                break;
            }
        }

        if(returnVal === null){
            returnVal = default_lang;
        }
        return returnVal;
    }


    /**
     * Get tenant main(city) chat rooms ids
     * @param {String} tenantLogin
     * @param {Array} cityArr
     * @returns {Array}
     */
    function getTenantMainRoomsId(tenantLogin, cityArr) {
        if (Array.isArray(cityArr)) {
            const tenantRooms = [];
            _.each(cityArr, (cityId, index, cityArr) => {
                const tenantRoomId = tenantLogin + "_city_" + cityId;
                if (typeof rooms[tenantRoomId] === 'undefined') {
                    rooms[tenantRoomId] = new Room(tenantRoomId, tenantLogin, cityId, 'city', false);
                }
                tenantRooms.push(tenantRoomId);
            });
            return tenantRooms;
        }
    }

    /**
     * Create worker chat room
     * @param {String} tenantLogin
     * @param {Number} cityId
     * @param {Number} workerId
     * @returns {*}
     */
    function createWorkerRoom(tenantLogin, cityId, workerId) {
        const workerRoomId = tenantLogin + "_worker_" + workerId;
        let room;
        if (typeof rooms[workerRoomId] === 'undefined') {
            room = new Room(workerRoomId, tenantLogin, cityId, 'worker', false);
            rooms[workerRoomId] = room;
        } else {
            room = rooms[workerRoomId];
        }
        return room;
    }


    /**
     * Send message to personal room users and save it to history(DB)
     * @param {Number} receiverId
     * @param {Number} senderId
     * @param {Number} cityId
     * @param {String} tenantLogin
     * @param {Object} new_message
     */
    function sendAndSavePersonalRoomMessage(receiverId, senderId, cityId, tenantLogin, new_message) {
        async.each(rooms,
            (room, callback) => {
                if ((room.id === tenantLogin + "_client_" + receiverId + "_" + senderId + "_city_" + cityId)
                    || (room.id === tenantLogin + "_client_" + senderId + "_" + receiverId + "_city_" + cityId)) {
                    if (parseInt(room.city_id) === parseInt(cityId) && room.private === true) {
                        const personalRoomId = room.id;
                        callback(personalRoomId);
                    } else {
                        callback();
                    }
                } else {
                    callback();
                }
            }, (personalRoomId) => {
                let privateRoom;
                if (personalRoomId) {
                    privateRoom = rooms[personalRoomId];
                } else {
                    personalRoomId = tenantLogin + "_client_" + receiverId + "_" + senderId + "_city_" + cityId;
                    privateRoom = new Room(personalRoomId, tenantLogin, cityId, 'user', true);
                    rooms[personalRoomId] = privateRoom;
                }
                //РџРѕРґРїРёСЃС‹РІР°РµРј С‚РµРєСѓС‰РµРіРѕ СЋР·РµСЂР° РЅР° РєРѕРјРЅР°С‚Сѓ, РµСЃР»Рё РµС‰Рµ РЅРµ РїРѕРґРїРёСЃР°РЅ
                const currentUser = clients[socket.id];
                if (!currentUser.isInRoom(privateRoom.id)) {
                    socket.join(privateRoom.id);
                    currentUser.addRoom(privateRoom.id);
                    privateRoom.addClient(currentUser.id);
                }
                let secondUserId;
                //РС‰РµРј РґСЂСѓРіРѕРіРѕ СЋР·РµСЂР° Рё РїРѕРґРїРёСЃС‹РІР°РµРј
                if (currentUser.client_id == receiverId) {
                    secondUserId = senderId;
                }
                if (currentUser.client_id == senderId) {
                    secondUserId = receiverId;
                }
                async.each(clients, (secondUser, callback) => {
                    if (secondUser.client_type === "user" && secondUser.client_id == secondUserId) {
                        if (!secondUser.isInRoom(privateRoom.id)) {
                            const secondUserSocket = io.sockets.connected[secondUser.id];
                            secondUserSocket.join(privateRoom.id);
                            secondUser.addRoom(privateRoom.id);
                            privateRoom.addClient(secondUser.id);
                            callback();
                        } else {
                            callback();
                        }

                    } else {
                        callback();
                    }

                }, (err) => {
                    if (err) {
                        chatLogger('error', err.message);
                    } else {
                        io.sockets.in(personalRoomId).emit("new_message", new_message);
                        saveToChatHistory(personalRoomId, new_message, true);
                    }
                });
            });
    }

    /**
     * Create client
     * @param {String} id  - socket id
     * @param {Object} clientData - client datga filtered object
     * @param {Function} resultCb
     */
    function createClient(id, clientData, resultCb) {
        const client = new Client(
            id,
            clientData.client_type,
            clientData.client_id,
            clientData.client_role,
            clientData.client_f,
            clientData.client_i,
            clientData.client_o,
            clientData.device,
            clientData.device_info,
            clientData.tenant_login,
            clientData.city_arr, (err, result) => {
                if (err) {
                    return resultCb(err);
                } else {
                    clients[client.id] = client;
                    return resultCb(null, client);
                }
            });
    }

    /**
     * Filtering client data (XSS + Case sense)
     * @param {Object} clientData
     * @returns {*}
     */
    function filterClientData(clientData) {
        if (typeof clientData === "object") {
            clientData = _.mapObject(clientData, (value, key) => {
                let newVal = value;
                if (typeof newVal === "string") {
                    newVal = xssFilters.inHTMLData(newVal);
                }
                let needToLowerKeys = ['client_type', 'client_role', 'device', 'tenant_login'];
                if (_.contains(needToLowerKeys, key) && typeof newVal === "string") {
                    newVal = newVal.toLowerCase();
                }
                return newVal;
            });
        }
        return clientData;
    }


    function getPrivatePreviousMessages(tenantLogin, receiverId, msg) {
        const currentClient = clients[socket.id];
        if (typeof currentClient === "undefined") {
            chatLogger('error', `socket ${socket.id} is unknown and will be disconnected`);
            socket.disconnect();
            return;
        }
        const senderId = currentClient.client_id;
        const cityId = msg.city_id;
        const count = msg.count;
        const timestamp = msg.last_timestamp;
        let roomId;
        if (typeof rooms[tenantLogin + "_client_" + receiverId + "_" + senderId + "_city_" + cityId] !== "undefined") {
            roomId = tenantLogin + "_client_" + receiverId + "_" + senderId;
        } else if (typeof rooms[tenantLogin + "_client_" + senderId + "_" + receiverId + "_city_" + cityId] !== "undefined") {
            roomId = tenantLogin + "_client_" + senderId + "_" + receiverId + "_city_" + cityId;
        }
        if (roomId !== null) {
            getPreviousMessages(roomId, cityId, timestamp, count);
            return;
        }
        const roomId1 = tenantLogin + "_client_" + receiverId + "_" + senderId + "_city_" + cityId;
        const roomId2 = tenantLogin + "_client_" + senderId + "_" + receiverId + "_city_" + cityId;
        ChatModel.findOne({'room_id': roomId1}, (err, chatRow) => {
            if (err) {
                chatLogger('error', err.message);
                return;
            }
            if (chatRow !== null) {
                getPreviousMessages(roomId1, cityId, timestamp, count);
            }
        });
        ChatModel.findOne({'room_id': roomId2}, (err, chatRow) => {
            if (err) {
                chatLogger('error', err.message);
                return;
            }
            if (chatRow !== null) {
                getPreviousMessages(roomId2, cityId, timestamp, count);
            }
        });
    }

    function getPrivateNextMessages(tenantLogin, receiverId, msg) {
        const currentClient = clients[socket.id];
        if (typeof currentClient === "undefined") {
            chatLogger('error', `socket ${socket.id} is unknown and will be disconnected`);
            socket.disconnect();
            return;
        }
        const senderId = currentClient.client_id;
        const cityId = msg.city_id;
        const count = msg.count;
        const timestamp = msg.last_timestamp;
        let roomId = null;
        if (typeof rooms[tenantLogin + "_client_" + receiverId + "_" + senderId + "_city_" + cityId] !== "undefined") {
            roomId = tenantLogin + "_client_" + receiverId + "_" + senderId + "_city_" + cityId;
        } else if (typeof rooms[tenantLogin + "_client_" + senderId + "_" + receiverId + "_city_" + cityId] !== "undefined") {
            roomId = tenantLogin + "_client_" + senderId + "_" + receiverId + "_city_" + cityId;
        }
        if (roomId !== null) {
            getNextMessages(roomId, cityId, timestamp, count);
            return;
        }
        const roomId1 = tenantLogin + "_client_" + receiverId + "_" + senderId + "_city_" + cityId;
        const roomId2 = tenantLogin + "_client_" + senderId + "_" + receiverId + "_city_" + cityId;
        ChatModel.findOne({'room_id': roomId1}, (err, chatRow) => {
            if (err) {
                chatLogger('error', err.message);
                return;
            }
            if (chatRow !== null) {
                getNextMessages(roomId1, cityId, timestamp, count);
            }
        });
        ChatModel.findOne({'room_id': roomId2}, (err, chatRow) => {
            if (err) {
                chatLogger('error', err.message);
                return;
            }
            if (chatRow !== null) {
                getNextMessages(roomId2, cityId, timestamp, count);
            }
        });
    }

    function getPrivateLastMessages(tenantLogin, receiverId, msg) {
        const currentClient = clients[socket.id];
        if (typeof currentClient === "undefined") {
            chatLogger('error', `socket ${socket.id} is unknown and will be disconnected`);
            socket.disconnect();
            return;
        }
        const senderId = currentClient.client_id;
        const cityId = msg.city_id;
        const count = msg.count;
        let roomId = null;
        if (typeof rooms[tenantLogin + "_client_" + receiverId + "_" + senderId + "_city_" + cityId] !== "undefined") {
            roomId = tenantLogin + "_client_" + receiverId + "_" + senderId + "_city_" + cityId;
        } else if (typeof rooms[tenantLogin + "_client_" + senderId + "_" + receiverId + "_city_" + cityId] !== "undefined") {
            roomId = tenantLogin + "_client_" + senderId + "_" + receiverId + "_city_" + cityId;
        }
        if (roomId !== null) {
            getLastMessages(roomId, cityId, count);
            return;
        }
        const roomId1 = tenantLogin + "_client_" + receiverId + "_" + senderId + "_city_" + cityId;
        const roomId2 = tenantLogin + "_client_" + senderId + "_" + receiverId + "_city_" + cityId;
        ChatModel.findOne({'room_id': roomId1}, (err, chatRow) => {
            if (err) {
                chatLogger('error', err.message);
                return;
            }
            if (chatRow !== null) {
                getLastMessages(roomId1, cityId, count);
            }
        });
        ChatModel.findOne({'room_id': roomId2}, (err, chatRow) => {
            if (err) {
                chatLogger('error', err.message);
                return;
            }
            if (chatRow !== null) {
                getLastMessages(roomId2, cityId, count);
            }
        });
    }

    function sendMessageToWorker(tenantLogin, workerCallsign, msg, client) {
        const roomId = tenantLogin + '_worker_' + workerCallsign;
        const room = rooms[roomId];
        const cityId = msg.city_id;
        const receiverType = msg.receiver_type;
        const receiverId = msg.receiver_id;
        const message = msg.message;
        const messageTime = msg.message_time;
        let timeStamp = parseInt((new Date()).getTime());
        timeStamp = timeStamp.toString();
        const new_message = {
            'city_id': cityId,
            'tenant_id': client.tenant_id,
            'tenant_domain': tenantLogin,
            'receiver_id': receiverId,
            'receiver_type': receiverType,
            'sender_id': client.client_id,
            'sender_type': client.client_type,
            'sender_role': client.client_role,
            'sender_f': client.client_f,
            'sender_i': client.client_i,
            'sender_o': client.client_o,
            'time': messageTime,
            'timestamp': timeStamp,
            'message': message
        };
        chatLogger('info', `new_message: ${JSON.stringify(new_message)}`);
        saveToChatHistory(roomId, new_message, true);
        io.sockets.in(room.id).emit("new_message", new_message);
        //РћС‚РїСЂР°РІР»СЏРµРј СЃРѕРѕР±С‰РµРЅРёРµ РµСЃР»Рё СЌС‚Рѕ РЅРµ СЃРІРѕРµ Р¶Рµ СЃРѕРѕР±С‰РµРЅРёРµ
        if (receiverType === client.client_type && receiverId == client.client_id) {

        } else {
            sendPushToWorker(tenantLogin, receiverId, client.client_f, client.client_i, client.client_o, messageTime, timeStamp, message);
        }
    }

    function sendPushToWorker(tenant_login, worker_callsign, sender_f, sender_i, sender_o, time, timestamp, message) {
        request.post({
            url: config.SERVICE_PUSH_URL + "notification/send_push_message_to_worker",
            form: {
                tenant_login: tenant_login,
                worker_callsign: worker_callsign,
                sender_f: sender_f,
                sender_i: sender_i,
                sender_o: sender_o,
                time: time,
                timestamp: timestamp,
                message: message
            }
        }, (error, response, body) => {
            if (!error && parseInt(response.statusCode) === 200) {
                try {
                    const info = JSON.parse(body);
                    if (parseInt(info.result) !== 1) {
                        chatLogger('error', "РћС€РёР±РєР° РѕС‚РїСЂР°РІРєРё Р·Р°РїСЂРѕСЃР° РЅР° РѕС‚РїСЂР°РІРєСѓ РїСѓС€Р° РІРѕРґРёС‚РµР»СЋ");
                        chatLogger('error', response.statusCode);
                        chatLogger('error', body);
                    }
                } catch (err) {
                    chatLogger('error', "РћС€РёР±РєР° Jresponseson.parse");
                    chatLogger('error', body);
                    throw err;
                }
            } else {
                chatLogger('error', response.body);
            }
        });
    }

    function sendPushToCityChat(tenant_login, city_id, sender_f, sender_i, sender_o, time, timestamp, message, senderId, senderType) {
        request.post({
            url: config.SERVICE_PUSH_URL + "notification/send_push_message_to_all_workers",
            form: {
                tenant_login: tenant_login,
                city_id: city_id,
                sender_f: sender_f,
                sender_i: sender_i,
                sender_o: sender_o,
                time: time,
                timestamp: timestamp,
                message: message,
                sender_id: senderId,
                sender_type: senderType
            }
        }, (error, response, body) => {
            if (!error && parseInt(response.statusCode) === 200) {
                try {
                    const info = JSON.parse(body);
                    if (parseInt(info.result) !== 1) {
                        chatLogger('error', "РћС€РёР±РєР° РѕС‚РїСЂР°РІРєРё Р·Р°РїСЂРѕСЃР° РЅР° РѕС‚РїСЂР°РІРєСѓ РїСѓС€Р° РІ С‡Р°С‚ РіРѕСЂРѕРґР°");
                        chatLogger('error', response.statusCode);
                        chatLogger('error', body);
                    }
                } catch (err) {
                    chatLogger('error', "РћС€РёР±РєР° Json.parse");
                    chatLogger('error', body);
                    throw err;
                }

            } else {
                chatLogger('error', body);
            }
        });
    }

    function saveToChatHistory(room_id, msg, is_private) {
        let is_readed = true;
        if (is_private) {
            is_readed = false;
        }
        let uuidVal = uuid();
        const chatMessage = new ChatModel({
            room_id: room_id,
            is_private: is_private,
            is_readed: is_readed,
            tenant_id: msg.tenant_id,
            tenant_domain: msg.tenant_domain,
            city_id: msg.city_id,
            receiver_id: msg.receiver_id,
            receiver_type: msg.receiver_type,
            sender_type: msg.sender_type,
            sender_role: msg.sender_role,
            sender_id: msg.sender_id,
            sender_f: msg.sender_f,
            sender_i: msg.sender_i,
            sender_o: msg.sender_o,
            sender_time: msg.time,
            timestamp: msg.timestamp,
            message: msg.message,
            uuid: uuidVal
        });
        chatMessage.save((err) => {
            if (err) {
                chatLogger('error', err.message);
            } else {
                //Check that message is read by dispatcher in dispatcher chat
                if (msg.sender_type === "worker" && msg.receiver_type === "worker") {
                    messageUnreadedWorker(uuidVal);
                }
            }
        });
    }

    function getLastMessages(room_id, city_id, count) {
        ChatModel.find({
            'room_id': room_id,
            'city_id': city_id
        }).limit(count).sort({timestamp: -1}).exec((err, messages) => {
            if (err) {
                chatLogger('error', err.message);
            } else {
                socket.emit('last_messages', messages);
            }
        });
    }

    function getPreviousMessages(room_id, city_id, timestamp, count) {
        ChatModel.find({
            'room_id': room_id,
            'city_id': city_id,
            'timestamp': {$lt: timestamp}
        }).limit(count).sort({timestamp: -1}).exec((err, messages) => {
            if (err) {
                chatLogger('error', err.message);
            } else {
                socket.emit('last_messages', messages);
            }
        });
    }

    function getNextMessages(room_id, city_id, timestamp, count) {
        ChatModel.find({
            'room_id': room_id,
            'city_id': city_id,
            'timestamp': {$gt: timestamp}
        }).limit(count).sort({timestamp: -1}).exec((err, messages) => {
            if (err) {
                chatLogger('error', err.message);
            } else {
                socket.emit('next_messages', messages);
            }
        });
    }

    function setWorkerMessageReaded(roomId, receiverId, whoReadsType) {
        let senderType = 'worker';
        if (whoReadsType === "worker") {
            senderType = 'user';
        }
        const conditions = {
            room_id: roomId,
            is_private: true,
            receiver_type: 'worker',
            sender_type: senderType,
            receiver_id: receiverId,
            is_readed: false
        }
            , update = {is_readed: true}
            , options = {multi: true};
        ChatModel.update(conditions, update, options, (err, numAffected) => {
            if (err) {
                chatLogger('error', err.message);
            } else {
            }
        });
    }

    function setUserMessageReaded(senderId, receiverId, cityId) {
        const conditions = {
            is_private: true,
            city_id: cityId,
            sender_type: 'user',
            sender_id: senderId,
            receiver_type: 'user',
            receiver_id: receiverId,
            is_readed: false
        }
            , update = {is_readed: true}
            , options = {multi: true};
        ChatModel.update(conditions, update, options, (err, numAffected) => {
            if (err) {
                chatLogger('error', err.message);
            } else {

            }
        });
    }

    function getUnreadedMessages(tenant_login, client_type, client_id) {
        if (client_type === "user") {
            getUserUnreadedMessages(tenant_login, client_id);
        } else if (client_type === "worker") {
            getWorkerUnreadedMessages(tenant_login, client_id);
        }
    }

    function getUserUnreadedMessages(tenant_login, client_id) {
        ChatModel.find({
            'is_readed': false,
            'is_private': true,
            'sender_type': 'user',
            'receiver_type': 'user',
            'receiver_id': client_id
        }).sort({timestamp: -1}).exec((err, messages) => {
            if (err) {
                chatLogger('error', err.message);
            } else {
                socket.emit('unreaded_message', messages);
            }
        });
    }

    function getWorkerUnreadedMessages(tenant_login, worker_id) {
        const room_id = tenant_login + '_worker_' + worker_id;
        ChatModel.find({
            'room_id': room_id,
            'is_readed': false,
            'is_private': true,
            'receiver_type': 'worker',
            'receiver_id': worker_id,
            'sender_type': 'user',
        }).sort({timestamp: -1}).exec((err, messages) => {
            if (err) {
                chatLogger('error', err.message);
            } else {
                socket.emit('unreaded_message', messages);
            }
        });
    }

    function deleteMessage(timestamp, message) {
        ChatModel
            .remove({timestamp: timestamp, message: message}, (err) => {
                if (err) {
                    chatLogger('error', err.message);
                    socket.emit('message_delete_result', {"timestamp": timestamp, "message": message, "result": 0});
                } else {
                    socket.emit('message_delete_result', {"timestamp": timestamp, "message": message, "result": 1});
                }

            });
    }

    function findOneMessageAndUpdate(timestamp, oldMessage, newMessage) {
        newMessage = xssFilters.inHTMLData(newMessage);
        const queryFind = {timestamp: timestamp, message: oldMessage};
        ChatModel.findOneAndUpdate(queryFind, {$set: {message: newMessage}}, (err, result) => {
            if (err) {
                chatLogger('error', err.message);
                socket.emit('message_update_result', {"timestamp": timestamp, "message": newMessage, "result": 0});
            } else {
                socket.emit('message_update_result', {"timestamp": timestamp, "message": newMessage, "result": 1});
            }
        });
    }

    function socketAction(client, actionName) {
        const clientType = client.client_type;
        const clientId = client.client_id;
        const clientCityArr = client.city_arr;
        const tenantLogin = client.tenant_login;
        const msg = [];
        _.each(clientCityArr, (clientCityId) => {
            const msgData = {
                'client_id': clientId,
                'client_type': clientType,
                'client_city': clientCityId
            };
            msg.push(msgData);
        });

        const tenantUsers = [];
        async.each(clients, (currClient, callback) => {
            if (currClient.tenant_login === tenantLogin) {
                tenantUsers.push(currClient);
            }
            callback();
        }, (err) => {
            if (err) {
                chatLogger('error', err.message);
                return;
            }
            const onlineUsers = [];
            if (tenantUsers.length > 0) {
                async.each(tenantUsers, (tenantUser, callback) => {
                    _.each(tenantUser.city_arr, (clientCityId) => {
                        if (tenantUser.isInCity(clientCityId)) {
                            const clientData = {
                                'id': tenantUser.id,
                                'client_id': tenantUser.client_id,
                                'client_type': tenantUser.client_type,
                                'client_city': clientCityId
                            };
                            onlineUsers.push(clientData);
                        }
                    });
                    callback();
                }, (err) => {
                    if (err) {
                        chatLogger('error', err.message);
                    } else {
                        const uniqueUsers = _.uniq(onlineUsers, (item) => {
                            return item.client_id + item.client_type + item.client_city;
                        });

                        if (uniqueUsers.length > 0) {
                            const finishUsers = [];
                            _.each(uniqueUsers, (uniqueUser) => {
                                if (clientCityArr.indexOf(uniqueUser.client_city) !== -1) {
                                    finishUsers.push(uniqueUser);
                                }
                            });

                            const sendedS = [];
                            _.each(finishUsers, (finishUser) => {
                                if (sendedS.indexOf(finishUser.id) === -1) {
                                    sendedS.push(finishUser.id);
                                    io.to(finishUser.id).emit(actionName, msg);
                                }
                            });
                        }

                    }
                });
            }

        });
    }

    function getOnlineUsers(tenantLogin, cityArr) {
        const tenantUsers = [];
        async.each(clients, (currClient, callback) => {
            if (currClient.tenant_login === tenantLogin) {
                tenantUsers.push(currClient);
            }
            callback();
        }, (err) => {
            if (err) {
                chatLogger('error', err.message);
            } else {
                const onlineUsers = [];
                if (tenantUsers.length > 0) {
                    async.each(tenantUsers, (tenantUser, callback) => {
                        _.each(tenantUser.city_arr, (clientCityId) => {
                            if (tenantUser.isInCity(clientCityId)) {
                                const clientData = {
                                    'client_id': tenantUser.client_id,
                                    'client_type': tenantUser.client_type,
                                    'client_city': clientCityId
                                };
                                onlineUsers.push(clientData);
                            }
                        });
                        callback();
                    }, (err) => {
                        if (err) {
                            chatLogger('error', err.message);
                        } else {
                            const uniqueUsers = _.uniq(onlineUsers, (item) => {
                                return item.client_id + item.client_type + item.client_city;
                            });
                            if (uniqueUsers.length > 0) {
                                const finishUsers = [];
                                _.each(uniqueUsers, (uniqueUser) => {
                                    if (cityArr.indexOf(uniqueUser.client_city) !== -1) {
                                        finishUsers.push(uniqueUser);
                                    }
                                });
                                socket.emit('online_users', finishUsers);
                            }

                        }
                    });
                }
            }
        });
    }

    function truncate(str, maxlength) {
        return (str.length > maxlength) ?
            str.slice(0, maxlength - 3) + '...' : str;
    }

});

process.on('uncaughtException', (err) => {
    chatLogger('error', 'Critical error: ' + err.message);
    chatLogger('error', 'Error stack: ' + err.stack);
    const transporter = nodemailer.createTransport({
        service: config.MAIL_SUPPORT_PROVIDER,
        auth: {
            user: config.MAIL_SUPPORT_LOGIN,
            pass: config.MAIL_SUPPORT_PASSWORD
        }
    });
    const mailOptions = {
        from: config.MAIL_SUPPORT_LOGIN,
        to: config.MAIL_SUPPORT_LOGIN,
        subject: config.MAIL_SUPPORT_SUBJECT_ERROR,
        text: err.message,
        html: err.message + "  |  " + err.stack
    };
    transporter.sendMail(mailOptions, (err, info) => {
        if (err) {
            chatLogger('error', err.message);
        } else {
            chatLogger('error', 'Message sent: ' + info.response);
        }
    });
});

process.on('warning', (err) => {
    chatLogger('error', 'Warning: ' + err.message);
    chatLogger('error', 'Warning stack: ' + err.stack);
    const transporter = nodemailer.createTransport({
        service: config.MAIL_SUPPORT_PROVIDER,
        auth: {
            user: config.MAIL_SUPPORT_LOGIN,
            pass: config.MAIL_SUPPORT_PASSWORD
        }
    });
    const mailOptions = {
        from: config.MAIL_SUPPORT_LOGIN,
        to: config.MAIL_SUPPORT_LOGIN,
        subject: config.MAIL_SUPPORT_SUBJECT_WARNING,
        text: err.message,
        html: err.message + "  |  " + err.stack
    };
    transporter.sendMail(mailOptions, (err, info) => {
        if (err) {
            chatLogger('error', err.message);
        } else {
            chatLogger('error', 'Message sent: ' + info.response);
        }
    });
});




