'use strict';

/**
 * herecom place to elastic formatter
 * @param data
 * @returns {{docs: Array, meta: {scores: Array}}}
 */
function herecomPlaceToElasticFormatter(data) {
    var rawResultArray = [];
    var results = {
        "docs": [],
        "meta": {
            "scores": []
        }
    };
    if (data.results) {
        var resultData = data.results;
        if (resultData.items) {
            rawResultArray = resultData.items;
        }
    }
    if (rawResultArray.length === 0) {
        return results;
    }

    rawResultArray.forEach(function (item, i, rawResultArray) {
        var geoItem = item;
        var lat = null;
        var lon = null;
        var number = '';
        var street = '';
        var name = '';
        var admin1 = '';
        var suburb = '';
        var locality = '';
        var alpha3 = '';
        var admin0 = '';
        var category = [];
        var _id = 1;
        var _type = "osmnode";
        var _score = 1;
        if (geoItem.position) {
            lat = geoItem.position[0];
            lon = geoItem.position[1];
        }
        if (geoItem.vicinity) {
            street = geoItem.vicinity;
            street = street.split('\n');
            street = street[0];
        }
        if (geoItem.title) {
            name = geoItem.title;
        }

        var formattedItem = {
            "center_point": {
                "lon": lon,
                "lat": lat
            },
            "address": {
                "number": number,
                "street": street
            },
            "name": {
                "default": name
            },
            "suburb": suburb,
            "admin1": admin1,
            "locality": locality,
            "alpha3": alpha3,
            "admin0": admin0,
            "category": category,
            "_id": _id,
            "_type": _type,
            "_score": _score
        };
        results.docs.push(formattedItem);
        results.meta.scores.push(1);
    });
    return results;
}

module.exports = herecomPlaceToElasticFormatter;