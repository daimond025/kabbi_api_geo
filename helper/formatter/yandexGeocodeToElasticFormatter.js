'use strict';

/**
 * yandex geocode to elastic formatter
 * @param data
 * @returns {{docs: Array, meta: {scores: Array}}}
 */
function yandexGeocodeToElasticFormatter(data) {
    var rawResultArray = [];
    var results = {
        "docs": [],
        "meta": {
            "scores": []
        }
    };
    if (data.response) {
        if (data.response.GeoObjectCollection) {
            var collection = data.response.GeoObjectCollection;
            if (collection.featureMember) {
                rawResultArray = collection.featureMember;
            }
        }
        if (rawResultArray.length === 0) {
            return results;
        }
        rawResultArray.forEach(function (item, i, rawResultArray) {
            var geoItem = item.GeoObject;
            var lat = null;
            var lon = null;
            var number = '';
            var street = '';
            var name = '';
            var admin1 = '';
            var suburb = '';
            var locality = '';
            var alpha3 = '';
            var admin0 = '';
            var category = [];
            var _id = 1;
            var _type = "";
            var _score = 1;
            if (geoItem.name) {
                name = geoItem.name;
            }
            if (geoItem.Point) {
                var pointLine = geoItem.Point.pos;
                var pointArray = pointLine.split(" ");
                lon = pointArray[0];
                lat = pointArray[1];
            }
            if (geoItem.metaDataProperty) {
                if (geoItem.metaDataProperty.GeocoderMetaData) {
                    var meta = geoItem.metaDataProperty.GeocoderMetaData;
                    var kind = meta.kind;
                    if (kind === "house" || kind === "street") {
                        _type = "osmaddress";
                    }
                    if (meta.AddressDetails) {
                        var thoroughfare, premise;
                        var addressData = meta.AddressDetails;
                        if (addressData.Country) {
                            var countryLevel = addressData.Country;
                            if (countryLevel.CountryNameCode) {
                                alpha3 = countryLevel.CountryNameCode;
                            }
                            if (countryLevel.CountryName) {
                                admin0 = countryLevel.CountryName;
                            }
                            if (countryLevel.Locality) {
                                countryLevel = countryLevel.Locality;
                            }
                            if (countryLevel.Thoroughfare) {
                                thoroughfare = countryLevel.Thoroughfare;
                                street = typeof thoroughfare.ThoroughfareName === "undefined" ? "" : thoroughfare.ThoroughfareName;
                                if (thoroughfare.Premise) {
                                    premise = thoroughfare.Premise;
                                    number = typeof premise.PremiseNumber === "undefined" ? "" : premise.PremiseNumber;
                                    if (premise.PremiseName) {
                                        street = street + ' ' + premise.PremiseName;
                                    }

                                }
                            }
                            if (countryLevel.AdministrativeArea) {
                                var adminLevel = countryLevel.AdministrativeArea;
                                if (adminLevel.SubAdministrativeArea) {
                                    adminLevel = adminLevel.SubAdministrativeArea;
                                }
                                if (adminLevel.Locality) {
                                    var localityLevel = adminLevel.Locality;
                                    if (localityLevel.LocalityName) {
                                        locality = localityLevel.LocalityName;
                                    }
                                    var dependentLocality;
                                    if (localityLevel.DependentLocality) {
                                        dependentLocality = localityLevel.DependentLocality;
                                        if (dependentLocality.DependentLocalityName) {
                                            suburb = dependentLocality.DependentLocalityName;
                                        }
                                        if (dependentLocality.Premise) {
                                            premise = dependentLocality.Premise;
                                            number = typeof premise.PremiseNumber === "undefined" ? "" : premise.PremiseNumber;
                                            if (premise.PremiseName) {
                                                street = street + ' ' + premise.PremiseName;
                                            }
                                        }
                                        if (dependentLocality.Thoroughfare) {
                                            thoroughfare = dependentLocality.Thoroughfare;
                                            street = typeof thoroughfare.ThoroughfareName === "undefined" ? "" : thoroughfare.ThoroughfareName;
                                            if (thoroughfare.Premise) {
                                                premise = thoroughfare.Premise;
                                                number = typeof premise.PremiseNumber === "undefined" ? "" : premise.PremiseNumber;
                                                if (premise.PremiseName) {
                                                    street = street + ' ' + premise.PremiseName;
                                                }

                                            }
                                        }
                                        if (dependentLocality.DependentLocality) {
                                            dependentLocality = dependentLocality.DependentLocality;
                                            if (dependentLocality.DependentLocalityName) {
                                                suburb = suburb + ', ' + dependentLocality.DependentLocalityName;
                                            }
                                            if (dependentLocality.Premise) {
                                                premise = dependentLocality.Premise;
                                                number = typeof premise.PremiseNumber === "undefined" ? "" : premise.PremiseNumber;
                                                if (premise.PremiseName) {
                                                    street = street + ' ' + premise.PremiseName;
                                                }
                                            }
                                            if (dependentLocality.Thoroughfare) {
                                                thoroughfare = dependentLocality.Thoroughfare;
                                                street = typeof thoroughfare.ThoroughfareName === "undefined" ? "" : thoroughfare.ThoroughfareName;
                                                if (thoroughfare.Premise) {
                                                    premise = thoroughfare.Premise;
                                                    number = typeof premise.PremiseNumber === "undefined" ? "" : premise.PremiseNumber;
                                                    if (premise.PremiseName) {
                                                        street = street + ' ' + premise.PremiseName;
                                                    }

                                                }
                                            }

                                            if (dependentLocality.DependentLocality) {
                                                dependentLocality = dependentLocality.DependentLocality;
                                                if (dependentLocality.DependentLocalityName) {
                                                    suburb = suburb + ', ' + dependentLocality.DependentLocalityName;
                                                }
                                                if (dependentLocality.Premise) {
                                                    premise = dependentLocality.Premise;
                                                    number = typeof premise.PremiseNumber === "undefined" ? "" : premise.PremiseNumber;
                                                    if (premise.PremiseName) {
                                                        street = street + ' ' + premise.PremiseName;
                                                    }
                                                }
                                                if (dependentLocality.Thoroughfare) {
                                                    thoroughfare = dependentLocality.Thoroughfare;
                                                    street = typeof thoroughfare.ThoroughfareName === "undefined" ? "" : thoroughfare.ThoroughfareName;
                                                    if (thoroughfare.Premise) {
                                                        premise = thoroughfare.Premise;
                                                        number = typeof premise.PremiseNumber === "undefined" ? "" : premise.PremiseNumber;
                                                        if (premise.PremiseName) {
                                                            street = street + ' ' + premise.PremiseName;
                                                        }

                                                    }
                                                }
                                            }
                                        }
                                    } else if (localityLevel.Thoroughfare) {
                                        thoroughfare = localityLevel.Thoroughfare;
                                        street = typeof thoroughfare.ThoroughfareName === "undefined" ? "" : thoroughfare.ThoroughfareName;
                                        if (thoroughfare.Premise) {
                                            premise = thoroughfare.Premise;
                                            number = typeof premise.PremiseNumber === "undefined" ? "" : premise.PremiseNumber;
                                            if (premise.PremiseName) {
                                                street = street + ' ' + premise.PremiseName;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            var formattedItem = {
                "center_point": {
                    "lon": lon,
                    "lat": lat
                },
                "address": {
                    "number": number,
                    "street": street
                },
                "name": {
                    "default": name
                },
                "suburb": suburb,
                "admin1": admin1,
                "locality": locality,
                "alpha3": alpha3,
                "admin0": admin0,
                "category": category,
                "_id": _id,
                "_type": _type,
                "_score": _score
            };
            results.docs.push(formattedItem);
            results.meta.scores.push(1);
        });
    }
    return results;
}

module.exports = yandexGeocodeToElasticFormatter;