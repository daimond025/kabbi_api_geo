'use strict';

/**
 * gootax formatter
 * @param resObject
 * @returns {Array}
 */
function gootaxFormatter(resObject) {
    var formattedAnswer = [];
    if (resObject && resObject.length) {
        for (var i in resObject) {
            var rawRow = resObject[i];
            var newRow = rawToNewFormat(rawRow);
            formattedAnswer.push(newRow);
        }

    }
    return formattedAnswer;
}

/**
 * format row
 * @param oldObject
 * @returns {{address: {city: *, comment: string, house: string, housing: string, label: string, lat: string, lon: string, porch: string, street: string}, distance: string, type: string}}
 */
function rawToNewFormat(oldObject) {
    var city = (typeof oldObject.city !== 'undefined') ? oldObject.city : '';
    if (city === '') {
        city = (typeof oldObject.locality !== 'undefined') ? oldObject.locality : '';
        if (city === '' && typeof oldObject.region !== 'undefined' && oldObject.region !== '' && oldObject.region !== null) {
            city = oldObject.region;
        }
    }
    var suburb = (typeof oldObject.suburb !== 'undefined') ? oldObject.suburb : '';
    var place_id = (typeof oldObject._id !== 'undefined') ? oldObject._id : '';
    var comment = (typeof oldObject.category !== 'undefined') ? oldObject.category : '';
    var house = (typeof oldObject.housenumber !== 'undefined') ? oldObject.housenumber : '';
    var distance = (typeof oldObject.distance !== 'undefined') ? oldObject.distance : '';
    var lat = (typeof oldObject.center_point.lat !== 'undefined') ? oldObject.center_point.lat : '';
    var lon = (typeof oldObject.center_point.lon !== 'undefined') ? oldObject.center_point.lon : '';
    var street = (typeof oldObject.street !== 'undefined') ? oldObject.street : '';
    var typeaddress = "public_place";
    var rawTypeAddress = (typeof oldObject._type !== 'undefined') ? oldObject._type : '';
    if (rawTypeAddress === "osmaddress") {
        typeaddress = "house";
    }
    var publicLabelRaw = (typeof oldObject.name.default !== 'undefined') ? oldObject.name.default : '';

    var labelPlace = '';
    if (street !== '' && street !== null) {
        labelPlace = street;
    }
    if (suburb !== '' && suburb !== null) {
        labelPlace = labelPlace + " (" + suburb + ")";
        street = street + " (" + suburb + ")";
    }
    if (house !== '' && house !== null) {
        labelPlace = labelPlace + " " + house;
    }
    if (typeaddress === "public_place") {
        if (publicLabelRaw !== '' && publicLabelRaw !== null) {
            if (labelPlace !== '' && labelPlace !== null) {
                labelPlace = publicLabelRaw + " | " + labelPlace;
            }
            else {
                labelPlace = publicLabelRaw;
            }

        }
    }
    /*
     Вырежем кавычки из строк
     \x27 - это шестнадцатеричный код одинарной кавычки (')
     
     \x22 - код двойной кавычки (")
     
     */
    var r1 = new RegExp("\x27+", "g");
    var r2 = new RegExp("\x22+", "g");

    city = city.replace(r1, "");
    city = city.replace(r2, "");

    labelPlace = labelPlace.replace(r1, "");
    labelPlace = labelPlace.replace(r2, "");

    street = street.replace(r1, "");
    street = street.replace(r2, "");

    return {
        address: {
            city: city,
            comment: comment,
            house: house,
            housing: '',
            label: labelPlace,
            lat: lat,
            lon: lon,
            placeId: place_id,
            porch: '',
            street: street
        },
        distance: distance,
        type: typeaddress
    };
}

module.exports = gootaxFormatter;
