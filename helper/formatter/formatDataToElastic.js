'use strict';

/**
 * format data to elastic
 * @param lat
 * @param lon
 * @param number
 * @param street
 * @param name
 * @param suburb
 * @param admin1
 * @param locality
 * @param alpha3
 * @param admin0
 * @param category
 * @param _id
 * @param _type
 * @param _score
 * @returns {{center_point: {lon: *, lat: *}, address: {number: *, street: *}, name: {default: *}, suburb: *, admin1: *, locality: *, alpha3: *, admin0: *, category: *, _id: *, _type: *, _score: *}}
 */
function formatDataToElastic(lat, lon, number, street, name, suburb, admin1, locality, alpha3, admin0, category, _id, _type, _score) {
    return {
        'center_point': {
            'lon': lon,
            'lat': lat
        },
        'address': {
            'number': number,
            'street': street
        },
        'name': {
            'default': name
        },
        'suburb': suburb,
        'admin1': admin1,
        'locality': locality,
        'alpha3': alpha3,
        'admin0': admin0,
        'category': category,
        '_id': _id,
        '_type': _type,
        '_score': _score
    };
}

module.exports = formatDataToElastic;
