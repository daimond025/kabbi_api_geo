'use strict';

var formatDataToElastic = require('./formatDataToElastic');

/**
 * google to elastic formatter
 * @param data
 * @param method
 * @returns {{docs: Array, meta: {scores: Array}}}
 */
function googleToElasticFormatter(data, method) {
    var results = {
        'docs': [],
        'meta': {
            "scores": []
        }
    };

    var rawResultArray = data.results;
    rawResultArray.forEach(function (item, i, raw) {
        var geoItem = item;
        var place_id = typeof geoItem.place_id != undefined ? geoItem.place_id : '';
        var lat = null;
        var lon = null;
        var number = '';
        var street = '';
        var name = '';
        var admin1 = '';
        var suburb = '';
        var locality = '';
        var alpha3 = '';
        var admin0 = '';
        var category = [];
         // var _id = 1;
        var _score = 1;
        var resultString = '';
        if (geoItem.geometry.location) {
            lat = geoItem.geometry.location.lat;
            lon = geoItem.geometry.location.lng;
        }
        if (geoItem.formatted_address) {
            resultString = geoItem.formatted_address;
        }

        var _type = 'osmnode';
        if(resultString === ''){
            name = geoItem.name;
        }else{
            if(typeof(geoItem.name) == "undefined"){
                name = resultString;
            }else{
                name = geoItem.name + ' | ' + resultString;
            }
        }


        var formattedItem = formatDataToElastic(lat, lon, number, street, name, suburb, admin1, locality, alpha3, admin0, category, place_id, _type, _score);
        results.docs.push(formattedItem);
        results.meta.scores.push(1);
    });
    return results;

}

module.exports = googleToElasticFormatter;
