'use strict';

var syslog = require('modern-syslog');
var config = require('./lib').getConfig();
var serviceVersion = require('./lib').getServiceVersion();
var prefix = '[main] ';
syslog.init(config.SYSLOG_MAIN_IDENT + '-' + serviceVersion, null, config.SYSLOG_MAIN_FACILITY);

module.exports = {
    debug: function (msg) {
        if (typeof msg === "object" && msg !== null) {
            msg = JSON.stringify(msg);
        }
        msg = prefix + msg;
        syslog.log(syslog.level.LOG_DEBUG, msg);
    },
    info: function (msg) {
        if (typeof msg === "object" && msg !== null) {
            msg = JSON.stringify(msg);
        }
        msg = prefix + msg;
        syslog.log(syslog.level.LOG_INFO, msg);
    },
    error: function (msg) {
        if (typeof msg === "object" && msg !== null) {
            msg = JSON.stringify(msg);
        }
        msg = prefix + msg;
        syslog.log(syslog.level.LOG_ERR, msg);
    },
    warning: function (msg) {
        if (typeof msg === "object" && msg !== null) {
            msg = JSON.stringify(msg);
        }
        msg = prefix + msg;
        syslog.log(syslog.level.LOG_WARNING, msg);
    }
};







