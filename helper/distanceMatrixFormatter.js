'use strict';
var logger = require('./logger');

/**
 *
 * @param raw
 * @returns {Array}
 */
function googleFormatter(raw,destination_addresses,destinations_geo ) {
    var result = [];
    raw.forEach(function (multiplicity, i, raw) {
        try {
            var elements = multiplicity.elements;
            if (elements.length) {
                result.push([]);
            }

            elements.forEach(function (point, j, elements) {
               var status = point.status;
               var duration =  '';
               var distance =   '';

               var destination_point = (typeof(destination_addresses[j]) !== 'undefined') ?  destination_addresses[j] : '';
               var destination_lat = (typeof(destinations_geo[j][0]) !== 'undefined') ?  destinations_geo[j][0] : '';
               var destination_lon = (typeof(destinations_geo[j][1]) !== 'undefined') ?  destinations_geo[j][1] : '';

               if((status.toString() === "ZERO_RESULTS") || (status.toString()=== "NOT_FOUND")){
                    duration = '';
                    distance =  '';

               }else{
                    duration =  (typeof(point.duration.value) !== 'undefined') ?  point.duration.value  : '';
                    distance =  (typeof(point.distance.value) !== 'undefined') ? point.distance.value  : '';
               }

               var itemFormatted = distanceMatrixItem(j, distance, duration ,destination_point, destination_lat, destination_lon);
               result[i].push(itemFormatted);
               
            });
        } catch (err) {
            logger.error('googleFormatter->Error: ' + err.message);
        }
    });

    return result;
}

/**
 * Osrm distance matrix formatter
 * @param {array} durations
 * raw example: [[0,48.4,48.4,0],[48.4,0,0,48.4]]
 * @returns {Array}
 */
function osrmFormatter(durations) {
    var result = [];
    durations.forEach(function (duration, i, durations) {
        result.push([]);
        try {
            //Delete the results between the source points
            var sliceCount = durations.length;
            var durationSliced = duration.slice(sliceCount);
            durationSliced.forEach(function (durationValue, j, durationSliced) {
                var itemFormatted = distanceMatrixItem(j + 1, null, durationValue);
                result[i].push(itemFormatted);
            });
        } catch (err) {
            logger.error('osrmFormatter->Error: ' + err.message);
        }
    });
    return result;
}


/**
 * Distance matrix item
 * @param id
 * @param distance
 * @param duration
 * @returns {{id: *, distance: *, duration: *}}
 */
function distanceMatrixItem(id, distance, duration, destination , latitude, longitude) {

    return {
        'id': id,
        'distance': distance,
        'duration': duration,
        'destination': destination,
        'latitude': latitude,
        'longitude':  longitude,
    };
}


module.exports.googleFormatter = googleFormatter;
module.exports.osrmFormatter = osrmFormatter;