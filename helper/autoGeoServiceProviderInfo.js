'use strict';

var service = {
    search: require('../service/search')};
var check = require('check-types');
var adapter = require('../database/AdapterDB');

function autoGeoServiceProviderInfo(tenantId, tenantDomain, cityId, typeApp, callback) {
    var geoServiceType = "auto_geo_service";
    adapter.getGeoKey(tenantId,cityId, geoServiceType ).then(function (geo_key) {
       var autoGeoServiceProviders = {
           1: "yandex",
           2: "herecom",
           3: "google",
           4: "2gis",
           5: "gootax",
           6: "yandex",
           7: "gomap.az",
           8: "geobase.az",
           9: "geobase.kgz"
       };

       var response = {
           "auto_geo_service_provider": {
               "provider": null,
               "key_1": null,
               "key_2": null
           }
       };

       let service_provider = parseInt( geo_key.service_provider_id);
       response.auto_geo_service_provider.provider = autoGeoServiceProviders[service_provider];
       response.auto_geo_service_provider.key_1 = geo_key.key_1;
       response.auto_geo_service_provider.key_2 = geo_key.key_1;
       return callback(null, response);
   });

   /* const MongoClient = require('mongodb').MongoClient;
    var url =  require('../helper/lib').getConfig().MONGODB_MAIN_DSN;
    var mongo_db =  require('../helper/lib').getConfig().MONGODB_DB;
    var mongo_db_collection =  require('../helper/lib').getConfig().MONGODB_COLLECTION;

    MongoClient.connect(url, { useNewUrlParser: true }, function(err, client) {
        const col = client.db(mongo_db).collection(mongo_db_collection);
        let key = tenantId + '_' + cityId + '_auto_geo_service';
        col.find({ row_id :key}).toArray(function(err, items) {

            console.log(items);

            client.close();
        });
    });*/



   /* var autoGeoServiceProviders = {
        1: "yandex",
        2: "herecom",
        3: "google",
        4: "2gis",
        5: "gootax",
        6: "yandex",
        7: "gomap.az",
        8: "geobase.az",
        9: "geobase.kgz"
    };

    var response = {
        "auto_geo_service_provider": {
            "provider": null,
            "key_1": null,
            "key_2": null
        }
    };

    // TODO DAIMONS
    response.auto_geo_service_provider.provider = autoGeoServiceProviders[3];
    response.auto_geo_service_provider.key_1 = 'AIzaSyBgVIObHG9w4w-ZsGeA2aHsiCheZRVA7m4';
    response.auto_geo_service_provider.key_2 = 'AIzaSyBgVIObHG9w4w-ZsGeA2aHsiCheZRVA7m4';
    console.log(101);
    return callback(null, response);*/

   /* if (check.undefined(cityId)) {
        return callback(null, response);
    }
    var query;

    if (typeApp === "driver") {
        if (check.undefined(tenantDomain)) {
            return callback(null, response);
        }
        query = {
            "query": {
                "bool": {
                    "must": [
                        {
                            "term": {
                                "tenant_domain": tenantDomain
                            }
                        }, {
                            "term": {
                                "city_id": cityId
                            }
                        }, {
                            "term": {
                                "service_type": geoServiceType
                            }
                        }

                    ]
                }
            },
            "size": 1
        };
    } else {
        if (check.undefined(tenantId)) {
            return callback(null, response);
        }
        query = {
            "query": {
                "bool": {
                    "must": [
                        {
                            "term": {
                                "tenant_id": tenantId
                            }
                        }, {
                            "term": {
                                "city_id": cityId
                            }
                        }, {
                            "term": {
                                "service_type": geoServiceType
                            }
                        }
                    ]
                }
            },
            "size": 1
        };
    }
    var cmd = {
        index: 'service_provider',
        body: query
    };
    // query backend
    service.search(backend, cmd, function (err, docs, meta) {
        if (err) {
            return callback(null, response);
        } else {
            if (check.array(docs)) {
                if (check.object(docs[0])) {
                    var providerData = docs[0];
                    var providerId = parseInt(providerData.service_provider_id, 10);
                    response.auto_geo_service_provider.provider = autoGeoServiceProviders[providerId];
                    response.auto_geo_service_provider.key_1 = providerData.key_1;
                    response.auto_geo_service_provider.key_2 = providerData.key_2;
                    return callback(null, response);
                }
            }
            return callback(null, response);

        }
    });*/

}

module.exports = autoGeoServiceProviderInfo;
