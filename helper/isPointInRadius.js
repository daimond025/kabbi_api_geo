'use strict';

var geolib = require('geolib');
var check = require('check-types');

/**
 * Check is point in circle with radius
 * @param {object} centerPoint
 * @param {number} centerPoint.latitude
 * @param {number} centerPoint.longitude
 * @param {object} addressPoint
 * @param {number} addressPoint.latitude
 * @param {number} addressPoint.longitude
 * @param {Number} radius - radius in metres
 */
function isPointInRadius(centerPoint, addressPoint, radius) {
    if (!(check.number(centerPoint.latitude) &&
        check.number(centerPoint.longitude)) ||
        !(check.number(addressPoint.latitude) &&
        check.number(addressPoint.longitude)) ||
        !(check.number(radius))) {
        return false
    }

    var distance = geolib.getDistance(
        centerPoint,
        addressPoint
    );
    return distance <= radius
}

module.exports = isPointInRadius;