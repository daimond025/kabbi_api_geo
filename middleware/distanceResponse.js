'use strict';

var standardDistancematrixResponse = require('../helper/standardDistancematrixResponse');
var logger = require('../helper/logger');

function routeResponse(req, res, next) {
    var version = '0.1';
    var timestamp = new Date().getTime();
    var query = req.clean;
    var errors = req.errors;
    var results;
    if (req.data) {
        results = req.data;
    }
    var response = standardDistancematrixResponse(version, query, errors, timestamp, results);
    if (errors && errors.length) {
        logger.error(errors);
        res.status(400).json(response);
    } else {
        res.json(response);
    }
}

module.exports = routeResponse;