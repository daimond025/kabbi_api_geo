'use strict';

var logger = require('../helper/logger');

function deteilsResponse(req, res, next) {
    var version = '0.1';
    var timestamp = new Date().getTime();
    var query = req.clean;
    var errors = req.errors;
    var results;
    if (req.data) {
        results = req.data;
    }
    var response = standartResponse(version, query, errors, timestamp, results);
    if (errors && errors.length) {
        logger.error(errors);
        res.status(400).json(response);
    } else {
        res.json(response);
    }
}

/**
 * standart route response
 * @param version
 * @param query
 * @param errors
 * @param timestamp
 * @param results
 * @returns {{geocoding: {version: (*|string), query: *, errors: (*|Array), timestamp: (*|string)}, results: (*|Array)}}
 */
function standartResponse(version, query, errors, timestamp, results) {
    return {
        place: {
            version: version || '',
            query: query || query,
            errors: errors || [],
            timestamp: timestamp || ''
        },
        results: results || []
    };
}

module.exports = deteilsResponse;