'use strict';

/**
 * Create a middleware that prints access logs via pelias-logger.
 */

'use strict';

var morgan = require('morgan');
var through = require('through2');
var logger = require('../helper/logger');

function createAccessLogger(logFormat) {
        return morgan(logFormat, {
            stream: through(function write(ln, _, next) {
                logger.debug(ln.toString().trim());
                next();
            })
        });
}

module.exports = createAccessLogger;
