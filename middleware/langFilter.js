'use strict';

var _ = require('lodash');

function setup() {
    return langFilter;
}

var filterMap = {
    "AZN": {
        "ru": "Азербайджан",
        "en": "Azerbaijan",
        "az": "Azərbaycan"
    },
    "AZ": {
        "ru": "Азербайджан",
        "en": "Azerbaijan",
        "az": "Azərbaycan"
    }
};

function langFilter(req, res, next) {
    if (_.isUndefined(req.clean) || _.isUndefined(res) || _.isUndefined(res.data) || !req.clean.lang) {
        return next();
    }
    var lang = req.clean.lang;
    var filteredByLangResults = [];
    _.some(res.data, function (hit) {
        if (hit.alpha3 && !_.isUndefined(filterMap[hit.alpha3]) && !_.isUndefined(filterMap[hit.alpha3][lang])) {
            if (hit.admin0 === filterMap[hit.alpha3][lang]) {
                filteredByLangResults.push(hit);
            }
        } else {
            filteredByLangResults.push(hit);
        }
    });
    res.data = filteredByLangResults;
    next();
}

module.exports = setup;
