'use strict';

// handle not found errors
function middleware(req, res) {
    res.header('Cache-Control', 'public');
    res.status(404).json();
}

module.exports = middleware;
