'use strict';

var Router = require('express').Router;
var async = require('async');

/** ----------------------- sanitisers ----------------------- **/
var sanitisers = {
    autocomplete: require('../sanitiser/autocomplete'),
    place: require('../sanitiser/place'),
    search: require('../sanitiser/search'),
    reverse: require('../sanitiser/reverse'),
    getRoute: require('../sanitiser/getRoute'),
    distancematrix: require('../sanitiser/distancematrix'),
    placeDeteils: require('../sanitiser/placeDetails')
};

/** ----------------------- middleware ------------------------ **/
var middleware = {
    types: require('../middleware/_types'),
    calcSize: require('../middleware/sizeCalculator')
};

/** ----------------------- controllers ----------------------- **/
var controllers = {
    mdToHTML: require('../controller/markdownToHtml'),
    search: require('../controller/searchBase'),
    status: require('../controller/status'),
    version: require('../controller/version'),
    clear: require('../controller/flush_cashe'),
    getRoute: require('../controller/getRoute'),
    distancematrix: require('../controller/distancematrix'),
    placeDeteils: require('../controller/placeDeteils')
};

/** ----------------------- controllers ----------------------- **/

var postProc = {
    distances: require('../middleware/distance'),
    confidenceScores: require('../middleware/confidenceScore'),
    confidenceScoresReverse: require('../middleware/confidenceScoreReverse'),
    dedupe: require('../middleware/dedupe'),
    localNamingConventions: require('../middleware/localNamingConventions'),
    renamePlacenames: require('../middleware/renamePlacenames'),
    geocodeJSON: require('../middleware/geocodeJSON'),
    sendJSON: require('../middleware/sendJSON'),
    routeResponse: require('../middleware/routeResponse'),
    langFilter: require('../middleware/langFilter'),
    distanceResponse: require('../middleware/distanceResponse'),
    placeDeteils: require('../middleware/deteilsResponse')
};

/**
 * Append routes to app
 *
 * @param {object} app
 * @param {object} peliasConfig
 */
function addRoutes(app) {

    var base = '/v1/';

    var peliasConfig = {};
    /** ------------------------- routers ------------------------- **/
    async.parallel(
        [
            function index(callback) {
                callback(null, [controllers.mdToHTML(peliasConfig, './public/apiDoc.md')]);
            },
            function search(callback) {
                controllers.search('search', function (err, controller) {
                    if (err) {
                        console.log(err);
                    } else {
                        callback(null, [
                            sanitisers.search.middleware,
                            middleware.types,
                            middleware.calcSize(),
                            controller,
                            postProc.distances('focus.point.'),
                            postProc.confidenceScores(peliasConfig),
                            postProc.dedupe(),
                            postProc.localNamingConventions(),
                            postProc.renamePlacenames(),
                            postProc.geocodeJSON(peliasConfig, base),
                            postProc.sendJSON
                        ]);
                    }
                });

            },
            function autocomplete(callback) {
                controllers.search('autocomplete', function (err, controller) {
                    if (err) {
                        console.log(err);
                    } else {
                        peliasConfig = {};
                        callback(null, [
                            sanitisers.autocomplete.middleware,
                            middleware.types,
                            controller,
                            postProc.distances('focus.point.'),
                            postProc.confidenceScores(peliasConfig),
                            postProc.dedupe(),
                            postProc.localNamingConventions(),
                            postProc.renamePlacenames(),
                            postProc.geocodeJSON(peliasConfig, base),
                            postProc.sendJSON
                        ]);
                    }
                });

            },
            function reverse(callback) {
                controllers.search('reverse', function (err, controller) {
                    if (err) {
                        console.log(err);
                    } else {
                        peliasConfig = {};
                        callback(null, [
                            sanitisers.reverse.middleware,
                            middleware.types,
                            middleware.calcSize(),
                            controller,
                            postProc.langFilter(),
                            postProc.distances('point.'),
                            postProc.confidenceScoresReverse(),
                            postProc.dedupe(),
                            postProc.localNamingConventions(),
                            postProc.renamePlacenames(),
                            postProc.geocodeJSON(peliasConfig, base),
                            postProc.sendJSON
                        ]);
                    }
                });
            },
            function status(callback) {
                callback(null, [controllers.status]);
            },
            function getRoute(callback) {
                callback(null, [
                    sanitisers.getRoute.middleware,
                    controllers.getRoute,
                    postProc.routeResponse
                ]);
            },
            function version(callback) {
                callback(null, [controllers.version]);
            },
            function distancematrix(callback) {
                callback(null, [
                    sanitisers.distancematrix.middleware,
                    controllers.distancematrix,
                    postProc.distanceResponse
                ]);
            },
            function clearCache(callback) {
                callback(null, [controllers.clear]);
            },
            function details(callback) {
                callback(null, [
                    sanitisers.placeDeteils.middleware,
                    controllers.placeDeteils,
                    postProc.placeDeteils
                ]);
            },
        ], function (err, results) {
            if (err) {
            } else {
                var routers = {
                    index: createRouter(results[0]),
                    search: createRouter(results[1]),
                    autocomplete: createRouter(results[2]),
                    reverse: createRouter(results[3]),
                    status: createRouter(results[4]),
                    getRoute: createRouter(results[5]),
                    version: createRouter(results[6]),
                    distancematrix: createRouter(results[7]),
                    clearCache: createRouter(results[8]),
                    details: createRouter(results[9])
                };
                // static data endpoints
                app.get(base, routers.index);
                app.get('/status', routers.status);
                app.get('/version', routers.version);
                app.get('/clear', routers.clearCache);
                app.get(base + 'autocomplete', routers.autocomplete);
                app.get(base + 'search', routers.search);
                app.get(base + 'reverse', routers.reverse);
                app.get(base + 'getRoute', routers.getRoute);
                app.get(base + 'distancematrix', routers.distancematrix);
                app.get(base + 'details', routers.details);
            }
        });

}

/**
 * Helper function for creating routers
 *
 * @param {[{function}]} functions
 * @returns {express.Router}
 */
function createRouter(functions) {
    var router = Router(); // jshint ignore:line
    functions.forEach(function (f) {
        router.use(f);
    });
    return router;
}


module.exports.addRoutes = addRoutes;
