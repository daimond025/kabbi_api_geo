// new version
var port = (process.env.PORT || 3100);
var app = require('./app');

var cluster = require('cluster');

// Code to run if we're in the master process
if (cluster.isMaster) {
    var cpuCount = require('os').cpus().length;

    // Create a worker for each CPU
    for (var i = 0; i < cpuCount; i += 1) {
        cluster.fork();
    }
} else {
    app.listen(port);
}


 // OLD VERSDION
/*var Cluster = require('cluster2'),
    app = require('./app'),
    port = (process.env.PORT || 3100),
    multicore = parseInt(require('./helper/lib').getConfig().MY_MULTICORE),
    logger = require('./helper/logger');


if (multicore ) {
    var c = new Cluster({port: port});
    c.listen(function (cb) {
        logger.info('Listenint on port: ' + port);
        cb(app);
    });

}
else {
    app.listen(port);
    logger.info('Listenint on port: ' + port);
}*/
