"use strict";
var mysql      = require('mysql');

var config_list = require('../helper/lib').getConfig();
var url =  config_list.DB_MAIN_LOCAL;
var database =  config_list.DB_MAIN_DATABASE;
var user =  config_list.DB_MAIN_USERNAME;
var password =  config_list.DB_MAIN_PASSWORD;



class Mysql_geo {
    constructor(url, options) {

        return new Promise((resolve, reject) => {

            let self = this;
            self.connection = null;
            return resolve(self);
        });

    }

    connect() {
        return new Promise((resolve, reject) => {
            try {
                let self = this;
                self.connection = mysql.createConnection({
                    localAddress: url,
                    host: url,
                    database: database,
                    user: user,
                    password: password
                });

                console.log("Подключение к базе");
                return resolve(self);
            }
            catch (e) {
                reject(e);
            }

        });
    }

    get(geoServiceType) {
        return new Promise((resolve, reject) => {
            let self = this;
            if (typeof(self.connection) === undefined || self.connection === null) {
                self.connect().then(function (self) {
                    self.connection.query("SELECT * FROM `tbl_auto_geo_key_default` WHERE `service_type` = ?",[geoServiceType], function (error, results) {
                        if (error) {
                            return reject(error);
                        }
                        if(results.length >= 1){
                            return resolve(results[0]);
                        }

                        return resolve(null);

                    });
                }).catch(function (error) {
                    return reject(error);
                });
            }else{
                self.connection.query("SELECT * FROM `tbl_auto_geo_key_default` WHERE `service_type` = ?",[geoServiceType], function (error, results) {
                    if (error) {
                        return reject(error);
                    }

                    if(results.length >= 1){
                        return resolve(results[0]);
                    }
                    return resolve(null);
                });
            }

        });
    }

    close() {
        return new Promise((resolve, reject) => {
            try {
                let self = this;
                if (typeof (self.connection) === undefined  ||  self.connection === null) {
                    return resolve(true);
                }
                self.connection.end();
                self.connection = null;

                console.log("закрыто к базе");
                return resolve(true);
            }
            catch (e) {
                reject(e);
            }

        });
    }
}

module.exports = new  Mysql_geo();
