"use strict";

const logger = require("../helper/logger");  //logger.error('osrmRoute->Error: ' + geoData.code);

var cache_promise =  require('../database/Cache');
var mongo_promise =  require('../database/mongo');
var mysql_promise =  require('../database/mysql');

class AdapterDB{

     // подключение к БД осуществлять только при запрсое -  ппосле полусения данных - сразу  сохранение в кеш т отключение от БД
    constructor(){
        let self = this;
        cache_promise.then(function(cache_connetion) {
            self.cache = cache_connetion;
            return mongo_promise;
           }, function(err) {
        }).then(function (mongo) {
            self.mongo = mongo;
            return mysql_promise;
        }).then(function (mysql) {
            self.mysql = mysql;
            return self;
        }).catch(function (error) {
            logger.error("DATABASE PROVIDER->Error: " + error );
        });

    }


    getGeoKEyMysql(key_index , geoServiceType){
        let self = this;
        return new Promise((resolve, reject) => {
            try{
                self.mysql.get(geoServiceType).then(function (rezult_mysql) {
                    var geo_rezult = rezult_mysql;
                    self.mysql.close().then(function () {
                        resolve(geo_rezult);
                    });
                });
            }catch (e) {
                reject(e);
            }


        });
    }



    // сначала берем данные из
    getGeoKey(tenantId, cityId, geoServiceType){

        // ОБРАЧИВАЕМ ВСЕ В PROMISE
        return new Promise((resolve, reject) => {
            let self = this;
            let key_index = this.getKet(tenantId, cityId, geoServiceType);

            let geo_rezult = self.cache.get(key_index);
            console.log("Данные из кеша " + geo_rezult + "\n");

            // берем данные из монго
            if(geo_rezult === null  || typeof (geo_rezult) === undefined ){

                self.mongo.get(key_index).then(function(rezult_mongo) {
                    if((rezult_mongo === null)|| (rezult_mongo  === undefined) || ( typeof(rezult_mongo) === undefined )){
                        self.mongo.close();

                        self.getGeoKEyMysql(key_index, geoServiceType).then(function (geo_rezult) {
                            self.cache.set(key_index, geo_rezult);
                            resolve(geo_rezult);
                        }).catch(function (error) {
                            logger.error("DATABASE PROVIDER MYSQL->Error: " + error );
                        });

                    }else{
                        geo_rezult = rezult_mongo;
                        self.cache.set(key_index, geo_rezult);
                        self.mongo.close();
                        resolve(geo_rezult);
                    }
                }).catch(function (error) {
                    logger.error("DATABASE PROVIDER MONGO->Error: " + error );

                    self.getGeoKEyMysql(key_index, geoServiceType).then(function (geo_rezult) {
                        self.cache.set(key_index, geo_rezult);
                        resolve(geo_rezult);
                    }).catch(function (error) {
                        logger.error("DATABASE PROVIDER MYSQL->Error: " + error );

                        reject(error);
                    });
                });

            }else {
                resolve(geo_rezult);
            }
        });


    }

    getKet(tenantId, cityId, geoServiceType){
        var key = tenantId + "_" + cityId  + "_" + geoServiceType;
        return key;
    }
}

module.exports = new AdapterDB();