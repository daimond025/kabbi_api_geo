"use strict";

const lib = require("../helper/lib");
const config = lib.getConfig();
const logger = require("../helper/logger");

//const PHPUnserialize = require('php-unserialize');

// класс для работы к кешем
const NodeCache = require( "node-cache" );

const tll_val = 86400; //  хранить ключ сутки

class Cache {

    constructor() {
        return new Promise((resolve, reject) => {
            this.cache = null;

            let self = this;
            if (self.cache) {
                return resolve(self);
            }
            try {
                self.cache = new NodeCache({ stdTTL: tll_val });
                return resolve(self);
            }
            catch (error) {
                reject(error);
            }

        });
    }

    connect() {
        const self = this;
        return new Promise((resolve, reject) => {
            if (self.cache) {
                return resolve(true);
            }
            try {
                self.cache = new NodeCache({ stdTTL: tll_val});
                return resolve(self);
            }
            catch (error) {
                reject(error);
            }
        });
    }


    get(key){
        const self = this;
        if(!self.cache){
            self.connect();
        }
        var geo_date =  self.cache.get( key);
        if(geo_date === undefined || typeof (geo_date) === undefined  || geo_date === null ){
            return null;
        }

        return JSON.parse(geo_date);
    }

    set(key, geo_date){

        const self = this;

        if(!self.cache){
            self.connect();
        }

        var save_value = JSON.stringify(geo_date);

        return self.cache.set( key, save_value);
    }
}



module.exports  = new Cache();