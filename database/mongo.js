"use strict";

var MongoClient = require('mongodb').MongoClient;

var config_list = require('../helper/lib').getConfig();
var url =  config_list.MONGODB_MAIN_DSN;
var mongo_db =  config_list.MONGODB_DB;
var mongo_db_collection =  config_list.MONGODB_COLLECTION;


const options = {
    useNewUrlParser: true,
    reconnectTries:  config_list.MONGODB_MAIN_RECONNECT_TRIES,
    reconnectInterval: config_list.MONGODB_MAIN_RECONNECT_INTERVAL_MS,
    poolSize: config_list.MONGODB_MAIN_POOL_SIZE,
    bufferMaxEntries: 0,
};


class Mongo {
    constructor(url, options) {

        return new Promise((resolve, reject) => {

            let self = this;
            self.connection = null;
            return resolve(self);
        });

    }

    connect(){
        return new Promise((resolve, reject) => {
            let self = this;
            try {
                MongoClient.connect(url,  options, function(err, client) {
                    self.connection = client;
                    return resolve(self);
                });
            }
            catch (e) {
                reject(e);
            }

        });
    }

    reConected(){
        return new Promise((resolve, reject) => {
            let self = this;
            self.connect().then(function (mongo_connection) {
                console.log("Соединение  с монго");
                resolve(mongo_connection);
            }).catch(function (error) {
                reject(error);
            });
        });


    }

    get(key){
        return new Promise((resolve, reject) => {
            let self = this;
            if( typeof(this.connection) === undefined  ||  this.connection === null){
                this.reConected().then(function (mongo_connection) {
                    self = mongo_connection;

                    let col = self.connection.db(mongo_db).collection(mongo_db_collection);
                    col.find({ row_id :key}).toArray(function(err, items) {
                        if(err){
                            reject(err);
                        }

                        if(items.length >= 1){
                             resolve(items[0]);
                        }
                        resolve(null);

                    });
                }).catch(function (error) {
                    reject(error);
                });

            }else{
                try {
                    self = this;
                    let col = self.connection.db(mongo_db).collection(mongo_db_collection);
                    col.find({ row_id :key}).toArray(function(err, items) {
                        if(err){
                            return reject(err);
                        }
                        if(items.length >= 1){
                            resolve(items[0]);
                        }
                        resolve(null);

                    });
                }
                catch (error) {
                    reject(error);
                }

            }


        });
    }

    get db() {
        return this.connection;
    }

    close() {
        return new Promise((resolve, reject) => {
            try{
                let self = this;
                if (typeof (self.connection) === undefined  ||  self.connection === null) {
                    return resolve(true);
                }
                self.connection.close();
                self.connection = null;
                console.log("Закрыто с монго");
                return resolve(self);
            }catch (e) {
                reject(e);
            }

        });
    }

}




module.exports = new  Mongo(url, options);