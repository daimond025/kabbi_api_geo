'use strict';

var routeGeoServiceProviderInfo = require('../helper/routeGeoServiceProviderInfo');
var placeDeteils_ = require('./google/placeDeteils');


/**
 * get route controller
 * @param req
 * @param res
 * @param next
 * @returns {*}
 */
function placeDeteils(req, res, next) {
    if (req.errors && req.errors.length) {
        return next();
    }
    var tenant_id = req.clean.tenant_id || '';
    var tenant_domain = req.clean.tenant_domain || '';
    var city_id = req.clean.city_id || '';
    var type_app = req.clean.type_app || '';
    routeGeoServiceProviderInfo(tenant_id, tenant_domain, city_id, type_app, function (err, dataProviderInfo) {
        if (err) {
            req.errors.push(err);
            return next();
        } else {
            var providerLabel = dataProviderInfo.route_geo_service_provider.provider;
            var apiKey1 = dataProviderInfo.route_geo_service_provider.key_1;
            switch (providerLabel) {
                case 'google':
                    placeDeteils_(apiKey1, req, res, next);
                    break;
                default:
                    req.errors.push(new Error('unsupported method for geo-provider:' + providerLabel));
                    return next();
            }
        }
    });

}

module.exports = placeDeteils;