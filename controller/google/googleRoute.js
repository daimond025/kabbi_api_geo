"use strict";

var logger = require('../../helper/logger');
var request = require('request');
var polyline = require('polyline');
var config = require("../../helper/lib").getConfig();
var apiUrl = config.GOOGLE_ROUTE_URL;


/**
 * google route
 * @param key
 * @param req
 * @param res
 * @param next
 * @returns {*}
 */
function googleRoute(key, req, res, next) {
    if (req.errors && req.errors.length) {
        return next();
    }
    var clean = req.clean;
    var coords = clean.coords || [];
    var format = clean.format; // by default 'array' by sanitiser
    var splitted_route = clean.splitted_route;
    if (coords.length <= 2) {
        splitted_route = 0;
    }
    var apiKey = key ? 'key=' + key : '';
    var reqUrl;
    // from
    var origin = coords.shift().join();
    // to
    var destination = coords.pop().join();
    var waypoints = '';
    if (coords.length) {
        if (coords.length === 1) {
            waypoints = '&waypoints=' + coords[0].join();
        } else {
            var waypointsTmpArr = coords.map(function (item) {
                return item.join();
            });
            waypoints = '&waypoints=' + waypointsTmpArr.join('|');
        }
    }
    var lang = clean.language ? '&language=' + clean.language : '';
    var units = 'metric';
    // return coords to query req.clean object for proper(all request points) answer
    coords.unshift(origin);
    coords.push(destination);
    reqUrl = apiUrl + '?' + apiKey + '&origin=' + origin + '&destination=' + destination + waypoints + '&units=' + units + lang;
    request.get({url: reqUrl}, function (error, response, body) {
        if (error) {
            req.errors.push(error);
            logger.error('googleRoute->Error: ' + error.message);
            return next();
        }
        if (response.statusCode !== 200) {
            req.errors.push('response status is not 200');
            return next();
        }
        var geoData;
        try {
            geoData = JSON.parse(body);
        } catch (e) {
            logger.error('googleRoute->Error: ' + e.message);
            req.errors.push(e);
            return next();
        }
        if (geoData.status !== 'OK') {
            logger.error('googleRoute->Error: ' + geoData.error_message);
            req.errors.push(geoData.error_message);
            return next();
        }
        var route;
        if (splitted_route && coords.length > 2) {
            route = get_splitted_route(geoData, format);
        } else {
            route = get_united_route(geoData, format);
        }
        req.data = route;
        return next();
    });
}

/**
 * get splitted route
 * @param data
 * @param format
 * @returns {Array}
 */
function get_splitted_route(data, format) {
    var routes = [];
    var legs;
    try {
        legs = data.routes[0].legs;
        // For each leg, where leg = n - 1 (n= amount of points)
        legs.forEach(function (leg) {
            var distance = leg.distance.value;
            var duration = leg.duration.value;
            var coords = [];
            // every leg has its own arr of steps
            // every step has its own polyline
            leg.steps.forEach(function (step) {
                var latLonArr = polyline.decode(step.polyline.points);
                latLonArr.forEach(function (latLon) {
                    coords.push(latLon);
                });
            });
            // encode array of lat, lon to polyline, if necessary
            if (format === 'polyline') {
                coords = polyline.encode(coords);
            }
            routes.push({
                distance: distance.toString(),
                time: (duration / 60).toFixed(),
                route: coords
            });
        });
    } catch (e) {
        logger.error('googleRoute->get_splitted_route->Error: ' + e.message);
    }
    return routes;
}

/**
 * get united route
 * @param data
 * @param format
 * @returns {[null]}
 */
function get_united_route(data, format) {
    var distance = 0;
    var time = 0;
    var coords = '';

    var route;
    try {
        route = data.routes[0].legs;
        if (data.routes[0].legs.length) {
            // time in seconds, distance in meters
            route.forEach(function (item) {
                time += item.duration.value;
                distance += item.distance.value;
            });
            if (data.routes[0] && data.routes[0].overview_polyline) {
                // polyline
                coords = data.routes[0].overview_polyline.points;
            }
            // to valid response types
            time = (time / 60).toFixed();
            distance = distance.toString();
            if (format === 'array') {
                coords = polyline.decode(coords);
            }
        }
    } catch (e) {
        logger.error('googleRoute->get_united_route->Error: ' + e.message);
    }

    return [{
        route: coords,
        time: time,
        distance: distance
    }];

}

module.exports = googleRoute;
