'use strict';

var logger = require('../../helper/logger');
var request = require('request');
var googleToElasticFormatter = require('../../helper/formatter/googleToElasticFormatter');
var config = require("../../helper/lib").getConfig();
var apiUrl = config.GOOGLE_SEARCH_URL;
var outputFormat = 'json';

/**
 * Google search controller
 * @param method
 * @param placesApiKey
 * @param geocodingApiKey
 * @param req
 * @param res
 * @param next
 * @returns {*}
 */
function controller(method, placesApiKey, geocodingApiKey, req, res, next) {
    if (req.errors && req.errors.length) {
        return next();
    }
    var clean = req.clean;
    var text = (typeof clean.text === "undefined") ? '' : encodeURIComponent(clean.text);
    var radius = (typeof clean.radius === "undefined") ? '' : parseInt(clean.radius);
    var lat = (typeof clean['focus.point.lat'] === "undefined") ? '' : clean['focus.point.lat'];
    var lon = (typeof clean['focus.point.lon'] === "undefined") ? '' : clean['focus.point.lon'];

    var lang = clean.lang || '';
    var language = '';
    if (lang) {
        language = '&language=' + lang;
    }
    var rgn = clean.region || '';
    var region = '';
    if(rgn){
        region = '&region=' + rgn;
    }

    // fit radius to google api (km->m)
    radius = radius * 1000;
    if (radius > 50000) {
        radius = 50000;
    }
    var apiService, dataUrl;
    if (method === 'autocomplete') {
        // example string https://maps.googleapis.com/maps/api/place/textsearch/output?parameters
        apiService = '/place/textsearch/';
        dataUrl = apiUrl + apiService + outputFormat + '?&' + 'location=' + lat + ',' + lon + '&radius=' + radius + '&query=' + text + '&key=' + placesApiKey + language + region;
        console.log(dataUrl);
    }
    if (method === 'search') {
        // when global request, radius has to be 0
        radius = 0;
        apiService = '/place/textsearch/';
        dataUrl = apiUrl + apiService + outputFormat + '?' + 'query=' + text + '&radius=' + radius + '&key=' + placesApiKey + language + '&radius=' + '0';
    }
    if (method === 'reverse') {
        apiService = '/geocode/';
        var resultType = '&result_type=street_address|route|intersection|colloquial_area|premise|subpremise|natural_feature|airport|park|street_number';
        var latP = (typeof clean['point.lat'] === "undefined") ? '' : clean['point.lat'];
        var lonP = (typeof clean['point.lon'] === "undefined") ? '' : clean['point.lon'];
        dataUrl = apiUrl + apiService + outputFormat + '?' + 'key=' + geocodingApiKey + resultType + '&latlng=' + latP + ',' + lonP + language;
    }

    request({url: dataUrl}, function (err, response, body) {
        if (err) {
            logger.error('googleSearch->Request Error: ' + err.message + ' . Request URL: ' + dataUrl);
            return next();
        }
        if (response.statusCode !== 200) {
            logger.error('Error: Bad status code: ' + response.statusCode + '. Function: googleSearch. Request URL: ' + req.url);
            return next();
        }
        try {
            var geoData = JSON.parse(body);
        } catch (err) {
            logger.error('googleSearch->JSN parsing Error: ' + err.message + ' . Request URL: ' + dataUrl);
            return next();
        }
        if (geoData.status !== 'OK') {
            logger.error('googleSearch->No OK response:' + geoData.status + ' . Request URL: ' + dataUrl);
            return next();
        }
        var formattedData = googleToElasticFormatter(geoData, method);
        var googleData = {
            'docs': formattedData.docs,
            'meta': formattedData.meta
        };
        res.data = googleData.docs;
        res.meta = googleData.meta;
        return next();
    });
}

module.exports = controller;
