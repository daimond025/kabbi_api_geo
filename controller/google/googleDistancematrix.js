'use strict';

var logger = require('../../helper/logger');
var request = require('request');
var distanceMatrixFormatter = require('../../helper/distanceMatrixFormatter');
var config = require("../../helper/lib").getConfig();
var apiUrl = config.GOOGLE_DISTANCEMATRIX_URL;


/**
 * Distance matrix
 * @param key
 * @param req
 * @param res
 * @param next
 * @returns {*}
 */
function distanceMatrix(key, req, res, next) {
    if (req.errors && req.errors.length) {
        return next();
    }
    var clean = req.clean;
    var origins = clean.origins || [];
    var destinations = clean.destinations || [];
    origins = origins.join('|');

    destinations = destinations.join('|');
    var lang = clean.language ?  clean.language : 'de';

    var reqUrl = apiUrl + '?origins=' + origins + '&destinations=' + destinations + '&departure_time=now&units=metric&key=' + key + '&language=' +lang ;
    request.get({url: reqUrl}, function (error, response, body) {
        if (error) {
            req.errors.push(error);
            logger.error('googleDistanceMatrix->Error: ' + error.message);
            return next();
        }
        if (response.statusCode !== 200) {
            req.errors.push('googleDistanceMatrix->response status is not 200');
            return next();
        }
        var resultData;
        try {
            resultData = JSON.parse(body);
        } catch (e) {
            logger.error('googleDistanceMatrix->->Error: ' + e.message);
            req.errors.push(e);
            return next();
        }
        if (resultData.status !== 'OK') {
            logger.error('googleDistanceMatrix->->Error: ' + resultData.error_message);
            req.errors.push(resultData.error_message);
            return next();
        }
        var destination_addresses = resultData.destination_addresses ? resultData.destination_addresses : [];
        var destinations_geo = clean.destinations || [];
        req.data = distanceMatrixFormatter.googleFormatter(resultData.rows, destination_addresses,destinations_geo );
        return next();
    });
}


module.exports = distanceMatrix;