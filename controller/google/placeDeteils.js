'use strict';

var logger = require('../../helper/logger');
var request = require('request');
var placeDeteilsFormatter = require('../../helper/placeDeteilsFormatter');
var config = require("../../helper/lib").getConfig();
var apiUrl = config.GOOGLE_DETEILSPLACES_URL;


/**
 * Distance matrix
 * @param key
 * @param req
 * @param res
 * @param next
 * @returns {*}
 */
function placeDeteils(key, req, res, next){
    if(req.errors && req.errors.length){
        return next();
    }
    var clean = req.clean;

    var placeId = (typeof clean.placeId === "undefined") ? '' : clean.placeId.trim();
    var lang = clean.lang ? clean.lang : 'de';

    var region = '';
    if(clean.region !== null){
        region = '&region=' + clean.region;
    }
    var reqUrl = apiUrl + '?place_id=' + placeId + '&key=' + key + '&language=' + lang + region;
    console.log(reqUrl);
    request.get({url: reqUrl}, function(error, response, body){
        if(error){
            req.errors.push(error);
            logger.error('googleDeteilPlace->Error: ' + error.message);
            return next();
        }
        if(response.statusCode !== 200){
            req.errors.push('googleDeteilPlace->response status is not 200');
            return next();
        }
        var resultData;
        try{
            resultData = JSON.parse(body);
        }catch(e){
            logger.error('googleDeteilPlace->->Error: ' + e.message);
            req.errors.push(e);
            return next();
        }
        if(resultData.status !== 'OK'){
            logger.error('googleDeteilPlace->->Error: ' + resultData.status);
            req.errors.push(resultData.status);
            return next();
        }

        var address_struct = resultData.result.address_components ? resultData.result.address_components : [];
        var name = resultData.result.name ? resultData.result.name : '';

        var lat = (typeof resultData.result.geometry.location.lat !== undefined) ? resultData.result.geometry.location.lat : 0.0;
        var lon = (typeof resultData.result.geometry.location.lng !== undefined) ? resultData.result.geometry.location.lng : 0.0;
        req.data = placeDeteilsFormatter.googleFormatter(address_struct, name,  lat, lon);
        return next();
    });
}


module.exports = placeDeteils;