'use strict';

var routeGeoServiceProviderInfo = require('../helper/routeGeoServiceProviderInfo');
var osrmRoute = require('./osrm/osrmRoute');
var googleRoute = require('./google/googleRoute');
var twoGisRoute = require('./twogis/twoGisRoute');
var herecomRoute = require('./herecom/herecomRoute');
var digimapRoute = require('./digimap/digimapRoute');

/**
 * get route controller
 * @param req
 * @param res
 * @param next
 * @returns {*}
 */
function getRoute(req, res, next) {
    if (req.errors && req.errors.length) {
        return next();
    }
    var tenant_id = req.clean.tenant_id || '';
    var tenant_domain = req.clean.tenant_domain || '';
    var city_id = req.clean.city_id || '';
    var type_app = req.clean.type_app || '';
    routeGeoServiceProviderInfo(tenant_id, tenant_domain, city_id, type_app, function (err, dataProviderInfo) {
        if (err) {
            req.errors.push(err);
            return next();
        } else {
            var providerLabel = dataProviderInfo.route_geo_service_provider.provider;
            var apiKey1 = dataProviderInfo.route_geo_service_provider.key_1;
            var apiKey2 = dataProviderInfo.route_geo_service_provider.key_2;
            switch (providerLabel) {
                case 'osrm':
                    osrmRoute(req, res, next);
                    break;
                case 'google':
                    googleRoute(apiKey1, req, res, next);
                    break;
                case '2gis':
                    twoGisRoute(apiKey1, req, res, next);
                    break;
                case 'herecom':
                    herecomRoute(apiKey1, apiKey2, req, res, next);
                    break;
                case 'digimap':
                    digimapRoute(apiKey1, req, res, next);
                    break;
                default:
                    osrmRoute(req, res, next);
            }
        }
    });

}
module.exports = getRoute;
