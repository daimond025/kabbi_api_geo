'use strict';

var packageInfo = require('../package.json');
/**
 * Service version controller
 * @param req
 * @param res
 * @param next
 */
module.exports = function controller(req, res, next) {
    var name = packageInfo.name;
    var version = packageInfo.version;
    res.setHeader('Content-Type', 'text/plain');
    var info = name + " v" + version + "\n" + "work on nodejs " + process.version;
    res.send(info);
};