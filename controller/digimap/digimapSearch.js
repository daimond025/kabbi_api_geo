'use strict';

var request = require('request');
var logger = require('../../helper/logger');
var async = require('async');
var service = {search: require('../../service/search')};
var backend = require('../../src/backend');
var digimapToElasticFormatter = require('../../helper/formatter/digimapToElasticFormatter');
var config = require("../../helper/lib").getConfig();
var apiUrl = config.DIGIMAP_SEARCH_URL;

/**
 * digimap search controller
 * @param method
 * @param key
 * @param req
 * @param res
 * @param next
 * @returns {*}
 */
function controller(method, key, req, res, next) {
    if (req.errors && req.errors.length) {
        return next();
    }

    var clean = req.clean;
    var text = clean.text && encodeURIComponent(clean.text) || '';
    var size = clean.size && parseInt(clean.size) || '';
    var lat = clean['focus.point.lat'] || '';
    var lon = clean['focus.point.lon'] || '';
    var lang = clean.lang || '&lang=ru';

    // note! these retards use x = lon , y = lat
    var apiKey = key || '';
    // spatial - "пространственный"
    var spatialFormatOutput = 'EPSG:4326';
    var spatialFormatInput = 'EPSG:4326';
    var outputFormat = 'ea2';
    size = size <= 10 ? size : 10;
    var reqUrl;

    // Form request to digimap
    if (method === 'autocomplete') {
        reqUrl = apiUrl + ':4682/suggest' +
            '?' +
            'text=' + text +
            '&count=' + size +
            '&format=' + outputFormat +
            '&spatialOut=' + spatialFormatOutput +
            '&spatialIn=' + spatialFormatInput +
            '&x=' + lon +
            '&y=' + lat +
            '&data=address' +
            '&guid=' + apiKey +
            lang;
    } else if (method === 'search') {
        reqUrl = apiUrl + ':4682/search' +
            '?' +
            'text=' + text +
            '&count=' + size +
            '&format=' + outputFormat +
            '&spatialOut=' + spatialFormatOutput +
            '&spatialIn=' + spatialFormatInput +
            '&x=' + lon +
            '&y=' + lat +
            '&data=address' +
            '&mode=search' +
            lang +
            '&guid=' + apiKey;
    } else {
        lat = clean['point.lat'] || '';
        lon = clean['point.lon'] || '';
        reqUrl = apiUrl + ':4850/getAddress' +
            '?' +
            'x=' + lon +
            '&y=' + lat +
            '&format=' + outputFormat +
            '&spatialOut=' + spatialFormatOutput +
            '&spatialIn=' + spatialFormatInput +
            '&pattern=intersect' +
            lang +
            '&guid=' + apiKey;
    }

    async.parallel(
        [
            function getDigitmapData(callback) {
                request({url: reqUrl}, function (error, response, body) {
                    if (error || response.statusCode !== 200) {
                        req.errors.push(error);
                        logger.error('getDigitmapData->Error: ' + error.message);
                        return next();
                    }

                    var geoData;
                    try {
                        geoData = JSON.parse(body);
                    } catch (e) {
                        req.errors.push(e);
                        logger.error('getDigitmapData->Error: ' + e.message);
                        return next();
                    }

                    var formattedData = digimapToElasticFormatter(geoData);

                    var digimapData = {
                        docs: formattedData.docs,
                        meta: formattedData.meta
                    };

                    callback(null, digimapData);
                });
            },
            function getOsmData(callback) {
                var query;
                switch (method) {
                    case 'search':
                        query = require('../../query/searchPublic');
                        break;
                    case 'autocomplete':
                        query = require('../../query/autocompletePublic');
                        break;
                    case 'reverse':
                        query = require('../../query/reverse');
                        break;
                    default:
                        query = require('../../query/searchPublic');
                        break;
                }

                // backend command
                var cmd = {
                    index: config.ELASTICSEARCH_GEO_INDEX,
                    searchType: 'dfs_query_then_fetch',
                    body: query(req.clean)
                };

                // ?
                if (req.clean.hasOwnProperty('type')) {
                    cmd.type = req.clean.type;
                }

                // query backend
                service.search(backend, cmd, function (err, docs, meta) {
                    if (err) {
                        req.errors.push(err);
                        logger.error('getDigitmapData->service.search-> Error: ' + err.message);
                    } else {
                        var osmData = {
                            'docs': docs,
                            'meta': meta
                        };
                        callback(null, osmData);
                    }
                });
            }
        ],
        function (err, results) {
            if (err) {
                req.errors.push(err);
                logger.error('getDigitmapData->Error: ' + err.message);
                return next();
            }

            var digimapDocs = [];
            var digimapScores = [];
            var osmDocs = [];
            var osmScores = [];
            var resultData;

            if (results[0]) {
                digimapDocs = results[0].docs;
                digimapScores = results[0].meta.scores;
            }
            if (results[1]) {
                osmDocs = results[1].docs;
                osmScores = results[1].meta.scores;
            }

            resultData = {
                'docs': digimapDocs.concat(osmDocs),
                'meta': digimapScores.concat(osmScores)
            };

            res.data = resultData.docs;
            res.meta = resultData.meta;
            next();
        }
    );
}

module.exports = controller;
