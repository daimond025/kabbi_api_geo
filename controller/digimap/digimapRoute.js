'use strict';

var config = require("../../helper/lib").getConfig();
var apiUrl = config.DIGIMAP_ROUTE_URL;

/**
 * Get route controller
 * @param key
 * @param req
 * @param res
 * @param next
 * @returns {*}
 */
function digimapRoute(key, req, res, next) {
    return next();
}

module.exports = digimapRoute;
