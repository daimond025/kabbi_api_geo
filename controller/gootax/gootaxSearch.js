"use strict";

var service = {search: require('../../service/search')};
var logger = require('../../helper/logger');
var backend = require('../../src/backend');

function controller(method, searchIndex, req, res, next) {
    var query;
    switch (method) {
        case 'search':
            query = require('../../query/search');
            break;
        case 'autocomplete':
            query = require('../../query/autocomplete');
            break;
        case 'reverse':
            query = require('../../query/reverse');
            break;
        default:
            query = require('../../query/search');
            break;
    }
    if (req.errors && req.errors.length) {
        return next();
    }

    var cmd = {
        index: searchIndex,
        searchType: 'dfs_query_then_fetch',
        body: query(req.clean)
    };

    if (req.clean.hasOwnProperty('type')) {
        cmd.type = req.clean.type;
    }
    service.search(backend, cmd, function (err, docs, meta) {
        if (err) {
            req.errors.push(err);
            logger.error('gootaxSearch->Error: ' + err.message);
        } else {
            res.data = docs;
            res.meta = meta;
        }
        next();
    });
}

module.exports = controller;