'use strict';

var packageInfo = require('../package.json');
var async = require('async');
var backend = require('../src/backend');
var service = {search: require('../service/search')};
var config = require('../helper/lib').getConfig();

/**
 * Service status controller
 * @param req
 * @param res
 * @param next
 */
module.exports = function controller(req, res, next) {
    var STATUS_OK = 'OK   ';
    var STATUS_ERROR = 'ERROR';
    var name = packageInfo.name;
    var version = packageInfo.version;
    var isNot = ' ';
    var hasError = false;
    var elasticsearchStatus, elasticsearchError;
    async.parallel(
        [
            function (cb) {
                checkElasticsearch(function (err, results) {
                    if (err) {
                        elasticsearchStatus = STATUS_ERROR;
                        elasticsearchError = '(' + err.message + ')';
                        hasError = true
                    } else {
                        elasticsearchStatus = STATUS_OK;
                        elasticsearchError = '';
                    }
                    cb(null, 1)
                });
            },
            function (cb) {
                cb(null, 1);
            }
        ], function (err, results) {
            if (hasError) {
                isNot = ' NOT ';
            }
            var status = name + ' v' + version + '\n' +
                "work on nodejs " + process.version + '\n' + '\n' +
                '[ ' + elasticsearchStatus + ' ] service "Elasticsearch" ' + elasticsearchError + '\n' + '\n' +
                'SERVICE' + isNot + 'OPERATIONAL';
            res.setHeader('Content-Type', 'text/plain');
            res.send(status);
        });

};

function checkElasticsearch(callback) {
    var cmd = {
        index: config.ELASTICSEARCH_GEO_INDEX
    };
    service.search(backend, cmd, function (err, docs, meta) {
        if (err) {
            callback(err);
        } else {
            callback(null, 1);
        }
    });

}
