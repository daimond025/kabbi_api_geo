'use strict';

var request = require('request');
var polyline = require('polyline');
var config = require("../../helper/lib").getConfig();
var apiUrl = config.TWOGIS_ROUTE_URL;
var logger = require('../../helper/logger');

/**
 * two gis route
 * @param key
 * @param req
 * @param res
 * @param next
 * @returns {*}
 */
function twoGisRoute(key, req, res, next) {
    if (req.errors && req.errors.length) {
        return next();
    }
    var clean = req.clean;

    var coords = clean.coords || [];
    var format = clean.format; // by default 'array' by sanitiser
    var splitted_route = clean.splitted_route;
    if (coords.length <= 2) {
        splitted_route = 0;
    }
    var apiKey = key ? '&key=' + key : '';

    // getting valid coordinates for 2gis api : lon lat, lon2 lat2...
    var tmpCoords = [];
    coords.forEach(function (item) {
        tmpCoords.push(item[1] + ' ' + item[0]);
    });
    var reqCoords = tmpCoords.join();

    var reqUrl = apiUrl + '?' + 'waypoints=' + reqCoords + apiKey;
    request({url: reqUrl}, function (error, response, body) {
        if (error) {
            req.errros.push(error);
            logger.error('twoGisRoute->Error: ' + error.message);
            return next();
        }
        if (response.statusCode !== 200) {
            req.errors.push('error!');
            logger.error('twoGisRoute->Error: ' + response.statusCode);
            return next();
        }

        var geoData;
        try {
            geoData = JSON.parse(body);
        } catch (e) {
            req.errors.push(e);
            logger.error('twoGisRoute->Error: ' + e.message);
            return next();
        }

        if (geoData && geoData.meta && geoData.meta.error) {
            logger.error('twoGisRoute->Error: ' + geoData.meta.error.message);
        }

        if (geoData && geoData.result && geoData.result.items) {
            var result;
            if (splitted_route && coords.length > 2) {
                result = get_splitted_route(geoData, format);
            } else {
                result = get_united_route(geoData, format);
            }
            req.data = result;
        }
        return next();
    });
}

/**
 * get splitted route
 * @param data
 * @param format
 * @returns {Array}
 */
function get_splitted_route(data, format) {
    var route = [];
    // this regexp break down a string in 9 groups,
    // match[0] is full string matched regexp
    // match[i] is fit to according group , i.e. parentheses
    var regular = /\((\d+\.\d+) (\d+\.\d+),(\d+\.\d+) (\d+\.\d+)(,(\d+\.\d+) (\d+\.\d+),(\d+\.\d+) (\d+\.\d+))?/;
    var legs;
    try {
        legs = data.result.items[0].legs;
        legs.forEach(function (leg) {
            var coords = [];
            var time = 0;
            var distance = 0;

            leg.steps.forEach(function (step) {
                distance += step.distance;
                time += step.duration;
                step.edges.forEach(function (edge) {
                    var match = edge.geometry.selection.match(regular);
                    // returned values lon lat we push in temporary array in format lat, lon
                    // NOTE:converting type to integer throught plus
                    coords.push([+match[2], +match[1]]);
                    if (match[6]) {
                        coords.push([+match[7], +match[6]]);
                    }
                });
            });
            // get rid of duplications
            coords.pop();
            coords.shift();
            // coords to necessary format
            if (format === 'polyline') {
                coords = polyline.encode(coords);
            }
            // single route to array of routes
            route.push({
                time: (time / 60).toFixed(),
                distance: distance.toString(),
                route: coords
            });
        });
    } catch (e) {
        logger.error('twoGisRoute->Error: ' + e.message);
    }

    return route;
}

/**
 * get united route
 * @param data
 * @param format
 * @returns {[null]}
 */
function get_united_route(data, format) {
    var distance = 0;
    var time = 0;
    var coords = [];

    try {
        time = data.result.items[0].total_duration;
        distance = data.result.items[0].total_distance;
        var routes = data.result.items[0].legs;
        // this regexp break down a string in 9 groups,
        // match[0] is full string matched regexp
        // match[i] is fit to according group , i.e. parentheses
        var regular = /\((\d+\.\d+) (\d+\.\d+),(\d+\.\d+) (\d+\.\d+)(,(\d+\.\d+) (\d+\.\d+),(\d+\.\d+) (\d+\.\d+))?/;
        var tmpCoords = [];
        var match;
        // very nested repeating objects
        routes.forEach(function (leg) {
            leg.steps.forEach(function (step) {
                step.edges.forEach(function (edge) {
                    match = edge.geometry.selection.match(regular);
                    // returned values lon lat we push in temporary array in format lat, lon
                    // NOTE:converting type to integer throught plus
                    tmpCoords.push([+match[2], +match[1]]);
                    if (match[6]) {
                        tmpCoords.push([+match[7], +match[6]]);
                    }
                });
            });
        });
        // delete first and last duplications
        tmpCoords.pop();
        tmpCoords.shift();

        // format to required data types
        time = (time / 60).toFixed();
        distance = distance.toString();
        if (format === 'array') {
            coords = tmpCoords;
        } else {
            coords = polyline.encode(tmpCoords);
        }
    } catch (e) {
        logger.error('twoGisRoute->Error: ' + e.message);
    }
    return [{
        time: time,
        distance: distance,
        route: coords
    }];
}

module.exports = twoGisRoute;
