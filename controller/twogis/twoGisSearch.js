'use strict';

var logger = require('../../helper/logger');
var geolib = require('geolib');
var request = require('request');
var twoGisToElasticFormatter = require('../../helper/formatter/twoGisToElasticFormatter');
var config = require("../../helper/lib").getConfig();
var apiUrl = config.TWOGIS_SEARCH_URL;
var format = 'json';


/**
 * 2gis search controller
 * @param method
 * @param apiKey
 * @param req
 * @param res
 * @param next
 * @returns {*}
 */
function controller(method, apiKey, req, res, next) {
    if (req.errors && req.errors.length) {
        return next();
    }
    // extract data
    var clean = req.clean;
    var text = (typeof clean.text === 'undefined') ? '' : encodeURIComponent(clean.text);
    var radius = (typeof clean.radius === 'undefined') ? '' : parseInt(clean.radius);
    var size = (typeof clean.size === 'undefined') ? '' : parseInt(clean.size);
    var lat = (typeof clean['focus.point.lat'] === 'undefined') ? '' : clean['focus.point.lat'];
    var lon = (typeof clean['focus.point.lon'] === 'undefined') ? '' : clean['focus.point.lon'];
    var dataUrl;
    if (method === 'autocomplete') {
        radius = radius < 30000 ? radius : 30000;
        // top-left point and right-bottom point
        var point1 = geolib.computeDestinationPoint({lat: lat, lon: lon}, radius, -45);
        var point2 = geolib.computeDestinationPoint({lat: lat, lon: lon}, radius, 135);
        dataUrl = apiUrl +
            '?key=' + apiKey +
            '&q=' + text +
            '&point1=' + point1.longitude + ',' + point1.latitude +
            '&point2=' + point2.longitude + ',' + point2.latitude +
            '&limit=' + size +
            '&format=' + format +
            '&page=1' +
            '&page_size=' + size +
            '&sort=relevance' +
            '&fields=' +
            'items.geometry.selection,' +
            'items.adm_div,' +
            'items.address,' +
            'items.description,' +
            'items.floors,' +
            'items.attraction,' +
            'items.statistics,' +
            'items.level_count,' +
            'items.capacity,' +
            'items.context,' +
            'items.schedule'
    }
    if (method === 'search') {
        dataUrl = apiUrl +
            '?key=' + apiKey +
            '&q=' + text +
            '&limit=' + size +
            '&format=' + format +
            '&page=1' +
            '&page_size=' + size +
            '&sort=relevance' +
            '&fields=' +
            'items.geometry.selection,' +
            'items.adm_div,' +
            'items.address,' +
            'items.description,' +
            'items.floors,' +
            'items.attraction,' +
            'items.statistics,' +
            'items.level_count,' +
            'items.capacity,' +
            'items.context,' +
            'items.schedule'
    }
    if (method === 'reverse') {
        lat = (typeof clean['point.lat'] === "undefined") ? '' : clean['point.lat'];
        lon = (typeof clean['point.lon'] === "undefined") ? '' : clean['point.lon'];
        radius = radius < 500 ? radius : 500;
        dataUrl = apiUrl +
            '?key=' + apiKey +
            '&point=' + lon + ',' + lat +
            '&radius=' + radius +
            '&limit=' + size +
            '&format=' + format +
            '&page=1' +
            '&page_size=' + size +
            '&sort=relevance' +
            '&fields=' +
            'items.geometry.selection,' +
            'items.adm_div,' +
            'items.address,' +
            'items.description,' +
            'items.floors,' +
            'items.attraction,' +
            'items.statistics,' +
            'items.level_count,' +
            'items.capacity,' +
            'items.context,' +
            'items.schedule'
    }
    request({url: dataUrl}, function (error, response, body) {
        if (error) {
            logger.error('twoGisSearch->Error: ' + error.message);
            return next();
        }
        try {
            var geoData = JSON.parse(body);
        } catch (e) {
            logger.error('twoGisSearch->Error: ' + e.message);
            return next();
        }
        if (parseInt(geoData.meta.code) !== 200) {
            req.errors.push(geoData);
            logger.info('twoGisSearch->Error: ' + geoData.meta.error.message);
            return next();
        }
        var formattedData = twoGisToElasticFormatter(geoData);
        var twoGisData = {
            'docs': formattedData.docs,
            'meta': formattedData.meta
        };
        res.data = twoGisData.docs;
        res.meta = twoGisData.meta;
        next();
    });
}

module.exports = controller;
