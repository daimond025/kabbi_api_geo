'use strict';

var autoGeoServiceProviderInfo = require('../helper/autoGeoServiceProviderInfo');
var gootaxSearch = require('./gootax/gootaxSearch');
var yandexSearch = require('./yandex/yandexSearch');
var herecomSearch = require('./herecom/herecomSearch');
var twoGisSearch = require('./twogis/twoGisSearch');
var googleSearch = require('./google/googleSearch');
var digimapSearch = require('./digimap/digimapSearch');
var gomapAzSearch = require('./gomap/gomapAzSearch');
var config = require('../helper/lib').getConfig();

/**
 * get search controller
 * @param method
 * @param callback
 * @returns {*}
 */
function setup(method, callback) {
    function controller(req, res, next) {
        if (req.errors && req.errors.length) {
            return next();
        }
        var clean = req.clean;
        var tenantId = clean.tenant_id;
        var tenantDomain = clean.tenant_domain;
        var cityId = clean.city_id;
        var typeApp = clean.type_app;
        var searchIndex = config.ELASTICSEARCH_GEO_INDEX;
        autoGeoServiceProviderInfo(tenantId, tenantDomain, cityId, typeApp, function (err, dataProverInfo) {
            if (err) {
                throw err;
            } else {
                var providerlabel = dataProverInfo.auto_geo_service_provider.provider;
                var apiKey1 = dataProverInfo.auto_geo_service_provider.key_1;
                var apiKey2 = dataProverInfo.auto_geo_service_provider.key_2;
                switch (providerlabel) {
                    case 'yandex':
                        yandexSearch(method, apiKey1, req, res, next);
                        break;
                    case 'herecom':
                        herecomSearch(method, apiKey1, apiKey2, req, res, next);
                        break;
                    case 'gootax':
                        gootaxSearch(method, searchIndex, req, res, next);
                        break;
                    case '2gis':
                        twoGisSearch(method, apiKey1, req, res, next);
                        break;
                    case 'google':
                        googleSearch(method, apiKey1, apiKey2, req, res, next);
                        break;
                    case 'digimap':
                        digimapSearch(method, apiKey1, req, res, next);
                        break;
                    case 'gomap.az':
                        gomapAzSearch(method, apiKey1, req, res, next);
                        break;
                    case 'geobase.az':
                        searchIndex = config.ELASTICSEARCH_GEO_AZ_INDEX;
                        gootaxSearch(method, searchIndex, req, res, next);
                        break;
                    case 'geobase.kgz':
                        searchIndex = config.ELASTICSEARCH_GEO_KGZ_INDEX;
                        gootaxSearch(method, searchIndex, req, res, next);
                        break;
                    default:
                        gootaxSearch(method, searchIndex, req, res, next);
                }
            }
        });
    }
    return callback(null, controller);
}

module.exports = setup;
