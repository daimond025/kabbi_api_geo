'use strict';

var service = {search: require('../../service/search')};
var backend = require('../../src/backend');
var logger = require('../../helper/logger');
var geolib = require('geolib');
var request = require('request');
var async = require('async');
var isPointInRadius = require('../../helper/isPointInRadius');
var config = require("../../helper/lib").getConfig();
var yandexGeocodeToElasticFormatter = require('../../helper/formatter/yandexGeocodeToElasticFormatter');
var yandexSuggestToElasticFormatter = require('../../helper/formatter/yandexSuggestToElasticFormatter');

//proxy
//////////////////////////////////////////////yandex//////////////////////////////////////////////
// "https://suggest-maps.yandex.ru/suggest-geo"  ->      "http://192.168.10.230/yandex/suggest"
// "https://geocode-maps.yandex.ru/1.x"         ->      "http://192.168.10.230/yandex/geocode"
var yandexSuggestApi = config.YANDEX_SUGGEST_URL;
var yandexGeocodeApi = config.YANDEX_GEOCODE_URL;

/**
 * yandex search controller
 * @param method
 * @param apiKey
 * @param req
 * @param res
 * @param next
 * @returns {*}
 */
function controller(method, apiKey, req, res, next) {
    if (req.errors && req.errors.length) {
        return next();
    }
    var emptyResult = {
        "docs": [],
        "meta": {
            "scores": []
        }
    };
    async.parallel(
        [
            function getSuggestYandexData(callback) {
                if (method === "autocomplete" || method === "search") {
                    var clean = req.clean;
                    var text = (typeof clean.text === "undefined") ? '' : encodeURIComponent(clean.text);
                    var lat = (typeof clean['focus.point.lat'] === "undefined") ? '' : clean['focus.point.lat'];
                    var lon = (typeof clean['focus.point.lon'] === "undefined") ? '' : clean['focus.point.lon'];
                    var lang = formatLangToIso(clean.lang);
                    var radius = (typeof clean.radius === "undefined") ? '' : parseInt(clean.radius);
                    var topPoint = geolib.computeDestinationPoint({latitude: lat, longitude: lon}, radius, 0);
                    var rightPoint = geolib.computeDestinationPoint({latitude: lat, longitude: lon}, radius, 90);
                    var spn1 = Math.abs(topPoint.latitude - lat);
                    var spn2 = Math.abs(rightPoint.longitude - lon);
                    var dataUrl = yandexSuggestApi + '?part=' + text + "&pos=4&v=7&fullpath=1&search_type=all&lang=" + lang + "&ll=" + lon + "%2C" + lat + '&spn=' + spn1 + ',' + spn2 + '&callback=';
                    request({url: dataUrl}, function (err, response, body) {
                        if (err) {
                            logger.error('yandexSearch->getSuggestYandexData->Error: ' + err.message);
                            return callback(null, emptyResult);
                        }
                        if (response.statusCode === 200) {
                            try {
                                var geoData = JSON.parse(body);
                            } catch (err) {
                                logger.error('yandexSearch->getSuggestYandexData->Error: ' + err.message);
                                return callback(null, emptyResult);
                            }
                            var formattedData = yandexSuggestToElasticFormatter(geoData);
                            var formattedFilteredData = {
                                "docs": [],
                                "meta": {
                                    "scores": []
                                }
                            };
                            formattedData.docs.forEach(function (item, i, rawResultArray) {
                                if (isPointInRadius(
                                        {
                                            latitude: parseFloat(lat),
                                            longitude: parseFloat(lon)
                                        },
                                        {
                                            latitude: item.center_point.lat,
                                            longitude: item.center_point.lon
                                        },
                                        radius)) {
                                    formattedFilteredData.docs.push(item);
                                    formattedFilteredData.meta.scores.push(1);

                                }
                            });
                            var yandexData = {
                                'docs': formattedFilteredData.docs,
                                'meta': formattedFilteredData.meta
                            };
                            return callback(null, yandexData);
                        } else {
                            logger.error('yandexSearch->getSuggestYandexData->Error: ' + response.statusCode);
                            return callback(null, emptyResult);
                        }
                    });
                } else {
                    return callback(null, emptyResult);
                }
            },
            function getGeocodeYandexData(callback) {
                var clean = req.clean;
                var text = (typeof clean.text === "undefined") ? '' : encodeURIComponent(clean.text);
                var radius = (typeof clean.radius === "undefined") ? '' : parseInt(clean.radius);
                var size = (typeof clean.size === "undefined") ? '' : parseInt(clean.size);
                var lat = (typeof clean['focus.point.lat'] === "undefined") ? '' : clean['focus.point.lat'];
                var lon = (typeof clean['focus.point.lon'] === "undefined") ? '' : clean['focus.point.lon'];
                var lang = formatLangToIso(clean.lang);
                var langAttr = "&lang=" + lang;
                var dataUrl;
                if (method === "autocomplete") {
                    rightPoint = geolib.computeDestinationPoint({latitude: lat, longitude: lon}, radius, 0);
                    topPoint = geolib.computeDestinationPoint({latitude: lat, longitude: lon}, radius, 90);
                    spn1 = topPoint.longitude - lon;
                    spn2 = rightPoint.latitude - lat;
                    dataUrl = yandexGeocodeApi + '?geocode=' + text + '&ll=' + lon + ',' + lat + '&spn=' + spn1 + ',' + spn2 + '&rspn=1' + '&results=' + size + '&apikey=' + apiKey + '&format=json' + langAttr;
                }
                if (method === "search") {
                    var rightPoint = geolib.computeDestinationPoint({latitude: lat, longitude: lon}, radius, 0);
                    var topPoint = geolib.computeDestinationPoint({latitude: lat, longitude: lon}, radius, 90);
                    var spn1 = topPoint.longitude - lon;
                    var spn2 = rightPoint.latitude - lat;
                    dataUrl = yandexGeocodeApi + '?geocode=' + text + '&ll=' + lon + ',' + lat + '&spn=' + spn1 + ',' + spn2 + '&rspn=0' + '&results=' + size + '&apikey=' + apiKey + '&format=json' + langAttr;
                }
                if (method === "reverse") {
                    lat = (typeof clean['point.lat'] === "undefined") ? '' : clean['point.lat'];
                    lon = (typeof clean['point.lon'] === "undefined") ? '' : clean['point.lon'];
                    dataUrl = yandexGeocodeApi + '?geocode=' + lon + ',' + lat + '&results=' + size + '&apikey=' + apiKey + '&format=json' + "&kind=house;street;metro" + langAttr;
                }
                request({url: dataUrl}, function (err, response, body) {
                    if (err) {
                        logger.error('yandexSearch->getGeocodeYandexData->Error: ' + err.message);
                        return callback(null, emptyResult);
                    }
                    if (parseInt(response.statusCode) === 200) {
                        try {
                            var geoData = JSON.parse(body);
                        } catch (err) {
                            logger.error('yandexSearch->getGeocodeYandexData->Error: ' + err.message);
                            return callback(null, emptyResult);
                        }
                        if (geoData.error && geoData.error.message) {
                            logger.error('yandexSearch->getGeocodeYandexData->Error: ' + geoData.error.message);
                            return callback(null, emptyResult);
                        }
                        var formattedData = yandexGeocodeToElasticFormatter(geoData);
                        var yandexData = {
                            'docs': formattedData.docs,
                            'meta': formattedData.meta
                        };
                        return callback(null, yandexData);
                    } else {
                        logger.error('yandexSearch->getGeocodeYandexData->Error: ' + response.statusCode);
                        return callback(null, emptyResult);
                    }
                });
            },
            function getOsmData(callback) {
                var query;
                switch (method) {
                    case 'search':
                        query = require('../../query/searchPublic');
                        break;
                    case 'autocomplete':
                        query = require('../../query/autocompletePublic');
                        break;
                    case 'reverse':
                        query = require('../../query/reverse');
                        break;
                    default:
                        query = require('../../query/searchPublic');
                        break;
                }
                var cmd = {
                    index: config.ELASTICSEARCH_GEO_INDEX,
                    searchType: 'dfs_query_then_fetch',
                    body: query(req.clean)
                };

                if (req.clean.hasOwnProperty('type')) {
                    cmd.type = req.clean.type;
                }
                service.search(backend, cmd, function (err, docs, meta) {
                    if (err) {
                        logger.error('yandexSearch->getOsmData->Error: ' + err.message);
                        return callback(null, emptyResult);
                    }
                    var osmData = {
                        'docs': docs,
                        'meta': meta
                    };
                    return callback(null, osmData);
                });
            }
        ],
        function (err, results) {
            if (err) {
                logger.error('yandexSearch->Error: ' + err.message);
                return next();
            }
            var yandexSuggestDocs = [];
            var yandexSuggestScores = [];

            var yandexGeocodeDocs = [];
            var yandexGeocodeScores = [];

            var osmDocs = [];
            var osmScores = [];

            if (results[0]) {
                yandexSuggestDocs = results[0].docs;
                yandexSuggestScores = results[0].meta.scores;
            }

            if (results[1]) {
                yandexGeocodeDocs = results[1].docs;
                yandexGeocodeScores = results[1].meta.scores;
            }
            if (results[2]) {
                osmDocs = results[2].docs;
                osmScores = results[2].meta.scores;
            }
            var resultData;
            if (yandexSuggestDocs.length) {
                resultData = {
                    'docs': yandexSuggestDocs.concat(osmDocs),
                    'meta': {
                        scores: yandexSuggestScores.concat(osmScores)
                    }
                };
            } else {
                resultData = {
                    'docs': yandexGeocodeDocs.concat(osmDocs),
                    'meta': {
                        scores: yandexGeocodeScores.concat(osmScores)
                    }
                };
            }
            res.data = resultData.docs;
            res.meta = resultData.meta;
            return next();
        });
}

function formatLangToIso(lang) {
    switch (lang) {
        case 'ru':
            lang = "ru_RU";
            break;
        case 'uk':
            lang = "uk_UA";
            break;
        case 'be':
            lang = "be_BY";
            break;
        case 'en':
            lang = "en_US";
            break;
        case 'tr':
            lang = "tr_TR";
            break;
        default:
            lang = "ru_RU";
            break;
    }
    return lang;
}

module.exports = controller;