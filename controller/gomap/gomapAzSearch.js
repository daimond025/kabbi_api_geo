'use strict';

var logger = require('../../helper/logger');
var request = require('request');
var gomapAzToElasticFormatter = require('../../helper/formatter/gomapAzToElasticFormatter');
var async = require('async');
var config = require("../../helper/lib").getConfig();
var apiUrl = config.GOMAP_SEARCH_URL;
var geolib = require('geolib');

/**
 * gomap controller
 * @param method
 * @param guid
 * @param req
 * @param res
 * @param next
 * @returns {*}
 */
function controller(method, guid, req, res, next) {
    if (req.errors && req.errors.length) {
        return next();
    }
    var clean = req.clean;
    var text = (typeof clean.text === "undefined") ? '' : clean.text;
    var radius = (typeof clean.radius === "undefined") ? '' : parseInt(clean.radius);
    var size = (typeof clean.size === "undefined") ? '' : parseInt(clean.size);
    var lang = clean.lang || '';
    var lat, lon;
    if (method === 'autocomplete' || method === 'search') {
        lat = (typeof clean['focus.point.lat'] === "undefined") ? '' : clean['focus.point.lat'];
        lon = (typeof clean['focus.point.lon'] === "undefined") ? '' : clean['focus.point.lon'];
        autocomplete({
            method: method,
            guid: guid,
            text: text,
            lang: lang,
            size: size,
            lat: lat,
            lon: lon
        }, res, next);
    } else if (method === 'reverse') {
        lat = (typeof clean['point.lat'] === "undefined") ? '' : clean['point.lat'];
        lon = (typeof clean['point.lon'] === "undefined") ? '' : clean['point.lon'];
        reverse({
            guid: guid,
            lat: lat,
            lon: lon,
            lang: lang,
            size: size,
            radius: radius
        }, res, next);

    }
}

/**
 * reverse search
 * @param params
 * @param res
 * @param next
 */
function reverse(params, res, next) {
    var dataUrl = apiUrl + '/searchNearBy';
    var form = {
        guid: params.guid,
        x: params.lon,
        y: params.lat,
        lng: params.lang,
        type: 0,
        subtype: 0,
        R: params.radius
    };
    request({
        headers: {'content-type': 'application/x-www-form-urlencoded'},
        method: 'POST',
        url: dataUrl,
        form: form
    }, function (error, response, body) {
        if (error || parseInt(response.statusCode) !== 200) {
            logger.error('gomapAzSearch->Error: ' + error.message);
            return next();
        }
        try {
            var geoData = JSON.parse(body);
        } catch (e) {
            logger.error('gomapAzSearch->Error: ' + e.message);
        }
        var success = geoData.success;
        if (!success) {
            return next();
        }
        var results = [];
        var rows = geoData.rows;
        results = results.concat(rows);
        var formattedData = gomapAzToElasticFormatter(results, params.lang);
        var gomapData = {
            'docs': formattedData.docs,
            'meta': formattedData.meta
        };
        res.data = gomapData.docs;
        res.meta = gomapData.meta;
        next();
    });


}

/**
 * autocomplete
 * @param {Object} params
 * @param {String} params.method
 * @param {String} params.guid
 * @param {String} params.text
 * @param {String} params.lang
 * @param {Number} params.size
 * @param {Number} params.lat
 * @param {Number} params.lon
 * @param res
 * @param next
 */
function autocomplete(params, res, next) {
    var dataUrl = apiUrl + '/searchObj';
    var form = {
        guid: params.guid,
        name: params.text,
        lng: params.lang
    };
    request({
        headers: {'content-type': 'application/x-www-form-urlencoded'},
        method: 'POST',
        url: dataUrl,
        form: form
    }, function (error, response, body) {
        if (error || parseInt(response.statusCode) !== 200) {
            return next();
        }
        try {
            var geoData = JSON.parse(body);
        } catch (e) {
            logger.error('[req]', e);
            return next();
        }
        var success = geoData.success;
        if (!success) {
            return next();
        }
        var total = geoData.total.split('^');
        var countRows = total[1];
        var results = [];
        var countPages = Math.ceil(countRows / 10);
        if (countPages > 2) {
            countPages = 2;
        }
        var rows = geoData.rows;
        results = results.concat(rows);
        if (countPages === 1) {
            if (params.size) {
                results = results.slice(0, params.size);
            }
            var formattedData = gomapAzToElasticFormatter(results, params.lang);
            var gomapData = {
                'docs': formattedData.docs,
                'meta': formattedData.meta
            };
            res.data = gomapData.docs;
            res.meta = gomapData.meta;
            if (params.method === "autocomplete") {
                fixResultScores(params.lat, params.lon, res, function (err, fixedResult) {
                    if (err) {
                        return next();
                    } else {
                        res.data = fixedResult.docs;
                        res.meta = fixedResult.meta;
                        return next();
                    }
                });
            } else {
                return next();
            }
        } else {
            var pages = [];
            for (var i = 2; i <= countPages; i++) {
                pages.push(i);
            }
            async.each(pages, function (numPage, done) {
                var dataUrl = apiUrl + '/searchObjPage';
                var objForm = {
                    guid: params.guid,
                    numPage: numPage
                };
                request({
                    headers: {'content-type': 'application/x-www-form-urlencoded'},
                    method: 'POST',
                    url: dataUrl,
                    form: objForm
                }, function (error, response, body) {
                    if (error || parseInt(response.statusCode) !== 200) {
                        logger.error('[req]', error);
                        return done();
                    }
                    try {
                        var geoData = JSON.parse(body);
                    } catch (e) {
                        logger.error('[req]', e);
                        return done();
                    }
                    var success = geoData.success;
                    var rows = geoData.rows;
                    if (!success) {
                        return done();
                    }
                    results = results.concat(rows);
                    return done();
                })
            }, function (err) {
                if (err) {
                    logger.error('[req]', err);
                    return next();
                }
                results = results.slice(0, params.size);
                var formattedData = gomapAzToElasticFormatter(results, params.lang);
                var gomapData = {
                    'docs': formattedData.docs,
                    'meta': formattedData.meta
                };
                res.data = gomapData.docs;
                res.meta = gomapData.meta;
                if (params.method === "autocomplete") {
                    fixResultScores(params.lat, params.lon, res, function (err, fixedResult) {
                        if (err) {
                            return next();
                        } else {
                            res.data = fixedResult.docs;
                            res.meta = fixedResult.meta;
                            return next();
                        }
                    });
                } else {
                    return next();
                }
            });
        }
    });
}

/**
 * fix Result Scores by distance
 * @param centerLat
 * @param centerLon
 * @param results
 * @param callback
 */
function fixResultScores(centerLat, centerLon, results, callback) {
    var fixedResults = {
        'docs': [],
        'meta': {
            "scores": []
        }
    };
    var centerPoint = {
        latitude: centerLat,
        longitude: centerLon
    };
    async.each(results.data, function (item, done) {
        var addressPoint = {
            latitude: item.center_point.lat,
            longitude: item.center_point.lon
        };
        var distance = 99999999999;
        //in metres
        try {
            distance = geolib.getDistance(
                centerPoint,
                addressPoint
            );
        } catch (err) {
            console.log(err);
        }
        item.distance = distance;
        fixedResults.docs.push(item);
        done();
    }, function (err) {
        if (err) {
            console.log(err);
        }
        //Сортируем по уменьшению расстояния
        fixedResults.docs.sort(function (a, b) {
            return b.distance - a.distance;
        });
        var score = 0.1;
        fixedResults.docs.forEach(function (item, i, raw) {
            fixedResults.meta.scores.push(score);
            score += 0.1;
        });
        fixedResults.docs.reverse();
        return callback(null, fixedResults);
    });
}

module.exports = controller;
