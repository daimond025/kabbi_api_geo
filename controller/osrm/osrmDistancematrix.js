'use strict';

var logger = require('../../helper/logger');
var request = require('request');
var distanceMatrixFormatter = require('../../helper/distanceMatrixFormatter');
var config = require("../../helper/lib").getConfig();
var apiUrl = config.OSRM_DISTANCEMATRIX_URL;


/**
 * Distance matrix
 * @param key
 * @param req
 * @param res
 * @param next
 * @returns {*}
 */
function distanceMatrix(req, res, next) {
    if (req.errors && req.errors.length) {
        return next();
    }
    var clean = req.clean;
    var origins = clean.origins || [];
    var originsLength = origins.length;
    var originsTmp = [];
    origins.forEach(function (latLon, index, coords) {
        originsTmp.push(latLon[1] + ',' + latLon[0]);
    });
    origins = originsTmp.join(';');
    var destinations = clean.destinations || [];
    var destinationsTmp = [];
    destinations.forEach(function (latLon, index, coords) {
        destinationsTmp.push(latLon[1] + ',' + latLon[0]);
    });
    destinations = destinationsTmp.join(';');
    var sources = '';
    for (var i = 0; i < originsLength; i++) {
        sources = sources + i + ';';
    }
    sources = sources.slice(0, -1);
    var reqUrl = apiUrl + '/' + origins + ';' + destinations + '?sources=' + sources;
    request.get({url: reqUrl}, function (error, response, body) {
        if (error) {
            req.errors.push(error);
            logger.error('osrmDistancematrix->Error: ' + error.message);
            return next();
        }
        if (response.statusCode !== 200) {
            req.errors.push('osrmDistancematrix->response status is not 200');
            return next();
        }
        var resultData;
        try {
            resultData = JSON.parse(body);
        } catch (e) {
            logger.error('osrmDistancematrix->->Error: ' + e.message);
            req.errors.push(e);
            return next();
        }
        if (resultData.code !== 'Ok') {
            req.errors.push(resultData.code);
            logger.error('osrmDistancematrix->Error: ' + resultData.code);
            return next();
        }

        req.data = distanceMatrixFormatter.osrmFormatter(resultData.durations);
        return next();
    });
}


module.exports = distanceMatrix;