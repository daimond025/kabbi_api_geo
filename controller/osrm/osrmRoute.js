'use strict';

var request = require('request');
var polyline = require('polyline');
var config = require("../../helper/lib").getConfig();
var apiUrl = config.OSRM_ROUTE_URL;
var logger = require('../../helper/logger');

/**
 * osrm route
 * @param req
 * @param res
 * @param next
 * @returns {*}
 */
function osrmRoute(req, res, next) {
    if (req.errors && req.errors.length) {
        return next();
    }
    var clean = req.clean;
    var coords = clean.coords || [];
    var format = clean.format; // by sanitiser default = 'array'
    var splitted_route = clean.splitted_route;
    if (coords.length <= 2) {
        splitted_route = 0;
    }
    var reqUrl;
    var coordsReq;
    var coordsTmp = [];

    coords.forEach(function (latLon, index, coords) {
        coordsTmp.push(latLon[1] + ',' + latLon[0]);
    });
    coordsReq = coordsTmp.join(';');

    reqUrl = apiUrl + '/' + coordsReq + '?' +
        'overview=full' +
        '&steps=true';// steps=true is for splitted_route

    request.get({url: reqUrl}, function (error, response, body) {
        if (error) {
            req.errors.push(error);
            logger.error('osrmRoute->Error: ' + error.message);
            return next();
        }
        if (response.statusCode !== 200) {
            logger.error('osrmRoute->Error: ' + response.statusCode);
            return next();
        }
        var geoData;
        try {
            geoData = JSON.parse(body);
        } catch (e) {
            req.errors.push(e);
            return next();
        }
        if (geoData.code !== 'Ok') {
            req.errors.push(geoData.code);
            logger.error('osrmRoute->Error: ' + geoData.code);
            return next();
        }

        var result;
        if (splitted_route && coords.length > 2) {
            result = get_splitted_route(geoData, format);
        } else {
            result = get_united_route(geoData, format);
        }
        req.data = result;
        next();
    });

    /**
     * get splitted route
     * @param data
     * @param format
     * @returns {Array}
     */
    function get_splitted_route(data, format) {
        var routes = [];

        var legs;
        try {
            legs = data.routes[0].legs;
            legs.forEach(function (leg) {
                var time = leg.duration;
                var distance = leg.distance;
                var coords = [];

                leg.steps.forEach(function (step) {
                    // latLonArr is [[lat,lon], [lat, lon]]
                    var latLonArr = polyline.decode(step.geometry);
                    latLonArr.forEach(function (latLon) {
                        coords.push(latLon);
                    });
                });
                // encode array of lat, lon to polyline, if necessary
                if (format === 'polyline') {
                    coords = polyline.encode(coords);
                }

                routes.push({
                    time: (time / 60).toFixed(),
                    distance: distance.toString(),
                    route: coords
                });
            });
        } catch (e) {
            req.errors.push(e);
            logger.error('osrmRoute->Error: ' + e.message);
        }

        return routes;
    }

    /**
     * get united route
     * @param data
     * @param format
     * @returns {[null]}
     */
    function get_united_route(data, format) {
        var distance = 0;
        var time = 0;

        var route;
        try {
            route = data.routes[0];
            time = route.duration ? (route.duration / 60).toFixed() : '';
            distance = route.distance ? route.distance.toString() : '';
        } catch (e) {
            req.errors.push(e);
            logger.error('osrmRoute->Error: ' + e.message);
        }
        // form to standart response
        var coords = route.geometry || [];
        if (format !== 'polyline') {
            coords = polyline.decode(route.geometry);
        }

        return [{
            route: coords,
            time: time,
            distance: distance
        }];
    }

}

module.exports = osrmRoute;
