'use strict';

var request = require('request');
var polyline = require('polyline');
var logger = require('../../helper/logger');
var config = require("../../helper/lib").getConfig();
var apiUrl = config.HERECOM_ROUTE_URL;

/**
 * herecom route controller
 * @param key1 app id
 * @param key2 app code
 * @param req
 * @param res
 * @param next
 * @returns {*}
 */
function herecomRoute(key1, key2, req, res, next) {
    if (req.errors && req.errors.length) {
        return next();
    }
    var clean = req.clean;

    var coords = clean.coords || [];
    var format = clean.format; // by default 'array' by sanitiser
    var splitted_route = clean.splitted_route;
    var appId = key1 ? 'app_id=' + key1 : '';
    var appCode = key2 ? '&app_code=' + key2 : '';
    if (coords.length <= 2) {
        splitted_route = 0;
    }
    // most important parametr cause response will return coords, either no coords at all
    // legs - for detailed_route, shape - for common
    var routeAttributes = '&routeAttributes=' + (splitted_route ? 'legs' : 'shape');
    // less unnecessary info
    var representation = '&representation=overview';
    // to who
    var mode = '&mode=fastest;car';
    // for detailed routes
    var legAttributes = '&legAttributes=shape,length,trafficTime';
    var waypoints = '';
    var reqUrl;

    coords.forEach(function (item, index, coords) {
        waypoints += '&waypoint' + index + '=' + 'geo!' + item[0] + ',' + item[1];
    });

    reqUrl = apiUrl + '?' +
        appId +
        appCode +
        waypoints +
        mode +
        routeAttributes +
        representation +
        legAttributes;
    request({url: reqUrl}, function (error, response, body) {
        if (error) {
            req.errros.push(error);
            logger.error('herecomRoute->Error: ' + error.message);
            return next();
        }
        if (parseInt(response.statusCode) !== 200) {
            logger.error('herecomRoute->Error: ' + response.statusCode);
            return next();
        }

        var geoData;
        try {
            geoData = JSON.parse(body);
        } catch (e) {
            req.errors.push(e);
            logger.error('herecomRoute->Error: ' + e.message);
            return next();
        }
        // res.json(geoData);
        if (geoData.response.type === 'ApplicationError') {
            req.errors.push(geoData.response.subtype);
            return next();
        }
        var route;
        if (coords.length > 2 && splitted_route === 1) {
            route = get_splitted_route(geoData, format);
        } else {
            route = get_united_route(geoData, format);
        }
        req.data = route;
        return next();
    });

    function get_united_route(data, format) {
        var distance = 0;
        var time = 0;
        var coords;

        var tmpCoords = [];
        var routes = [];
        var tmpSubArr;

        try {
            time = data.response.route[0].summary.trafficTime ? (data.response.route[0].summary.trafficTime / 60).toFixed() : '';
            distance = data.response.route[0].summary.distance ? data.response.route[0].summary.distance.toString() : '';
            routes = data.response.route[0].shape;
            routes.forEach(function (item) {
                tmpSubArr = item.split(',');
                tmpCoords.push([+tmpSubArr[0], +tmpSubArr[1]]);
            });
        } catch (e) {
            logger.error('herecomRoute->Error: ' + e.message);
            req.errors.push(e);
        }

        // form to required type
        if (format === 'polyline') {
            coords = polyline.encode(tmpCoords);
        } else {
            coords = tmpCoords;
        }
        // res.json(geoData);
        return [{
            time: time,
            distance: distance,
            route: coords
        }];
    }

    function get_splitted_route(data, format) {
        var routes = [];

        var legs;
        try {
            legs = data.response.route[0].leg;

            legs.forEach(function (leg) {
                var distance = leg.length;
                var time = leg.trafficTime;

                // array of 'lat,lon'
                var coords = leg.shape.map(function (route) {
                    return route.split(',');
                });
                if (format === 'polyline') {
                    coords = polyline.encode(coords);
                }

                routes.push({
                    distance: distance.toString(),
                    time: (time / 60).toFixed(),
                    route: coords
                });
            });
        } catch (e) {
            logger.error('herecomRoute->Error: ' + e.message);
            req.errors.push(e);
        }

        return routes;
    }

}

module.exports = herecomRoute;
