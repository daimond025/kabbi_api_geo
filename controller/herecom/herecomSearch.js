'use strict';

var logger = require('../../helper/logger');
var geolib = require('geolib');
var request = require('request');
var async = require('async');
var herecomAddressToElasticFormatter = require('../../helper/formatter/herecomAddressToElasticFormatter');
var herecomPlaceToElasticFormatter = require('../../helper/formatter/herecomPlaceToElasticFormatter');
var config = require("../../helper/lib").getConfig();
var apiGeocodeUrl = config.HERECOM_SEARCH_URL;
var apiGeocodeReverseUrl = config.HERECOM_REVERSE_URL;
var apiPlaceUrl = config.HERECOM_PLACE_URL;

/**
 * herecom search controller
 * @param method
 * @param appCode
 * @param appId
 * @param req
 * @param res
 * @param next
 * @returns {*}
 */
function controller(method, appCode, appId, req, res, next) {
    if (req.errors && req.errors.length) {
        return next();
    }
    var clean = req.clean;
    var text = (typeof clean.text === "undefined") ? '' : encodeURIComponent(clean.text);
    var radius = (typeof clean.radius === "undefined") ? '' : parseInt(clean.radius);
    var size = (typeof clean.size === "undefined") ? '' : parseInt(clean.size);
    var lat = (typeof clean['focus.point.lat'] === "undefined") ? '' : clean['focus.point.lat'];
    var lon = (typeof clean['focus.point.lon'] === "undefined") ? '' : clean['focus.point.lon'];
    var lang = (typeof clean.lang === "undefined") ? null : clean.lang;
    var langAttr = lang ? "&language=" + lang : "";
    var app_code = appCode;//'GR5J1S5ytRuUf2e3E4G8Ww';
    var app_id = appId;//'d8RGq7ltXdCCv8G5wJjr';
    if (method === "autocomplete" || method === "search") {
        var topLeftPoint = geolib.computeDestinationPoint({latitude: lat, longitude: lon}, radius, -45);
        var topLeftLat = topLeftPoint.latitude;
        var topLeftLon = topLeftPoint.longitude;
        var buttomRightPoint = geolib.computeDestinationPoint({latitude: lat, longitude: lon}, radius, 135);
        var bottomRightLat = buttomRightPoint.latitude;
        var bottomRightLon = buttomRightPoint.longitude;

        var dataGeocodeUrl = apiGeocodeUrl + "?app_id=" + app_id + "&app_code=" + app_code + "&mapview=" + topLeftLat + "," + topLeftLon + ";" + bottomRightLat + "," + bottomRightLon + "&maxresults=" + size + "&searchtext=" + text + langAttr;
        var dataPlaceUrl = apiPlaceUrl + "?app_id=" + app_id + "&app_code=" + app_code + "&q=" + text + "&in=" + lat + "," + lon + ";r=" + radius + "&refinements=true" + "&size=" + size + "&tf=plain";
        async.parallel(
            [
                function getAddresses(callback) {
                    request({
                        url: dataGeocodeUrl,
                        headers: {'content-type': 'application/json'}
                    }, function (error, response, body) {
                        if (error) {
                            logger.error('herecomSearch->Error: ' + error.message);
                            return callback(error);
                        }
                        if (!error && parseInt(response.statusCode) === 200) {
                            try {
                                var geoData = JSON.parse(body);
                            } catch (err) {
                                logger.error('herecomSearch->Error: ' + err.message);
                                return callback(error);
                            }
                            var formattedData = herecomAddressToElasticFormatter(geoData);
                            var addressData = {
                                'docs': formattedData.docs,
                                'meta': formattedData.meta
                            };
                            return callback(null, addressData);
                        } else {
                            callback(error);
                        }
                    });

                },
                function getPlaces(callback) {
                    request({
                        url: dataPlaceUrl, headers: {
                            'content-type': 'application/json',
                            'accept-language': lang
                        }
                    }, function (error, response, body) {
                        if (error) {
                            logger.error('herecomSearch->Error: ' + error.message);
                            return callback(error);
                        }
                        if (!error && parseInt(response.statusCode) === 200) {
                            try {
                                var geoData = JSON.parse(body);
                            } catch (err) {
                                logger.error('herecomSearch->Error: ' + error.message);
                                return callback(error);
                            }
                            var formattedData = herecomPlaceToElasticFormatter(geoData);
                            var placeData = {
                                'docs': formattedData.docs,
                                'meta': formattedData.meta
                            };
                            callback(null, placeData);
                        } else {
                            callback(error);
                        }
                    });
                }],
            function (err, results) {
                if (err) {
                    logger.error('herecomSearch->Error: ' + err.message);
                    return next();
                }
                var addressDocs = [];
                var addressScores = [];
                var placeDocs = [];
                var placeScores = [];
                if (results[0]) {
                    addressDocs = results[0].docs;
                    addressScores = results[0].meta.scores;
                }
                if (results[1]) {
                    placeDocs = results[1].docs;
                    placeScores = results[1].meta.scores;
                }
                var resultData = {
                    'docs': addressDocs.concat(placeDocs),
                    'meta': {scores: addressScores.concat(placeScores)}
                };
                res.data = resultData.docs;
                res.meta = resultData.meta;
                next();
            });
    } else if (method === "reverse") {
        lat = (typeof clean['point.lat'] === "undefined") ? '' : clean['point.lat'];
        lon = (typeof clean['point.lon'] === "undefined") ? '' : clean['point.lon'];
        var dataUrl = apiGeocodeReverseUrl + "?app_id=" + app_id + "&app_code=" + app_code + "&pos=" + lat + "," + lon + "&maxresults=" + size + "&mode=retrieveAddresses" + "&prox=" + lat + "," + lon + "," + radius + langAttr;
        request({url: dataUrl, headers: {'content-type': 'application/json'}}, function (error, response, body) {
            if (error) {
                logger.error('herecomSearch->Error: ' + error.message);
                return next();
            }
            if (!error && parseInt(response.statusCode) === 200) {
                try {
                    var geoData = JSON.parse(body);
                } catch (err) {
                    logger.error('herecomSearch->Error: ' + error.message);
                    return next();
                }
                var formattedData = herecomAddressToElasticFormatter(geoData);
                res.data = formattedData.docs;
                res.meta = formattedData.meta;
                next();
            } else {
                next();
            }
        });
    }

}

module.exports = controller;
