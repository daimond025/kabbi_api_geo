'use strict';

var packageInfo = require('../package.json');


var config_list = require('../helper/lib').getConfig();
var name_service =  config_list.NAME_SERVICE;

var exec = require('child_process').exec;
/**
 * Service version controller
 * @param req
 * @param res
 * @param next  service api_geo stop
 */
module.exports = function controller(req, res, next) {

    res.setHeader('Content-Type', 'text/plain');
    var info = "restart " + name_service + " service";

    console.log('service ' + name_service + ' restart');

    setTimeout(function () {
        exec("service " + name_service + " restart", function(error, stdout, stderr) {
            if (error) {
                console.log(error.code);
            }
            console.log(error);
            console.log(stdout);
            console.log(stderr);


        });
    }, 5000);


    res.send(info);
};