var app = require('express')();

//var peliasConfig = require('./modules/pelias-config').generate().api;

var debug = parseInt(require('./helper/lib').getConfig().MY_DEBUG);

if (debug) {
    app.use(require('./middleware/access_log')('common'));
}

/** ----------------------- pre-processing-middleware ----------------------- **/

app.use(require('./middleware/headers'));
app.use(require('./middleware/cors'));
app.use(require('./middleware/options'));
app.use(require('./middleware/jsonp'));

/** ----------------------- routes ----------------------- **/
var v1 = require('./routes/v1');
v1.addRoutes(app);

/** ----------------------- error middleware ----------------------- **/

app.use(require('./middleware/404'));
app.use(require('./middleware/500'));

module.exports = app;
