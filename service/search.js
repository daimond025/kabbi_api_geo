'use strict';

function service(backend, cmd, cb) {
    backend().client.search(cmd, function (err, data) {

        if (err) {
            return cb(err);
        }
        var docs = [];
        var meta = {
            scores: []
        };

        if (data && data.hits && data.hits.total && Array.isArray(data.hits.hits)) {

            docs = data.hits.hits.map(function (hit) {

                meta.scores.push(hit._score);

                // map metadata in to _source so we
                // can serve it up to the consumer
                hit._source._id = hit._id;
                hit._source._type = hit._type;
                hit._source._score = hit._score;

                return hit._source;
            });
        }

        // fire callback
        return cb(null, docs, meta);
    });

}

module.exports = service;
