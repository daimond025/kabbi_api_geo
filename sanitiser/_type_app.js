'use strict';

var supportedTypeApps = ['client', 'driver', 'frontend'];

// validate inputs, convert types and apply defaults
function sanitize(raw, clean) {
    // error & warning messages
    var messages = {errors: [], warnings: []};

    // coercions
    clean.type_app = raw.type_app;
    if (supportedTypeApps.indexOf(clean.type_app) === -1) {
        //FIX FOR OLD VERSIONS
        clean.type_app = "frontend";
        // messages.errors.push('Invalid type of application');
    }
    return messages;
}

// export function
module.exports = sanitize;