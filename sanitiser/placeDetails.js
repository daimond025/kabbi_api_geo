'use strict';

var async = require('async');

var sanitizeAll = require('../sanitiser/sanitizeAll');
var hashSanitizer = require('../sanitiser/_hash');

// all req.clean params have been formed here
var sanitizers = {
    tenant_id: require('../sanitiser/_tenant_id'),
    city_id: require('../sanitiser/_city_id'),
    lang: require('../sanitiser/_lang'),
    region: require('../sanitiser/_region'),
    placeId: require('../sanitiser/_placeId'),
};

var sanitize = function (req, cb) {
    async.waterfall([
        function (callback) {
            hashSanitizer(req, req.query, callback);
        },
        function (reqHashValidated, callback) {
            sanitizeAll(req, sanitizers, callback);
        }
    ], function (err, recChecked) {

        cb(null, recChecked);
    });
};

// export sanitize for testing
module.exports.sanitize = sanitize;
module.exports.sanitiser_list = sanitizers;

// middleware
module.exports.middleware = function (req, res, next) {
    sanitize(req, function (err, clean) {
        if (err) {
            res.status(400);
            return next(err);
        }
        next();
    });
};