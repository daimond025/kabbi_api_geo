'use strict';

/**
 *
 * @param raw
 * @param clean
 * @returns {{errors: Array, warnings: Array}}
 */
function sanitize(raw, clean) {
    // error & warning messages
    var messages = {errors: [], warnings: []};
    // coercions
    clean.sort = raw.sort;
    if (clean.sort === "distance" && clean.sort === "growth") {
        clean.sort = "distance";
    }
    return messages;
}

// export function
module.exports = sanitize;