'use strict';

var type_mapping = require('../helper/type_mapping'),
    async = require('async'),
    sanitizeAll = require('../sanitiser/sanitizeAll'),
    hashSanitizer = require('../sanitiser/_hash');


var sanitizers = {
    singleScalarParameters: require('../sanitiser/_single_scalar_parameters'),
    layers: require('../sanitiser/_targets')('layers', type_mapping.layer_with_aliases_to_type),
    sources: require('../sanitiser/_targets')('sources', type_mapping.source_to_type),
    size: require('../sanitiser/_size'),
    radius: require('../sanitiser/_radius'),
    lang: require('../sanitiser/_lang'),
    private: require('../sanitiser/_flag_bool')('private', false),
    geo_reverse: require('../sanitiser/_geo_reverse'),
    boundary_country: require('../sanitiser/_boundary_country'),
    type_app: require('../sanitiser/_type_app'),
    tenant_id: require('../sanitiser/_tenant_id'),
    tenant_domain: require('../sanitiser/_tenant_domain'),
    city_id: require('../sanitiser/_city_id'),
    sort: require('../sanitiser/_sort')
};


var sanitize = function (req, cb) {
    async.waterfall([
        function (callback1) {
            hashSanitizer(req, req.query, callback1);
        },
        function (reqHashValidated, callback2) {
            sanitizeAll(req, sanitizers, callback2);
        }], function (err, recChecked) {
        if (err) {
            cb(err);
        } else {
            cb(null, recChecked);
        }
    });
};

// export sanitize for testing
module.exports.sanitize = sanitize;
module.exports.sanitiser_list = sanitizers;

// middleware
module.exports.middleware = function (req, res, next) {
    sanitize(req, function (err, clean) {
        next();
    });
};
