'use strict';

var check = require('check-types');


// validate inputs, convert types and apply defaults
function sanitize(raw, clean) {
    // error & warning messages
    var messages = {errors: [], warnings: []};
    // coercions
    clean.tenant_id = raw.tenant_id;
    clean.type_app = raw.type_app;
    if (clean.type_app === "client" || clean.type_app === "frontend") {
        if ((!check.string(clean.tenant_id)) || (check.string(clean.tenant_id) && clean.tenant_id.length < 1)) {
            messages.errors.push('Invalid tenant id');
        }
    }
    return messages;
}

// export function
module.exports = sanitize;