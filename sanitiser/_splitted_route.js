'use strict';

/**
 * If amount of points > 2
 * you might split single route response into several
 * e.g. single was: [{route}]
 * splitted become: [{route}, {route}...]
 *
 * @param {Object } raw req.params
 * @param {Object} clean req.clean
 */
function sanitize(raw, clean) {
    // error & warning messages
    var messages = {
        errors: [],
        warnings: []
    };
    var splitted_route = 0;

    if (parseInt(raw.splitted_route) === 1) {
        splitted_route = 1;
    }
    clean.splitted_route = splitted_route;

    return messages;
}

module.exports = sanitize;
