'use strict';

var async = require('async');

var sanitizeAll = require('../sanitiser/sanitizeAll');
var hashSanitizer = require('../sanitiser/_hash');

// all req.clean params have been formed here
var sanitizers = {
    type_app: require('../sanitiser/_type_app'),
    tenant_id: require('../sanitiser/_tenant_id'),
    tenant_domain: require('../sanitiser/_tenant_domain'),
    city_id: require('../sanitiser/_city_id'),
    coords: require('../sanitiser/_coords'),
    format: require('./_format_osrm'),
    splitted_route: require('./_splitted_route')
};

var sanitize = function (req, cb) {
    async.waterfall([
        function (callback) {
            hashSanitizer(req, req.query, callback);
        },
        function (reqHashValidated, callback) {
            sanitizeAll(req, sanitizers, callback);
        }
    ], function (err, recChecked) {
        cb(null, recChecked);
    });
};

// export sanitize for testing
module.exports.sanitize = sanitize;
module.exports.sanitiser_list = sanitizers;

// middleware
module.exports.middleware = function (req, res, next) {
    sanitize(req, function (err, clean) {
        if (err) {
            res.status(400);
            return next(err);
        }
        next();
    });
};
