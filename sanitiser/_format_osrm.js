'use strict';

/**
 *
 * @param raw
 * @param clean
 * @returns {{errors: Array, warnings: Array}}
 */
function sanitize(raw, clean) {
    var messages = {
        errors: [],
        warnings: []
    };

    var format;
    switch (raw.format) {
        case 'polyline':
            format = 'polyline';
            break;
        default:
            format = 'array';
    }
    clean.format = format;

    return messages;
}

module.exports = sanitize;
