'use strict';

var sanitizeAll = require('../sanitiser/sanitizeAll'),
    async = require('async'),
    hashSanitizer = require('../sanitiser/_hash');

var sanitizers = {
    singleScalarParameters: require('../sanitiser/_single_scalar_parameters'),
    text: require('../sanitiser/_text'),
    size: require('../sanitiser/_size'),
    radius: require('../sanitiser/_radius'),
    region: require('../sanitiser/_region'),
    lang: require('../sanitiser/_lang'),
    private: require('../sanitiser/_flag_bool')('private', false),
    geo_autocomplete: require('../sanitiser/_geo_autocomplete'),
    type_app: require('../sanitiser/_type_app'),
    tenant_id: require('../sanitiser/_tenant_id'),
    tenant_domain: require('../sanitiser/_tenant_domain'),
    city_id: require('../sanitiser/_city_id'),
    sort: require('../sanitiser/_sort')
};

var sanitize = function (req, cb) {
    async.waterfall([
        function (callback1) {
            hashSanitizer(req, req.query, callback1);
        },
        function (reqHashValidated, callback2) {
            sanitizeAll(req, sanitizers, callback2);
        }
    ], function (err, recChecked) {
        if (err) {
            cb(err);
        } else {
            cb(null, recChecked);
        }
    });
};

// export sanitize for testing
module.exports.sanitize = sanitize;
module.exports.sanitiser_list = sanitizers;

// middleware
module.exports.middleware = function (req, res, next) {
    sanitize(req, function (err, clean) {
        if (err) {
            res.status(400); // 400 Bad Request
            return next(err);
        }
        next();
    });
};
