'use strict';

// validate inputs, convert types and apply defaults
function sanitize(raw, clean) {
  // error & warning messages
  var messages = {
    errors: [],
    warnings: []
  };
  if (raw.lang) {
    clean.lang = raw.lang;
  }

  return messages;
}

// export function
module.exports = sanitize;
