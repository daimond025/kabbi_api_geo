'use strict';

var regions = ['de', 'en' , 'ru'];

// validate inputs, convert types and apply defaults
function sanitize(raw, clean) {
    // error & warning messages
    var messages = {errors: [], warnings: []};

    if (typeof raw.region == 'undefined') {
        clean.region = null;
    }
    else{
        var region_value = raw.region.trim();

        if(regions.includes(region_value)){
            clean.region = region_value;
        }

    }

    return messages;
}

// export function
module.exports = sanitize;