'use strict';

/**
 *  validate inputs, convert types and apply defaults
 *
 * @param {Object} raw  req.params
 * @param {Object} clean req.clean
 *
 */

function sanitize(raw, clean) {
    var messages = {
        errors: [],
        warnings: []
    };
    var coordinations = [];

    if (!raw.destinations) {
        messages.errors.push('Empty destinations coords');
        return messages;
    }


    var tmpArr = raw.destinations.split(';');
    var tmpArr2;
    tmpArr.forEach(function (item) {
        var etalonStr = /^(\d+[.]\d+,\d+[.]\d+)$/;
        var isCorrect = etalonStr.test(item);
        if (!isCorrect) {
            messages.errors.push('Destinations coords are incorrect :' + item);
            return messages;
        }
        tmpArr2 = item.split(',');
        coordinations.push([+tmpArr2[0], +tmpArr2[1]]);
    });


    clean.destinations = coordinations;
    return messages;
}

module.exports = sanitize;
