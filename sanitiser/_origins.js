'use strict';

/**
 *  validate inputs, convert types and apply defaults
 *
 * @param {Object} raw  req.params
 * @param {Object} clean req.clean
 *
 */

function sanitize(raw, clean) {
    var messages = {
        errors: [],
        warnings: []
    };
    var coordinations = [];

    if (!raw.origins) {
        messages.errors.push('Empty origin coords');
        return messages;
    }

    var tmpArr = raw.origins.split(';');

    if (!tmpArr.length) {
        messages.errors.push('Bad origin coords');
        return messages;
    }

    var tmpArr2;
    tmpArr.forEach(function (item) {
        var etalonStr = /^(\d+[.]\d+,\d+[.]\d+)$/;
        var isCorrect = etalonStr.test(item);
        if (!isCorrect) {
            messages.errors.push('Origins coords are incorrect :' + item);
            return messages;
        }
        tmpArr2 = item.split(',');
        coordinations.push([+tmpArr2[0], +tmpArr2[1]]);
    });
    clean.origins = coordinations;
    return messages;
}

module.exports = sanitize;
