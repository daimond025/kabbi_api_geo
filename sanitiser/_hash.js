'use strict';

//var queryHashValidator = require('../helper/queryHashValidator');
var config = require("../helper/lib").getConfig();
var checkHash = parseInt(config.MY_CHECK_HASH);

function hashSanitizer(req, clean, cb) {
    var hash = req.query.hash;
    clean['hash'] = hash;
    var type_app = clean.type_app;
    var tenant_id = clean.tenant_id;
    var tenant_domain = clean.tenant_domain;
    if (typeof req.errors === "undefined") {
        req.errors = [];
    }

    return cb(null, clean);
   /* if (checkHash) {
        if (hash === null || hash === "" || typeof hash === "undefined") {
            req.errors.push('Invalid hash value');
            return cb(null, clean);
        }
        queryHashValidator(req.query, hash, type_app, tenant_id, tenant_domain, function (err, result) {
            if (err) {
                req.errors.push(err.message);
                return cb(null, clean);
            }
            if (result !== true) {
                if (req.errors.length === 0) {
                    req.errors.push('Invalid hash value');
                }
            }
            return cb(null, clean);
        });
    } else {
        return cb(null, clean);
    }*/
}

module.exports = hashSanitizer;
