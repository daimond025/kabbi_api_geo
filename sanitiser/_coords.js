'use strict';

/**
 *  validate inputs, convert types and apply defaults
 *
 * @param {Object} raw  req.params
 * @param {Object} clean req.clean
 *
 */

function sanitize(raw, clean) {
    // error & warning messages
    var messages = {
        errors: [],
        warnings: []
    };
    var coordinations = [];
    // example for testing 13.388860,52.517037;13.388860,52.517037
    var etalonStr = /^(\d+[.]\d+,\d+[.]\d+;)(\d+[.]\d+,\d+[.]\d+;)*(\d+[.]\d+,\d+[.]\d+)$/;
    var isCorrect = etalonStr.test(raw.coords);
    if (raw.coords) {
        if (!isCorrect) {
            messages.errors.push('Coords is incorrect! coords:' + raw.coords);
            return messages;
        }
        // extract from kind of lat1,lon1;lat2,lon2...
        var tmpArr = raw.coords.split(';');
        var tmpArr2;
        tmpArr.forEach(function (item) {
            tmpArr2 = item.split(',');
            coordinations.push([+tmpArr2[0], +tmpArr2[1]]);
        });

        clean.coords = coordinations;
    } else {
        messages.errors.push('Empty coords! coords:');
        return messages;
    }

    return messages;
}

module.exports = sanitize;
