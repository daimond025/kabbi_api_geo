'use strict';

var MIN_RADIUS = 0.01,
    MAX_RADIUS = 50000,
    DEFAULT_RADIUS = 30;

// validate inputs, convert types and apply defaults
function sanitize(raw, clean) {
    // error & warning messages
    var messages = {errors: [], warnings: []};

    // coercions
    clean.radius = parseFloat(raw.radius);

    if (isNaN(clean.radius)) {
        clean.radius = DEFAULT_RADIUS;
    }


    // ensure size falls within defined range
    if (clean.radius > MAX_RADIUS) {
        // set the max size
        messages.warnings.push('out-of-range integer \'radius\', using MAX_SIZE');
        clean.radius = MAX_RADIUS;
    }
    else if (clean.size < MIN_RADIUS) {
        // set the min size
        messages.warnings.push('out-of-range integer \'size\', using MIN_SIZE');
        clean.radius = MIN_RADIUS;
    }


    clean.radius = clean.radius;
    return messages;
}

// export function
module.exports = sanitize;