'use strict';

var check = require('check-types');


// validate inputs, convert types and apply defaults
function sanitize(raw, clean) {
    // error & warning messages
    var messages = {errors: [], warnings: []};
    // coercions
    clean.city_id = raw.city_id;
    if ((!check.string(clean.city_id)) || (check.string(clean.city_id) && clean.city_id.length < 1)) {
        clean.city_id = undefined;
    }
    return messages;
}

// export function
module.exports = sanitize;