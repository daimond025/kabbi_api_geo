'use strict';

var check = require('check-types');


// validate inputs, convert types and apply defaults
function sanitize(raw, clean) {
    // error & warning messages
    var messages = {errors: [], warnings: []};
    // coercions
    clean.tenant_domain = raw.tenant_domain;
    clean.type_app = raw.type_app;
    if (clean.type_app === "driver") {
        if (!check.string(clean.tenant_domain)) {
            messages.errors.push('Invalid tenant domain');
        }
        if (check.string(clean.tenant_domain)) {
            clean.tenant_domain = clean.tenant_domain.toLowerCase();
        }
    }
    return messages;
}

// export function
module.exports = sanitize;