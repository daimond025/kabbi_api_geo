'use strict';

var check = require('check-types');


// validate inputs, convert types and apply defaults
function sanitize(raw, clean) {
    // error & warning messages
    var messages = {errors: [], warnings: []};
    // coercions
    clean.placeId = raw.placeId.trim();
    if((!check.string(clean.placeId))){
        messages.errors.push('Invalid place id');
    }
    return messages;
}

// export function
module.exports = sanitize;