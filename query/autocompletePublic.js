'use strict';

/**
 * generate query
 * @param clean
 * @returns {{query: {filtered: {query: {bool: {should: [null,null]}}, filter: {bool: {must: [null], must_not: {term: {_type: string}}}}}}, size: *, track_scores: boolean}}
 */
function generateQuery(clean) {
    var radius = clean['radius'];
    var text = clean.text.trim();
    if (typeof clean.parsed_text === "object") {
        if (typeof clean.parsed_text.name === "string") {
            text = clean.parsed_text.name;
        }

    }
    var lat = clean['focus.point.lat'];
    var lon = clean['focus.point.lon'];
    var size = clean['size'];
    var sort = clean['sort'];
    if (sort !== "distance" && sort !== "growth") {
        sort = "distance";
    }
    if (clean.types) {
        delete clean.types;
    }
    if (clean.type) {
        delete clean.type;
    }
    var q = {
        "query": {
            "filtered": {
                "query": {
                    "bool": {
                        "should": [
                            {
                                "match": {
                                    "name.default": {
                                        "analyzer": "peliasPhrase",
                                        "type": "phrase",
                                        "boost": 20,
                                        "slop": 1,
                                        "query": text
                                    }
                                }

                            },
                            {
                                "function_score": {
                                    "query": {
                                        "match": {
                                            "name.default": {
                                                "analyzer": "peliasPhrase",
                                                "type": "phrase",
                                                "boost": 20,
                                                "slop": 1,
                                                "query": text
                                            }
                                        }
                                    },
                                    "functions": [
                                        {
                                            "linear": {
                                                "center_point": {
                                                    "origin": {
                                                        "lat": lat,
                                                        "lon": lon
                                                    },
                                                    "offset": "30km",
                                                    "scale": "50km",
                                                    "decay": 0.5
                                                }
                                            }
                                        }
                                    ],
                                    "score_mode": "avg",
                                    "boost_mode": "replace"
                                }
                            }
                        ]
                    }
                },
                "filter": {
                    "bool": {
                        "must": [
                            {
                                "geo_distance": {
                                    "distance": radius,
                                    "distance_type": "plane",
                                    "optimize_bbox": "indexed",
                                    "_cache": true,
                                    "center_point": {
                                        "lat": lat,
                                        "lon": lon
                                    }
                                }
                            }
                        ],
                        "must_not": {
                            "term": {"_type": "osmaddress"}
                        }

                    }
                }

            }
        },
        "size": size,
        "track_scores": true
    };
    if (sort === "growth") {
        q.sort = [
            {
                "_script": {
                    "script": "try { Integer.parseInt(doc['address.number'].value); } catch(Exception e){ return Integer.MAX_VALUE;}",
                    "type": "number",
                    "order": "asc",
                    "lang": "groovy"
                }
            }
        ];
    } else {
        q.sort = [
            {
                "_geo_distance": {
                    "order": "asc",
                    "mode": "min",
                    "center_point": {
                        "lat": lat,
                        "lon": lon
                    }
                }
            },
            "_score"
        ];

    }
    return q;
}

module.exports = generateQuery;
