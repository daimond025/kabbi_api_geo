var config;
try {
    config = require('dotenv-safe').load();
    console.log('OK');
    process.exit(0);
} catch (err) {
    console.error(err.message);
    process.exit(255);
}

