## Common description 

Gootax GEO service

## Requirements version of Node.js
Node version : 0.12.12

## Install Dependencies

```bash
npm install
```
    
## How to run service
* Заполнить .env файл по примеру .env.example файла


* Проверить корректность .env файла: 
```bash
npm run check-env
``` 
При успешном выполнении в консоль выведет: OK. В случае ошибки в консоль выведет описание ошибки.

* Запустить сервис: 
```bash
npm run start
```
Или
```bash
node index.js
```

## API description

https://docs.google.com/document/d/1g8-BQSq5LL1ShpABRDddPeWS81J0vsXUPaTIWmDrov0